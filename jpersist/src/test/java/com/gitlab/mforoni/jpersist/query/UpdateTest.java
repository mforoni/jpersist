package com.gitlab.mforoni.jpersist.query;

import java.math.BigDecimal;
import org.joda.time.LocalDate;
import com.github.mforoni.jbasic.time.JLocalDates;
import com.gitlab.mforoni.jpersist.query.Update;
import com.gitlab.mforoni.jpersist.query.Where;
import com.gitlab.mforoni.jpersist.query.Query.Operator;
import junit.framework.TestCase;

public class UpdateTest extends TestCase {

	private Update getUpdateQuery(final LocalDate date, final Float f, final BigDecimal bd) {
		return new Update.Builder("table", "column1", true).set("column2", date)
				.where(new Where.Builder("column3", Operator.EQUAL, f).and("column4", Operator.MINOR, bd).toWhere()).toUpdate();
	}

	public void testGetStatement() {
		final LocalDate date = JLocalDates.today();
		final Float f = new Float(19.5667);
		final BigDecimal bd = BigDecimal.valueOf(0.33);
		final Update update = getUpdateQuery(date, f, bd);
		assertNotNull(update);
		assertEquals("update table set column1=?, column2=? where column3=? and column4<?", update.getStatement().toLowerCase());
		assertNotNull(update.getParameters());
		assertTrue(update.getParameters().size() == 4);
		assertEquals(new Boolean(true), update.getParameters().get(0));
		assertEquals(date, update.getParameters().get(1));
		assertEquals(f, update.getParameters().get(2));
		assertEquals(bd, update.getParameters().get(3));
	}

	public void testGetParameters() {
		final LocalDate date = JLocalDates.today();
		final Float f = new Float(19.5667);
		final BigDecimal bd = BigDecimal.valueOf(0.33);
		final Update update = getUpdateQuery(date, f, bd);
		assertNotNull(update);
		assertNotNull(update.getParameters());
		assertTrue(update.getParameters().size() == 4);
		assertEquals(new Boolean(true), update.getParameters().get(0));
		assertEquals(date, update.getParameters().get(1));
		assertEquals(f, update.getParameters().get(2));
		assertEquals(bd, update.getParameters().get(3));
	}
}
