package com.gitlab.mforoni.jpersist.query;

import java.util.Arrays;
import com.gitlab.mforoni.jpersist.query.Join;
import junit.framework.TestCase;

public class JoinTest extends TestCase {

	public void testGetStatement() {
		final Join join = new Join.Builder("p", "persone", "p.idpersona=u.idpersona").toJoin();
		assertEquals("inner join persone p on p.idpersona=u.idpersona", join.getStatement());
	}

	public void testBuildExpression() {}

	public void testBuildConditionStringStringString() {
		final String cond = Join.buildCondition("a", "f", "idsoggetto");
		assertEquals("a.idsoggetto=f.idsoggetto", cond);
	}

	public void testBuildConditionStringStringListOfString() {
		final String cond = Join.buildCondition("a", "f", Arrays.asList("id1", "id2", "id3"));
		assertEquals("a.id1=f.id1 and a.id2=f.id2 and a.id3=f.id3", cond);
	}
}
