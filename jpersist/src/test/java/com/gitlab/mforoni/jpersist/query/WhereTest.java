package com.gitlab.mforoni.jpersist.query;

import com.gitlab.mforoni.jpersist.query.Where;
import com.gitlab.mforoni.jpersist.query.Query.Operator;
import junit.framework.TestCase;

public class WhereTest extends TestCase {

	private Where getWhereInABC() {
		return new Where("columnName", Operator.IN, new String[] { "A", "B", "C" });
	}

	private Where getWhereInPrimitiveIntegers() {
		return new Where("columnName", Operator.IN, new int[] { 12, 445, 33, 290 });
	}

	public void testWhere() {
		final Where inABC = getWhereInABC();
		assertNotNull(inABC);
		final Where inPrimitiveIntegers = getWhereInPrimitiveIntegers();
		assertNotNull(inPrimitiveIntegers);
	}

	public void testGetExpression() {
		final Where inABC = getWhereInABC();
		assertNotNull(inABC);
		assertEquals("where columnname in (?, ?, ?)", inABC.getExpression().toLowerCase());
		final Where inPrimitiveIntegers = getWhereInPrimitiveIntegers();
		assertNotNull(inPrimitiveIntegers);
		assertEquals("where columnname in (?, ?, ?, ?)", inPrimitiveIntegers.getExpression().toLowerCase());
	}

	public void testGetParameters() {
		final Where inABC = getWhereInABC();
		assertNotNull(inABC);
		assertNotNull(inABC.getParameters());
		assertEquals(3, inABC.getParameters().size());
		assertEquals("A", inABC.getParameters().get(0));
		assertEquals("B", inABC.getParameters().get(1));
		assertEquals("C", inABC.getParameters().get(2));
		final Where inPrimitiveIntegers = getWhereInPrimitiveIntegers();
		assertNotNull(inPrimitiveIntegers);
		assertNotNull(inPrimitiveIntegers.getParameters());
		assertEquals(4, inPrimitiveIntegers.getParameters().size());
		assertEquals(445, inPrimitiveIntegers.getParameters().get(1));
		assertEquals(33, inPrimitiveIntegers.getParameters().get(2));
		assertEquals(290, inPrimitiveIntegers.getParameters().get(3));
	}

	public void testGetColumnNames() {
		final Where inABC = getWhereInABC();
		assertNotNull(inABC);
		assertNotNull(inABC.getColumnNames());
		assertEquals(3, inABC.getColumnNames().size());
		assertEquals("columnName", inABC.getColumnNames().get(0));
		assertEquals("columnName", inABC.getColumnNames().get(1));
		assertEquals("columnName", inABC.getColumnNames().get(2));
		final Where inPrimitiveIntegers = getWhereInPrimitiveIntegers();
		assertNotNull(inPrimitiveIntegers);
		assertNotNull(inPrimitiveIntegers.getColumnNames());
		assertEquals(4, inPrimitiveIntegers.getColumnNames().size());
		assertEquals("columnName", inPrimitiveIntegers.getColumnNames().get(0));
		assertEquals("columnName", inPrimitiveIntegers.getColumnNames().get(2));
	}
}
