package com.gitlab.mforoni.jpersist.query;

import com.gitlab.mforoni.jpersist.query.OrderBy;
import com.gitlab.mforoni.jpersist.query.Select;
import com.gitlab.mforoni.jpersist.query.Where;
import com.gitlab.mforoni.jpersist.query.Query.Operator;
import junit.framework.TestCase;

public class OldSelectTest extends TestCase {

	private Select getSelect() {
		return new Select.Builder("pers", "persone", Select.columns("nome", "cognome", "indirizzo")).toSelect();
	}

	private Select getSelectMax() {
		return new Select.Builder("p", "persone", Select.max("idpersona")).toSelect();
	}

	private Select getSelectCount() {
		return new Select.Builder("p", "persone", Select.count("idpersona")).toSelect();
	}

	private Select getSelectJoin() {
		// select R.* from PPRISCHI R
		// inner join PPUNIT U on R.IDRISCHIO=U.IDRISCHIO
		// where U.IDPRODOTTO=idProdotto and U.IDVERSIONE=idVersione order by R.CDESCRIZIONE
		return new Select.Builder("R", "PPRISCHI", null).innerJoin("U", "PPUNIT", "R.IDRISCHIO=U.IDRISCHIO")
				.where(new Where.Builder("U", "IDPRODOTTO", Operator.EQUAL, 33).and("U", "IDVERSIONE", Operator.EQUAL, 1).toWhere())
				.orderBy(new OrderBy("R", "CDESCRIZIONE")).toSelect();
	}

	public void testGetStatement() {
		final Select select = getSelect();
		assertNotNull(select);
		assertEquals("select nome, cognome, indirizzo from persone pers", select.getStatement().toLowerCase());
		final Select max = getSelectMax();
		assertNotNull(max);
		assertEquals("select max(idpersona) from persone p", max.getStatement().toLowerCase());
		final Select count = getSelectCount();
		assertNotNull(count);
		assertEquals("select count(idpersona) from persone p", count.getStatement().toLowerCase());
		final Select join = getSelectJoin();
		assertNotNull(join);
		assertEquals("select r.* from pprischi r inner join ppunit u on r.idrischio=u.idrischio"
				+ " where u.idprodotto=? and u.idversione=? order by r.cdescrizione", join.getStatement().toLowerCase());
	}

	public void testGetParameters() {
		final Select join = getSelectJoin();
		assertNotNull(join);
		assertEquals(2, join.getParameters().size());
		assertEquals(33, join.getParameters().get(0));
		assertEquals(1, join.getParameters().get(1));
	}
}
