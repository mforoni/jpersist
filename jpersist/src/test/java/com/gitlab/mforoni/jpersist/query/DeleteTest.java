package com.gitlab.mforoni.jpersist.query;

import com.gitlab.mforoni.jpersist.query.Delete;
import com.gitlab.mforoni.jpersist.query.Where;
import com.gitlab.mforoni.jpersist.query.Query.Operator;
import junit.framework.TestCase;

public class DeleteTest extends TestCase {

	public void testGetStatement() {
		final Where where = new Where("columnName", Operator.MAJOR, 33);
		final Delete delete = new Delete.Builder("table").where(where).toDelete();
		assertNotNull(delete);
		assertEquals("delete from table where columnname>?", delete.getStatement().toLowerCase());
	}

	public void testGetParameters() {
		final Where where = new Where("columnName", Operator.MAJOR, 33);
		final Delete delete = new Delete.Builder("table").where(where).toDelete();
		assertNotNull(delete);
		assertNotNull(delete.getParameters());
		assertTrue(delete.getParameters().size() == 1);
		assertEquals(33, delete.getParameters().get(0));
	}
}
