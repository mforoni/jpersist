package com.gitlab.mforoni.jpersist;

import java.lang.reflect.Field;
import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Table;
import com.google.common.base.Strings;

final class Annotations {
  private Annotations() {
    throw new AssertionError();
  }

  @Nonnull
  public static String getColumnName(@Nonnull final Field f) {
    final Column column = f.getAnnotation(Column.class);
    return Strings.isNullOrEmpty(column.name()) ? f.getName() : column.name();
  }

  @Nonnull
  public static String getTableName(@Nonnull final Class<?> type) {
    final Table table = type.getAnnotation(Table.class);
    final String name = Strings.isNullOrEmpty(table.name()) ? type.getSimpleName() : table.name();
    return Strings.isNullOrEmpty(table.schema()) ? name : table.schema() + "." + name;
  }
}
