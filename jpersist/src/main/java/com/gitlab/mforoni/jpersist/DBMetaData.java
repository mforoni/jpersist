package com.gitlab.mforoni.jpersist;

import static com.google.common.base.Preconditions.checkNotNull;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * Immutable class representing a Relational Database meta-data. It contains:
 * <ul>
 * <li>the {@link DatabaseMetaData}</li>
 * <li>the {@code Driver}</li>
 * </ul>
 * Provides useful method for retrieving:
 * <ul>
 * <li>the catalog names defined in this database<br>
 * {@link #getCatalogs()}</li>
 * <li>the schemas defined in this database<br>
 * {@link #getSchemas()}</li>
 * <li>the tables defined in this database<br>
 * {@link #getTable(String, String, String)}</li>
 * <li>the columns of a specified table<br>
 * {@link #getColumns(String, String, String)}</li>
 * <li>the primary key of a specified table<br>
 * {@link #getPKMetaData(String, String, String)}</li>
 * <li>the foreign keys of a specified table<br>
 * TODO
 * <li>the indexes defined on a specified table<br>
 * {@link #getIndexes(String, String, String)}</li>
 * </ul>
 *
 * @see java.sql.DatabaseMetaData
 * @see Driver
 * @see java.sql.Connection
 * @see Schema
 * @see TableDesc
 * @see ColumnMetaData
 * @see PKMetaData
 * @see IndexDescription
 * @author Foroni
 */
@Immutable
public final class DBMetaData {
  private final DatabaseMetaData databaseMetaData;
  private DBMSName dbmsName;

  /**
   * Instantiates a new database.
   * 
   * @param databaseMetaData the {@code DatabaseMetaData} (cannot be <tt>null</tt>)
   * @param driver the driver
   * @see java.sql.DatabaseMetaData
   * @see Driver
   */
  public DBMetaData(@Nonnull final DatabaseMetaData databaseMetaData) {
    checkNotNull(databaseMetaData);
    this.databaseMetaData = databaseMetaData;
    dbmsName = null; // lazy initialization is really Immutable? FIXME
  }

  public DBMetaData(final Connection connection) throws SQLException {
    this(connection.getMetaData());
  }

  /**
   * Returns the {@link DatabaseMetaData}.
   *
   * @return the database meta-data
   */
  @Nonnull
  public DatabaseMetaData getDatabaseMetaData() {
    return databaseMetaData;
  }

  /**
   * Gets the database product name.
   * <p>
   * With UCanAccess driver the returned database product name is "Ucanaccess for access db(Jet)
   * using hasqldb".
   *
   * @return the database product name
   * @throws SQLException
   */
  public String getDatabaseProductName() throws SQLException {
    return databaseMetaData.getDatabaseProductName();
  }

  /**
   * Returns the {@link DBMSName} of this {@code Database}.
   *
   * @return the DBMS name
   * @throws SQLException
   * @see DatabaseMetaData#getDatabaseProductName()
   */
  public DBMSName getDBMSName() throws SQLException {
    if (dbmsName == null) {
      final String databaseProductName = databaseMetaData.getDatabaseProductName();
      if (databaseProductName.startsWith("Ucanaccess")) {
        return DBMSName.ACCESS;
      }
      if (databaseProductName.startsWith("HSQL")) {
        return DBMSName.HSQL;
      }
      dbmsName = DBMSName.valueOf(databaseProductName.toUpperCase());
    }
    return dbmsName;
  }

  /**
   * The Enum DBMSName.
   */
  @Deprecated
  public enum DBMSName {
    ORACLE, ACCESS, HSQL, UNKNOWN;
  }

  @Override
  public String toString() {
    return databaseMetaData.toString();
  }

  /**
   * Retrieves the catalog names available in this database. The results are ordered by catalog
   * name.<br>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> this method is supported by odbc-jdbc driver with MS Access database.
   *
   * @see DatabaseMetaData#getCatalogs()
   * @return a list of strings i.e. the catalog names defined in this {@code Database}
   * @throws SQLException
   * @see DatabaseMetaData#getCatalogs
   */
  @Nonnull
  public List<String> getCatalogs() throws SQLException {
    return DBMetaDatas.getCatalogs(databaseMetaData);
  }

  /**
   * Retrieves a description of the schemas available in this database in a list of {@link Schema}
   * objects. The results are ordered first by catalog name and then by schema name. <br>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> this method is not supported by jdbc odbc bridge with MS Access database.
   *
   * @return the schemas defined in this {@code Database}
   * @throws SQLException
   * @see DatabaseMetaData#getSchemas()
   * @see Schema
   */
  @Nonnull
  public List<Schema> getSchemas() throws SQLException {
    return DBMetaDatas.getSchemas(databaseMetaData);
  }

  /**
   * Retrieves a list of descriptions for the tables available in this database matching the
   * specified catalog, schema and table name. All the parameters are optional, if not specified the
   * comparison is omitted. The results are ordered by table type, catalog, schema and table name.
   * <p>
   * A {@code TableDescription} contains the following information:
   * <ul>
   * <li>catalog (nullable)</li>
   * <li>schema (nullable)</li>
   * <li>table name (not nullable)</li>
   * <li>table type: typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL
   * TEMPORARY", "ALIAS", "SYNONYM"</li>
   * </ul>
   * Pattern example: {@code tableName = "PP%"} retrieves the {@code TableDescription} for all the
   * tables having name starting with <tt>PP</tt>.<br>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> this method is supported by odbc-jdbc driver with MS Access database.
   *
   * @param catalog the catalog name
   * @param schemaPattern a pattern for the schema
   * @param tableNamePattern a pattern for the table name
   * @return a list of {@code TableDescription} (may be empty)
   * @throws SQLException
   * @see DatabaseMetaData#getTables(String, String, String, String[])
   * @see TableDesc
   */
  @Nonnull
  public List<TableDesc> getTables(@Nullable final String catalog,
      @Nullable final String schemaPattern, @Nullable final String tableNamePattern,
      final TableType... tableTypes) throws SQLException {
    return DBMetaDatas.getTables(databaseMetaData, catalog, schemaPattern, tableNamePattern,
        tableTypes);
  }

  /**
   * Retrieves a list of descriptions for the tables available in this database matching the
   * specified patterns for schema and table name. All the parameters are optional, if not specified
   * the comparison is omitted. The results are ordered by table type, catalog, schema and table
   * name.
   * <p>
   * A {@code TableDescription} contains the following information:
   * <ul>
   * <li>catalog (nullable)</li>
   * <li>schema (nullable)</li>
   * <li>table name (not nullable)</li>
   * <li>table type: typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL
   * TEMPORARY", "ALIAS", "SYNONYM"</li>
   * </ul>
   * Pattern example: {@code tableName = "PP%"} retrieves the {@code TableDescription} for all the
   * tables having name starting with <tt>PP</tt>.<br>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> this method is supported by odbc-jdbc driver with MS Access database.
   *
   * @param schemaPattern a pattern for the schema
   * @param tableNamePattern a pattern for the table name
   * @return a list of {@code TableDescription} (may be empty)
   * @throws SQLException
   * @see DatabaseMetaData#getTables(String, String, String, String[])
   * @see TableDesc
   */
  @Nonnull
  public List<TableDesc> getTables(final String schemaPattern, final String tableNamePattern)
      throws SQLException {
    return DBMetaDatas.getTables(databaseMetaData, null, schemaPattern, tableNamePattern);
  }

  /**
   * Retrieves a list of descriptions for the tables available in this database matching the
   * specified pattern for table name. The parameter is optional, if not specified the comparison is
   * omitted. The results are ordered by table type, catalog, schema and table name.
   * <p>
   * A {@link TableDesc} contains the following information:
   * <ul>
   * <li>catalog (nullable)</li>
   * <li>schema (nullable)</li>
   * <li>table name (not nullable)</li>
   * <li>table type: typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL
   * TEMPORARY", "ALIAS", "SYNONYM"</li>
   * </ul>
   * Pattern example: {@code tableName = "PP%"} retrieves the {@code TableDescription} for all the
   * tables having name starting with <tt>PP</tt>.<br>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> this method is supported by odbc-jdbc driver with MS Access database.
   *
   * @param tableNamePattern a pattern for the table name
   * @return a list of {@code TableDescription} (may be empty)
   * @throws SQLException
   * @see DatabaseMetaData#getTables(String, String, String, String[])
   * @see TableDesc
   */
  @Nonnull
  public List<TableDesc> getTables(final String tableNamePattern) throws SQLException {
    return DBMetaDatas.getTables(databaseMetaData, null, null, tableNamePattern);
  }

  /**
   * Retrieves a list of descriptions for the tables available in this database. The results are
   * ordered by table type, catalog, schema and table name.
   * <p>
   * A {@link TableDesc} contains the following information:
   * <ul>
   * <li>catalog (nullable)</li>
   * <li>schema (nullable)</li>
   * <li>table name (not nullable)</li>
   * <li>table type: typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL
   * TEMPORARY", "ALIAS", "SYNONYM"</li>
   * </ul>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> this method is supported by odbc-jdbc driver with MS Access database.
   *
   * @return a list of {@code TableDescription} (may be empty)
   * @throws SQLException
   * @see DatabaseMetaData#getTables(String, String, String, String[])
   * @see TableDesc
   */
  @Nonnull
  public List<TableDesc> getTables() throws SQLException {
    return DBMetaDatas.getTables(databaseMetaData, null, null, null);
  }

  /**
   * Retrieves the description for the table available in this database matching the specified
   * catalog, schema and table name. All the parameters except the table name are optional, if not
   * specified the comparison is omitted. The results are ordered by table type, catalog, schema and
   * table name.
   * <p>
   * A {@link TableDesc} contains the following information:
   * <ul>
   * <li>catalog (nullable)</li>
   * <li>schema (nullable)</li>
   * <li>table name (not nullable)</li>
   * <li>table type: typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL
   * TEMPORARY", "ALIAS", "SYNONYM"</li>
   * </ul>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> this method is supported by odbc-jdbc driver with MS Access database.
   *
   * @param catalog the catalog
   * @param schema the schema
   * @param tableName the table name
   * @return a {@code TableDescription}
   * @throws SQLException
   * @throws IllegalStateException if not exactly one table is found with the specified name
   * @see DatabaseMetaData#getTables(String, String, String, String[])
   * @see TableDesc
   */
  @Nonnull
  public TableDesc getTable(@Nullable final String catalog, @Nullable final String schema,
      @Nonnull final String tableName) throws SQLException {
    checkNotNull(tableName);
    final List<TableDesc> list =
        DBMetaDatas.getTables(databaseMetaData, catalog, schema, tableName, TableType.TABLE);
    switch (list.size()) {
      case 0:
        throw new IllegalStateException(String.format(
            "Unable to retrieve TableDescription of table %s: the table does not exist in the catalog %s schema %s",
            tableName, catalog, schema));
      case 1:
        return list.get(0);
      default:
        throw new IllegalStateException(String.format(
            "Unable to retrieve a unique TableDescription for table %s: under the catalog %s schema %s were found %s tables",
            tableName, catalog, schema, list.size()));
    }
  }

  /**
   * Retrieves the description for the table available in this database matching the specified
   * schema and table name. The schema's name parameter is optional, if not specified the comparison
   * is omitted. The results are ordered by table type, catalog, schema and table name.
   * <p>
   * A {@code TableDescription} contains the following information:
   * <ul>
   * <li>catalog (nullable)</li>
   * <li>schema (nullable)</li>
   * <li>table name (not nullable)</li>
   * <li>table type: typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL
   * TEMPORARY", "ALIAS", "SYNONYM"</li>
   * </ul>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> this method is supported by odbc-jdbc driver with MS Access database.
   *
   * @param schema the schema (may be <tt>null</tt>)
   * @param tableName the table name (cannot be <tt>null</tt>)
   * @return a {@code TableDescription}
   * @throws SQLException
   * @throws IllegalStateException if not exactly one table is found with the specified name
   * @see DatabaseMetaData#getTables(String, String, String, String[])
   * @see TableDesc
   */
  @Nonnull
  public TableDesc getTable(final String schema, @Nonnull final String tableName)
      throws SQLException {
    return getTable(null, schema, tableName);
  }

  /**
   * Retrieves the description for the table available in this database matching the specified table
   * name. The results are ordered by table type, catalog, schema and table name.
   * <p>
   * A {@link TableDesc} contains the following information:
   * <ul>
   * <li>catalog (nullable)</li>
   * <li>schema (nullable)</li>
   * <li>table name (not nullable)</li>
   * <li>table type: typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL
   * TEMPORARY", "ALIAS", "SYNONYM"</li>
   * </ul>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> this method is supported by odbc-jdbc driver with MS Access database.
   *
   * @param tableName the table name (cannot be <tt>null</tt>)
   * @return a {@code TableDescription}
   * @throws SQLException
   * @throws IllegalStateException if not exactly one table is found with the specified name
   * @see DatabaseMetaData#getTables(String, String, String, String[])
   * @see TableDesc
   */
  @Nonnull
  public TableDesc getTable(@Nonnull final String tableName) throws SQLException {
    return getTable(null, null, tableName);
  }

  /**
   * Retrieves a description of the given table's primary key columns. The resulting primary key
   * columns are stored in the {@code PKMetaData} ordered by column name. A {@code PKMetaData}
   * contains some primary key meta information as the columns that make up the primary key, their
   * order and the name of the constraint.<br>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> not supported by odbc jdbc driver with MS ACCESS: using this DBMS this method will
   * throw the following {@code java.sql.SQLException}: [Microsoft][Driver Manager ODBC] Il driver
   * non supporta questa funzione.
   *
   * @param tableName the table name (cannot be <tt>null</tt>)
   * @return the {@code PKMetaData}
   * @throws SQLException
   * @see DBMetaData#getPKMetaData(String, String, String)
   * @see PKMetaData
   */
  @Nonnull
  public PKMetaData getPKMetaData(@Nonnull final String tableName) throws SQLException {
    return DBMetaDatas.getPKMetaData(databaseMetaData, null, null, tableName);
  }

  /**
   * Retrieves a description of the given table's primary key columns. The resulting primary key
   * columns are stored in the {@code PKMetaData} ordered by column name. A {@code PKMetaData}
   * contains some primary key meta information as the columns that make up the primary key, their
   * order and the name of the constraint.<br>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> not supported by odbc jdbc driver with MS ACCESS: using this DBMS this method will
   * throw the following {@code java.sql.SQLException}: [Microsoft][Driver Manager ODBC] Il driver
   * non supporta questa funzione.
   *
   * @param schema the schema
   * @param tableName the table name (cannot be <tt>null</tt>)
   * @return the {@code PKMetaData}
   * @throws SQLException
   * @see DBMetaData#getPKMetaData(String, String, String)
   */
  @Nonnull
  public PKMetaData getPKMetaData(final String schema, @Nonnull final String tableName)
      throws SQLException {
    return DBMetaDatas.getPKMetaData(databaseMetaData, null, schema, tableName);
  }

  @Nonnull
  public PKMetaData getPKMetaData(final String catalog, final String schema,
      @Nonnull final String tableName) throws SQLException {
    return DBMetaDatas.getPKMetaData(databaseMetaData, catalog, schema, tableName);
  }

  /**
   * Retrieves a description of the given table's indices and statistics. The returned list of
   * {@code IndexDescription} are ordered by non unique, type, index name, and ordinal position. All
   * the parameters except the table name are optional, if not specified the comparison is omitted.
   * An {@code IndexDescription} contains some index meta information like the column on which the
   * index is defined, the type and the name.
   * <p>
   * Opens, uses and closes a {@code ResultSet}.
   *
   * @param catalog the catalog
   * @param schema the schema
   * @param tableName the table name
   * @return the {@code IndexDescription}
   * @throws SQLException
   * @see DatabaseMetaData#getIndexInfo(String, String, String, boolean, boolean)
   * @see IndexDescription
   */
  @Nonnull
  public List<IndexDescription> getIndexes(final String catalog, final String schema,
      @Nonnull final String tableName) throws SQLException {
    return DBMetaDatas.getIndexes(databaseMetaData, catalog, schema, tableName);
  }

  /**
   * Retrieves a description of table columns available in this database matching the specified
   * table name. The returned list of {@link ColumnMetaData} are ordered by catalog, schema, table
   * name, and finally by ordinal position. <br>
   * A {@code ColumnMetaData} contains the meta information of a database column i.e. the index,
   * name, SQL type, precision, scale and nullability.<br>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> this method is supported by odbc-jdbc driver with MS Access database.
   *
   * @param tableName the name of the table (cannot be <tt>null</tt>)
   * @return a list of {@code ColumnMetaData}
   * @throws SQLException
   * @see ColumnMetaData
   * @see DatabaseMetaData#getColumns(String, String, String, String)
   */
  @Nonnull
  public List<ColumnMetaData> getColumns(@Nonnull final String tableName) throws SQLException {
    return DBMetaDatas.getColumns(databaseMetaData, null, null, tableName, "%");
  }

  /**
   * Retrieves a description of table columns available in this database matching the specified
   * schema and table name. The schema parameter is optional, if not specified the comparison is
   * omitted. The returned list of {@code ColumnMetaData} are ordered by catalog, schema, table
   * name, and finally by ordinal position. <br>
   * A {@code ColumnMetaData} contains the meta information of a database column i.e. the index,
   * name, SQL type, precision, scale and nullability.<br>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> this method is supported by odbc-jdbc driver with MS Access database.
   *
   * @param schema the name of the schema (may be <tt>null</tt>)
   * @param tableName the name of the table (cannot be <tt>null</tt>)
   * @return a list of {@code ColumnMetaData}
   * @throws SQLException
   * @see ColumnMetaData
   * @see DatabaseMetaData#getColumns(String, String, String, String)
   */
  @Nonnull
  public List<ColumnMetaData> getColumns(final String schema, @Nonnull final String tableName)
      throws SQLException {
    return DBMetaDatas.getColumns(databaseMetaData, null, schema, tableName, "%");
  }

  /**
   * Retrieves a description of table columns available in this database matching the specified
   * catalog, schema, and table name. All the parameters except the table name are optional, if not
   * specified the comparison is omitted. The returned list of {@code ColumnMetaData} are ordered by
   * catalog, schema, table name, and finally by ordinal position. <br>
   * A {@code ColumnMetaData} contains the meta information of a database column i.e. the index,
   * name, SQL type, precision, scale and nullability.<br>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> this method is supported by odbc-jdbc driver with MS Access database.
   *
   * @param catalog the name of the catalog
   * @param schema the name of the schema
   * @param tableName the name of the table (cannot be <tt>null</tt>)
   * @return a list of {@code ColumnMetaData}
   * @throws SQLException
   * @see ColumnMetaData
   * @see DatabaseMetaData#getColumns(String, String, String, String)
   */
  @Nonnull
  public List<ColumnMetaData> getColumns(final String catalog, final String schema,
      final String tableName) throws SQLException {
    return DBMetaDatas.getColumns(databaseMetaData, catalog, schema, tableName, "%");
  }

  @Nonnull
  public List<ColumnMetaData> getColumns(@Nonnull final TableDesc tableDesc) throws SQLException {
    return DBMetaDatas.getColumns(databaseMetaData, tableDesc.getCatalog(), tableDesc.getSchema(),
        tableDesc.getName(), "%");
  }
}
