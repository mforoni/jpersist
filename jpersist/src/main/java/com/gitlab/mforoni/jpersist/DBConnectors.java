package com.gitlab.mforoni.jpersist;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import com.google.common.base.Preconditions;

/**
 *
 * @author Foroni Marco
 * @see DBConnector
 */
public final class DBConnectors {
  private static final String URL = "URL";
  private static final String USERNAME = "USERNAME";
  private static final String PASSWORD = "PASSWORD";

  private DBConnectors() {
    throw new AssertionError();
  }

  public static DBConnector newDBConnector(@Nonnull final File properties) throws SQLException {
    final Properties p = new Properties();
    try (final FileInputStream fis = new FileInputStream(properties)) {
      p.load(fis);
    } catch (final IOException e) {
      throw new IllegalArgumentException(e);
    }
    return newDBConnector(p);
  }

  public static DBConnector newDBConnector(@Nonnull final Properties properties)
      throws SQLException {
    final String url = properties.getProperty(URL);
    final String user = properties.getProperty(USERNAME);
    final String password = properties.getProperty(PASSWORD);
    return newDBConnector(url, user, password);
  }

  public static DBConnector newDBConnector(@Nonnull final String url, @Nullable final String user,
      @Nullable final String password) throws SQLException {
    return newDBConnector(url, user, password, false);
  }

  /**
   * Create a new instance of DBConnector.
   *
   * @param url the url
   * @param user
   * @param password
   * @return
   * @throws SQLException
   */
  public static DBConnector newDBConnector(@Nonnull final String url, @Nullable final String user,
      @Nullable final String password, final boolean readonly) throws SQLException {
    Preconditions.checkNotNull(url);
    final Driver driver = Driver.parseUrl(url);
    DriverCache.INSTANCE.initializeJdbcDriver(driver);
    final Connection connection = DriverManager.getConnection(url, user, password);
    return new SimpleDBConnector(connection, readonly);
  }
}
