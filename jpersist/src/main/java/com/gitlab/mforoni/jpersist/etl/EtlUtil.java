package com.gitlab.mforoni.jpersist.etl;

import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gitlab.mforoni.jpersist.ColumnMetaData;
import com.gitlab.mforoni.jpersist.DBRow;
import com.gitlab.mforoni.jpersist.DBTable;
import com.gitlab.mforoni.jpersist.PKMetaData;
import com.gitlab.mforoni.jpersist.PrimaryKey;

/**
 * @author Foroni
 */
public final class EtlUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(EtlUtil.class);
	private static final String WARN_MSG_COLUMN_NOT_FOUND = "La colonna {} del database TARGET non è stata trovata nel database SOURCE!";
	private static final String WARN_MSG_DIFFERENT_METADATA = "La colonna {} del database TARGET presenta metadato {} differente dal database SOURCE!";

	public enum DefaultValue {
		NULL, SOURCE
	}

	private EtlUtil() {} // Preventing instantiation

	/**
	 * Check preconditions so as to ensure the success of the import. It follows the list of controls made:
	 * <ul>
	 * <li>for each column of the target {@code DBTable}, it checks if there is a column with the same name in
	 * the source {@code DBTable}</li>
	 * <li>it also checks if the {@code ColumnMetaData} of the target can contain the source.</li>
	 * </ul>
	 *
	 * @return
	 * @see DBTable
	 * @see ColumnMetaData
	 */
	public static boolean checkPreconditions(@Nonnull final DBTable source, @Nonnull final DBTable target) {
		boolean ret = true;
		for (final ColumnMetaData targetColumn : target.getColumnMetaDatas()) {
			final ColumnMetaData sourceColumn = source.getColumnMetaData(targetColumn.getColumnName());
			if (sourceColumn == null) {
				// Colonna non trovata: l'import può essere eseguito
				LOGGER.warn(WARN_MSG_COLUMN_NOT_FOUND, targetColumn);
			} else {
				// Controllo che i ColumnMetaData del target siano compatibili col source:
				if (sourceColumn.getSQLTypeCode() != targetColumn.getSQLTypeCode()) {
					LOGGER.warn(WARN_MSG_DIFFERENT_METADATA, targetColumn, "SQL type");
					ret = false;
				}
				if (sourceColumn.getPrecision() > targetColumn.getPrecision()) {
					LOGGER.warn(WARN_MSG_DIFFERENT_METADATA, targetColumn, "precision");
					ret = false;
				}
				if (sourceColumn.getScale() > targetColumn.getScale()) {
					LOGGER.warn(WARN_MSG_DIFFERENT_METADATA, targetColumn, "scale");
					ret = false;
				}
				if (sourceColumn.getNullable() != targetColumn.getNullable()) {
					LOGGER.warn(WARN_MSG_DIFFERENT_METADATA, targetColumn, "nullable");
					ret = false;
				}
			}
		}
		return ret;
	}

	public static DBRow generateTargetRow(final DBTable targetTable, final PrimaryKey targetPK, final DBRow sourceRow) {
		if (!targetPK.getMetaData().equals(targetTable.getPKMetaData())) {
			throw new IllegalArgumentException();
		}
		final DBRow targetRow = targetTable.newDBRow();
		final PKMetaData targetPKMD = targetTable.getPKMetaData();
		for (final ColumnMetaData targetColumnMetaData : targetTable.getColumnMetaDatas()) {
			final String targetColumnName = targetColumnMetaData.getColumnName();
			if (targetPKMD.containsColumn(targetColumnName)) {
				targetRow.setObject(targetColumnName, targetPK.getObject(targetColumnName));
			} else {
				final ColumnMetaData sourceColumnMetaData = sourceRow.getDBTable().getColumnMetaData(targetColumnName);
				if (sourceColumnMetaData == null) {
					// Campo non presente nel DBTable Source inserimento valore null:
					LOGGER.warn("Colonna {} non presente sul DBTable {} del SOURCE. Inserito valore null.", targetColumnName,
							sourceRow.getDBTable().getTableDescription());
				} else {
					targetRow.setObject(targetColumnName, sourceRow.getObject(targetColumnName));
				}
			}
		}
		return targetRow;
	}

	public static DBRow generateEmptyRow(final DBTable targetTable, final PrimaryKey targetPK) {
		if (!targetPK.getMetaData().equals(targetTable.getPKMetaData())) {
			throw new IllegalArgumentException();
		}
		final DBRow targetRow = targetTable.newDBRow();
		for (final Map.Entry<String, Object> pkEntry : targetPK.getEntries()) {
			targetRow.setObject(pkEntry.getKey(), pkEntry.getValue());
		}
		return targetRow;
	}

	@Deprecated
	public static @Nonnull DBRow generateTargetRow(@Nonnull final DBTable targetTable, @Nonnull final DBRow sourceRow,
			@Nonnull final DefaultValue defaultValue, @Nullable final Map<String, Object> forcedValues) {
		final DBRow targetRow = targetTable.newDBRow();
		final PKMetaData targetPKMD = targetTable.getPKMetaData();
		for (final ColumnMetaData targetColumnMetaData : targetTable.getColumnMetaDatas()) {
			final String targetColumnName = targetColumnMetaData.getColumnName();
			final ColumnMetaData sourceColumnMetaData = sourceRow.getDBTable().getColumnMetaData(targetColumnName);
			if (sourceColumnMetaData == null) {
				// Campo non presente nel DBTableSource inserimento valore null:
				LOGGER.warn("Colonna {} non presente sul DBTableSource. Inserito valore null.", targetColumnName);
				targetRow.setObject(targetColumnName, null);
			} else {
				assert (targetColumnName.equals(sourceColumnMetaData.getColumnName()));
				final Object sourceValue = sourceRow.getObject(targetColumnName);
				if (targetPKMD.containsColumn(targetColumnName)) {
					// FIXME do nothing ???
				} else if (forcedValues != null && forcedValues.containsKey(targetColumnName)) {
					final Object targetValue = forcedValues.get(targetColumnName);
					targetRow.setObject(targetColumnName, targetValue);
				} else {
					switch (defaultValue) {
						case NULL:
							targetRow.setObject(targetColumnName, null);
							break;
						case SOURCE:
							targetRow.setObject(targetColumnName, sourceValue);
							break;
					}
				}
			}
		}
		return targetRow;
	}
}
