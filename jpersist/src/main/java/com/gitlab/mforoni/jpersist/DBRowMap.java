package com.gitlab.mforoni.jpersist;

import static com.google.common.base.Preconditions.checkNotNull;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map.Entry;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import com.github.mforoni.jbasic.JExceptions;
import com.github.mforoni.jbasic.JNumbers;
import com.gitlab.mforoni.jpersist.query.Select;

/**
 * Mutable class that represents a row (a record/tuple) of a {@link DBTable}.
 *
 * @author Marco Foroni
 * @see DBTable
 * @see PrimaryKey
 */
final class DBRowMap extends DBRow {
  private final DBTable dbTable;
  /**
   * Mapping row values by column name
   */
  private final IgnoreCaseMap<Value> map = new IgnoreCaseMap<>();
  private State state = State.NEW;
  private PrimaryKey originalPK = null;

  /**
   * Constructs a {@code DBRow} with State {@link State#NEW}.
   *
   * @param dbTable the database table
   * @see DBTable
   * @see State
   */
  DBRowMap(@Nonnull final DBTable dbTable) {
    checkNotNull(dbTable);
    this.dbTable = dbTable;
  }

  /**
   * Constructs a {@code DBRow} with State {@link State#SPECULAR} filled with the data retrieved
   * from the {@code ResultSet}. As long as is not modified, it maintains the state
   * {@link State#SPECULAR}. When the client modifies one of the fields the state automatically
   * switches to {@link State#UPDATED}.
   *
   * @param dbTable the database table
   * @param rs the {@code ResultSet}
   * @param database the database
   * @throws SQLException the SQL exception
   * @see DBTable
   * @see ResultSet
   * @see DBMetaData
   * @see State
   */
  DBRowMap(@Nonnull final DBTable dbTable, @Nonnull final ResultSet rs,
      @Nonnull final DBMetaData database) throws SQLException {
    this(dbTable);
    checkNotNull(rs);
    checkNotNull(database);
    state = State.SPECULAR;
    for (final ColumnMetaData columnMetaData : dbTable.getColumnMetaDatas()) {
      final Object value =
          ResultSets.getValue(rs, columnMetaData.getColumnIndex(), columnMetaData, database);
      map.put(columnMetaData.getColumnName(), new Value(value));
    }
    originalPK = PrimaryKey.valueOf(this);
  }

  /**
   * The mutable class value.
   */
  private class Value {
    @Nullable
    private Object value;
    private boolean modified;

    /**
     * Instantiates a new value as not modified.
     *
     * @param value the value
     */
    private Value(@Nullable final Object value) {
      this.value = value;
      this.modified = false;
    }

    @Nullable
    Object getValue() {
      return value;
    }

    void setValue(final Object value) {
      this.value = value;
      this.modified = true;
    }

    boolean isModified() {
      return modified;
    }

    @Override
    public String toString() {
      return "Value: ".concat(String.valueOf(value)).concat(", modified=") + modified;
    }
  }

  @Nonnull
  @Override
  public DBTable getDBTable() {
    return dbTable;
  }

  /**
   * Sets the state of this {@code DBRow}.
   *
   * @param state the new state of this {@code DBRow}
   * @see State
   */
  @Override
  protected void setState(@Nonnull final State state) {
    if (state == State.SPECULAR) {
      originalPK = PrimaryKey.valueOf(this);
    }
    this.state = state;
  }

  @Nonnull
  @Override
  public State getState() {
    return state;
  }

  @Nullable
  public ColumnMetaData getColumnMetaData(@Nonnull final String columnName) {
    return dbTable.getColumnMetaData(columnName);
  }

  @Override
  public boolean isModified(@Nonnull final String columnName) {
    checkColumnName(columnName);
    final Value value = map.get(columnName);
    return value.isModified();
  }

  /**
   * Check if the specified column name is defined in the column meta data of the {@code DBTable}
   * associated with this {@code DBRow}.
   *
   * @param columnName the column name to check
   * @throws IllegalArgumentException if the specified column name is not defined in the column meta
   *         data of the {@code DBTable} associated with this {@code DBRow}.
   */
  private void checkColumnName(@Nonnull final String columnName) {
    if (!dbTable.containsColumn(columnName)) {
      throw new IllegalArgumentException(String.format(
          "The specified column name %s is not defined in the meta data of this DBRow",
          columnName));
    }
  }

  @Override
  public int getSize() {
    return dbTable.getColumns();
  }

  @Override
  public PrimaryKey getOriginalPK() {
    return originalPK;
  }

  @Override
  public PrimaryKey getPrimaryKey() {
    return PrimaryKey.valueOf(this);
  }

  /**
   * Sets the primary key value. FIXME richiede approfondimenti
   *
   * @param primaryKey the new primary key value to set
   * @throws IllegalArgumentException it the primary key meta data of the specified
   *         {@code PrimaryKey} is not the same of this {@code DBRow}
   * @see PrimaryKey
   */
  void setPrimaryKey(@Nonnull final PrimaryKey primaryKey) {
    // non credo vada reso pubblico è pericoloso permettere di settare una nuova primary key su un
    // record letto da DB
    if (!primaryKey.getMetaData().equals(dbTable.getPKMetaData())) {
      throw new IllegalArgumentException(
          "The primary key meta data of the specified PK value is not the same of this DBRow");
    }
    for (final Entry<String, Object> entry : primaryKey.getEntries()) {
      setObject(entry.getKey(), entry.getValue());
    }
  }

  Value getValue(final String columnName) throws IllegalArgumentException {
    checkColumnName(columnName);
    return map.get(columnName);
  }

  @Nullable
  @Override
  public Object getObject(final String columnName) throws IllegalArgumentException {
    checkColumnName(columnName);
    final Value value = map.get(columnName);
    return value == null ? null : value.getValue();
  }

  @Nullable
  @Override
  public Long getLong(final String columnName) throws IllegalArgumentException {
    final Object value = getObject(columnName);
    if (value == null) {
      return null;
    } else if (value instanceof BigDecimal) { // FIXME ???
      return ((BigDecimal) value).longValue();
    } else if (value instanceof Long) {
      return (Long) value;
    } else {
      throw new IllegalStateException(
          "Cannot cast " + value.getClass().getSimpleName() + " to Long");
    }
  }

  @Nullable
  @Override
  public Integer getInteger(final String columnName) throws IllegalArgumentException {
    final Object value = getObject(columnName);
    if (value == null) {
      return null;
    } else if (value instanceof BigDecimal) { // FIXME ???
      return ((BigDecimal) value).intValue();
    } else if (value instanceof Integer) {
      return (Integer) value;
    } else {
      throw new IllegalStateException(
          "Cannot cast " + value.getClass().getSimpleName() + " to Integer");
    }
  }

  @Override
  public int getInt(final String columnName) throws IllegalArgumentException {
    final Integer value = getInteger(columnName);
    if (value == null) {
      throw new NullPointerException();
    }
    return value.intValue();
  }

  @Nullable
  @Override
  public String getString(final String columnName) throws IllegalArgumentException {
    final Object value = getObject(columnName);
    if (value == null) {
      return null;
    } else if (value instanceof String) {
      return (String) value;
    } else {
      throw new IllegalStateException(String.format("%s.%s: cannot cast %s to String",
          dbTable.getTableDescription().getName(), columnName, value.getClass().getSimpleName()));
    }
  }

  @Nullable
  @Override
  public BigDecimal getBigDecimal(final String columnName) throws IllegalArgumentException {
    final Object value = getObject(columnName);
    if (value == null) {
      return null;
    } else if (value instanceof BigDecimal) {
      return (BigDecimal) value;
    } else {
      throw new IllegalStateException(
          "Cannot cast " + value.getClass().getSimpleName() + " to BigDecimal");
    }
  }

  @Override
  public void setObject(@Nonnull final String columnName, @Nullable final Object value) {
    checkColumnName(columnName);
    // check if the value has type compatible with the database type
    checkType(columnName, value);
    Value v = map.get(columnName);
    if (v == null) {
      v = new Value(value);
      map.put(columnName, v);
    } else {
      v.setValue(value);
      if (state == State.SPECULAR) {
        state = State.UPDATED;
      }
    }
  }

  private void checkType(@Nonnull final String columnName, @Nullable final Object value) {
    if (value == null) {
      return;
    }
    final ColumnMetaData columnMetaData = getColumnMetaData(columnName);
    if (!columnMetaData.getSQLType().acceptType(value.getClass())) {
      throw JExceptions.newIllegalState(
          "Cannot set value %s of type %s on column %s having type %s", value, value.getClass(),
          columnMetaData.getColumnName(), columnMetaData.getSQLType());
    }
  }

  /**
   * Set the primary key to the next available numeric value. This {@code DBRow} must have state
   * equal to {@link State#NEW} and the primary key of the related database table must have size
   * one.
   */
  @Deprecated
  public void setNextPrimaryKey() {
    if (state != State.NEW) {
      throw new IllegalStateException(
          "Cannot set the primary key on a DBRow with state different from NEW");
    }
    if (dbTable.getPKMetaData().getSize() > 1) {
      throw new IllegalStateException(
          "Cannot automatically retrieve and set the primary key if the size is not 1");
    }
    final String pkColumnName = dbTable.getPKMetaData().getColumns().get(0).getName();
    final Select query = new Select.Builder(null, dbTable.getTableDescription().getQualifiedName(),
        Select.max(pkColumnName)).toSelect();
    try {
      final Number number = dbTable.getDBConnector().executeSelectNumber(query);
      setObject(pkColumnName, JNumbers.add(number, 1));
    } catch (final SQLException ex) {
      throw new IllegalStateException(
          "Cannot set the primary key value on DBRow " + this.toString(), ex);
    }
  }

  @Override
  public String getDescription() {
    final StringBuilder sb = new StringBuilder(25 * dbTable.getColumnMetaDatas().size());
    sb.append(toString()).append(": ");
    final Iterator<ColumnMetaData> iterator = dbTable.getColumnMetaDatas().iterator();
    if (iterator.hasNext()) {
      ColumnMetaData columnMetaData = iterator.next();
      sb.append(columnMetaData.getColumnName()).append('=')
          .append(String.valueOf(getObject(columnMetaData.getColumnName())));
      while (iterator.hasNext()) {
        columnMetaData = iterator.next();
        sb.append(", ").append(columnMetaData.getColumnName()).append('=')
            .append(String.valueOf(getObject(columnMetaData.getColumnName())));
      }
    }
    return sb.toString();
  }

  @Override
  public String toString() {
    return new StringBuilder("DBRow of ").append(dbTable.getTableDescription().getQualifiedName())
        .append(" (").append(originalPK).append(", ").append(state).append(')').toString();
  }

  @Override
  public int persist(final DBConnector dbConnector) throws SQLException {
    switch (state) {
      case NEW:
        return dbConnector.insert(this);
      case UPDATED:
        return dbConnector.update(this);
      case SPECULAR:
        return 0;
      default:
        throw new AssertionError();
    }
  }
}
