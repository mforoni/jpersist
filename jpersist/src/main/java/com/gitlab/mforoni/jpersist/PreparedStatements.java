package com.gitlab.mforoni.jpersist;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map.Entry;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.mforoni.jbasic.JExceptions;
import com.github.mforoni.jbasic.MoreInts;
import com.google.common.annotations.Beta;
import com.google.common.base.Preconditions;

/**
 * This class consists of {@code static} utility methods for setting parameters into a
 * {@link PreparedStatement}.
 *
 * @author Foroni
 */
@Beta
final class PreparedStatements {
  private static final Logger LOGGER = LoggerFactory.getLogger(PreparedStatements.class);

  // Suppresses default constructor, ensuring non-instantiability.
  private PreparedStatements() {
    throw new AssertionError();
  }

  @Beta
  static int setUpdateStatementParameters(@Nonnull final PreparedStatement ps,
      @Nonnull final DBRow dbRow) throws SQLException {
    int index = 1;
    ps.clearParameters();
    for (final ColumnMetaData columnMetaData : dbRow.getDBTable().getColumnMetaDatas()) {
      final Object value = dbRow.getObject(columnMetaData.getColumnName());
      setParameter(ps, index++, value, columnMetaData);
    }
    // setting primary key backup values on where condition
    final PrimaryKey primaryKey = dbRow.getOriginalPK();
    for (final Entry<String, Object> entry : primaryKey.getEntries()) {
      setParameter(ps, index++, entry.getValue(),
          dbRow.getDBTable().getColumnMetaData(entry.getKey()));
    }
    return index;
  }

  @Beta
  static int setUpdateStatementParametersForModifiedValues(@Nonnull final PreparedStatement ps,
      @Nonnull final DBRow dbRow) throws SQLException {
    int index = 1;
    ps.clearParameters();
    for (final ColumnMetaData columnMetaData : dbRow.getDBTable().getColumnMetaDatas()) {
      if (dbRow.isModified(columnMetaData.getColumnName())) {
        final Object value = dbRow.getObject(columnMetaData.getColumnName());
        setParameter(ps, index++, value, columnMetaData);
      }
    }
    // setting primary key backup values on where condition
    final PrimaryKey primaryKey = dbRow.getOriginalPK();
    for (final Entry<String, Object> entry : primaryKey.getEntries()) {
      setParameter(ps, index++, entry.getValue(),
          dbRow.getDBTable().getColumnMetaData(entry.getKey()));
    }
    return index;
  }

  @Beta
  static int setInsertStatementParameters(@Nonnull final PreparedStatement ps,
      @Nonnull final DBRow dbRow) throws SQLException {
    int index = 1;
    ps.clearParameters();
    for (final ColumnMetaData columnMetaData : dbRow.getDBTable().getColumnMetaDatas()) {
      final Object value = dbRow.getObject(columnMetaData.getColumnName());
      setParameter(ps, index++, value, columnMetaData);
    }
    return index;
  }

  /**
   * Sets the given list of parameters into the specified {@code PreparedStatement} invoking the
   * method {@link PreparedStatement#setObject(int, Object)}.
   * 
   *
   * @param ps the {@code PreparedStatement}
   * @param parameters the list of parameters
   * @return the next index to be set
   * @throws SQLException
   * @see PreparedStatement
   */
  public static int setParameters(final PreparedStatement ps,
      @Nonnull final List<Object> parameters) throws SQLException {
    ps.clearParameters();
    return setParameters(ps, 1, parameters);
  }

  public static int setParameters(final PreparedStatement ps, final int startIndex,
      @Nonnull final List<Object> parameters) throws SQLException {
    Preconditions.checkNotNull(parameters);
    int index = startIndex;
    for (final Object param : parameters) {
      ps.setObject(index++, param);
    }
    return index;
  }

  public static int setParameters(final PreparedStatement ps, @Nonnull final Object[] parameters)
      throws SQLException {
    ps.clearParameters();
    return setParameters(ps, 1, parameters);
  }

  public static int setParameters(final PreparedStatement ps, final int startIndex,
      @Nonnull final Object[] parameters) throws SQLException {
    Preconditions.checkNotNull(parameters);
    int index = startIndex;
    for (final Object param : parameters) {
      ps.setObject(index++, param);
    }
    return index;
  }

  /**
   * Sets the {@code value} into the specified {@code PreparedStatement} at the position
   * {@code index} according to the specified {@code ColumnMetaData}.
   * <p>
   * Useful resources:
   * https://stackoverflow.com/questions/5686514/jdbc-how-to-set-char-in-a-prepared-statement
   * https://stackoverflow.com/questions/18614836/using-setdate-in-preparedstatement
   *
   * @param ps the {@code PreparedStatement}
   * @param index the parameter index
   * @param columnMetaData the {@code ColumnMetaData} of the corresponding field
   * @param value the value to set
   * @throws SQLException
   * @see PreparedStatement
   * @see ColumnMetaData
   */
  public static void setParameterCheckingType(@Nonnull final PreparedStatement ps, final int index,
      @Nullable final Object value, @Nonnull final ColumnMetaData columnMetaData)
      throws SQLException {
    final SQLType sqlType = columnMetaData.getSQLType();
    if (value == null) {
      ps.setNull(index, sqlType.getCode());
    } else {
      if (sqlType.getJavaType().equals(value.getClass())) {
        setObject(ps, value, index, columnMetaData);
      } else if (sqlType == SQLType.DATE) {
        setDate(ps, value, index, columnMetaData);
      } else if (sqlType == SQLType.TIMESTAMP) {
        setTimestamp(ps, value, index, columnMetaData);
      } else {
        throw JExceptions.newIllegalState(
            "SQL type %s (Column type name = %s): case not handled. Unable to set parameter.",
            sqlType, columnMetaData.getColumnTypeName());
      }
    }
  }

  public static void setParameter(@Nonnull final PreparedStatement ps, final int index,
      @Nullable final Object value, @Nonnull final ColumnMetaData columnMetaData)
      throws SQLException {
    final SQLType sqlType = columnMetaData.getSQLType();
    if (value == null) {
      ps.setNull(index, sqlType.getCode());
    } else {
      if (sqlType == SQLType.DATE) {
        setDate(ps, value, index, columnMetaData);
      } else if (sqlType == SQLType.TIMESTAMP) {
        setTimestamp(ps, value, index, columnMetaData);
      } else if (sqlType.acceptType(value.getClass())) {
        setObject(ps, value, index, columnMetaData);
      } else {
        throw JExceptions.newIllegalState(
            "SQL type %s (Column type name = %s): case not handled. Unable to set parameter.",
            sqlType, columnMetaData.getColumnTypeName());
      }
    }
  }

  private static void setObject(final PreparedStatement ps, final Object value, final int index,
      final ColumnMetaData columnMetaData) throws SQLException {
    try {
      LOGGER.debug("Set {} at {} on {}", value, index, ps);
      ps.setObject(index, value);
    } catch (final SQLException e) {
      final String msg = getErrorMessage(value, columnMetaData);
      LOGGER.error(msg);
      throw new IllegalStateException(msg, e);
    }
  }

  private static void setBoolean(final PreparedStatement ps, final Object value, final int index,
      final ColumnMetaData columnMetaData) throws SQLException {
    if (value instanceof Boolean) {
      LOGGER.debug("Set {} at {} on {}", value, index, ps);
      ps.setBoolean(index, (Boolean) value);
    } else {
      final String msg = getErrorMessage(value, columnMetaData);
      LOGGER.error(msg);
      throw new IllegalStateException(msg);
    }
  }

  private static void setString(final PreparedStatement ps, final Object value, final int index,
      final ColumnMetaData columnMetaData) throws SQLException {
    if (value instanceof String) {
      LOGGER.debug("Set {} at {} on {}", value, index, ps);
      ps.setString(index, (String) value);
    } else {
      final String msg = getErrorMessage(value, columnMetaData);
      LOGGER.error(msg);
      throw new IllegalStateException(msg);
    }
  }

  private static void setInteger(final PreparedStatement ps, final Object value, final int index,
      final ColumnMetaData columnMetaData) throws SQLException {
    if (value instanceof Integer) {
      LOGGER.debug("Set {} at {} on {}", value, index, ps);
      ps.setInt(index, (Integer) value);
    } else {
      final String msg = getErrorMessage(value, columnMetaData);
      LOGGER.error(msg);
      throw new IllegalStateException(msg);
    }
  }

  private static void setDouble(final PreparedStatement ps, final Object value, final int index,
      final ColumnMetaData columnMetaData) throws SQLException {
    if (value instanceof Double) {
      LOGGER.debug("Set {} at {} on {}", value, index, ps);
      ps.setDouble(index, (Double) value);
    } else {
      final String msg = getErrorMessage(value, columnMetaData);
      LOGGER.error(msg);
      throw new IllegalStateException(msg);
    }
  }

  private static void setNumericToMSAccess(final PreparedStatement ps, final Object value,
      final int index) throws SQLException {
    if (value instanceof Integer) {
      ps.setInt(index, (Integer) value);
    } else if (value instanceof Long) {
      // Only setInt implemented: safely cast to int throwing exception if can't contain
      // Math.toIntExact() requires jdk 1.8
      ps.setInt(index, MoreInts.toIntExact((Long) value));
    } else {
      final String msg = String.format(
          "Case not handled for MSAccess DBMS, sql type NUMERIC and value %s having type %s", value,
          value.getClass().getSimpleName());
      throw new IllegalStateException(msg);
    }
  }

  private static void setDate(final PreparedStatement ps, Object value, final int index,
      final ColumnMetaData columnMetaData) throws SQLException {
    if (value instanceof LocalDate) {
      value = ((LocalDate) value).toDate();
    }
    if (value instanceof java.util.Date) {
      LOGGER.debug("Set {} at {} on {}", value, index, ps);
      final java.util.Date date = (java.util.Date) value;
      ps.setDate(index, new java.sql.Date(date.getTime()));
    } else {
      final String msg = getErrorMessage(value, columnMetaData);
      LOGGER.error(msg);
      throw new IllegalStateException(msg);
    }
  }

  private static void setTimestamp(final PreparedStatement ps, final Object value, final int index,
      final ColumnMetaData columnMetaData) throws SQLException {
    if (value instanceof java.sql.Timestamp) {
      LOGGER.debug("Set {} at {} on {}", value, index, ps);
      ps.setTimestamp(index, (java.sql.Timestamp) value);
    } else {
      final String msg = getErrorMessage(value, columnMetaData);
      LOGGER.error(msg);
      throw new IllegalStateException(msg);
    }
  }

  private static String getErrorMessage(final Object value, final ColumnMetaData columnMetaData) {
    return String.format(
        "Cannot set PreparedStatement value %s in the column %s: the sql type is %s but the value is type %s",
        value, columnMetaData.toString(), SQLType.parse(columnMetaData.getSQLTypeCode()),
        value.getClass().getSimpleName());
  }
}
