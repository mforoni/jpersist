package com.gitlab.mforoni.jpersist;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import com.gitlab.mforoni.jpersist.DBMetaData.DBMSName;

public final class ResultSets {
  // Suppresses default constructor, ensuring non-instantiability.
  private ResultSets() {
    throw new AssertionError();
  }

  @Deprecated
  @Nullable
  public static Object getValue(@Nonnull final ResultSet rs, final int columnIndex,
      @Nonnull final ColumnMetaData columnMetaData, @Nonnull final DBMetaData database)
      throws SQLException {
    return getValue(rs, columnIndex, columnMetaData.getSQLTypeCode(),
        columnMetaData.getColumnName(), columnMetaData.getColumnTypeName(), database);
  }

  @Deprecated
  @Nullable
  public static Object getValue(@Nonnull final ResultSet rs, final int columnIndex,
      @Nonnull final ResultSetMetaData rsmd, @Nonnull final DBMetaData database)
      throws SQLException {
    return getValue(rs, columnIndex, rsmd.getColumnType(columnIndex),
        rsmd.getColumnName(columnIndex), rsmd.getColumnTypeName(columnIndex), database);
  }

  @Deprecated
  @Nullable
  private static Object getValue(final ResultSet rs, final int columnIndex, final int sqlType,
      final String columnName, final String columnTypeName, final DBMetaData database)
      throws SQLException, IllegalStateException {
    switch (sqlType) {
      case java.sql.Types.BOOLEAN:
        return getBoolean(rs, columnIndex);
      case java.sql.Types.BIT:
        return getBoolean(rs, columnIndex);
      case java.sql.Types.DECIMAL:
        if (database.getDBMSName() == DBMSName.ACCESS) {
          return getInteger(rs, columnIndex);
        } else {
          return rs.getBigDecimal(columnIndex);
        }
      case java.sql.Types.NUMERIC:
        if (database.getDBMSName() == DBMSName.ACCESS) {
          return getInteger(rs, columnIndex);
        } else {
          return rs.getBigDecimal(columnIndex);
        }
      case java.sql.Types.INTEGER:
        return getInteger(rs, columnIndex);
      case java.sql.Types.BIGINT:
        throw new IllegalStateException("Sql type BIGINT not handled for column " + columnName);
      case java.sql.Types.DOUBLE:
        return getDouble(rs, columnIndex);
      case java.sql.Types.LONGVARCHAR:
        return rs.getString(columnIndex);
      case java.sql.Types.VARCHAR:
        return rs.getString(columnIndex);
      case java.sql.Types.CHAR:
        return rs.getString(columnIndex);
      case java.sql.Types.DATE:
        final java.sql.Date date = rs.getDate(columnIndex);
        return rs.wasNull() ? null : new java.util.Date(date.getTime());
      case java.sql.Types.TIMESTAMP:
        final java.sql.Timestamp timestamp = rs.getTimestamp(columnIndex);
        return rs.wasNull() ? null : timestamp;
      default:
        throw new IllegalStateException(String.format(
            "ColumnIndex=%s: SQL type %s (Column type name = %s) not handled. Unable to retrieve data from DB.",
            columnIndex, sqlType, columnTypeName));
    }
  }

  @Nullable
  public static Boolean getBoolean(final ResultSet rs, final int columnIndex) throws SQLException {
    final boolean b = rs.getBoolean(columnIndex);
    return rs.wasNull() ? null : b;
  }

  @Nullable
  public static Integer getInteger(final ResultSet rs, final int columnIndex) throws SQLException {
    final int i = rs.getInt(columnIndex);
    return rs.wasNull() ? null : i;
  }

  @Nullable
  public static Double getDouble(final ResultSet rs, final int columnIndex) throws SQLException {
    final double d = rs.getDouble(columnIndex);
    return rs.wasNull() ? null : d;
  }
}
