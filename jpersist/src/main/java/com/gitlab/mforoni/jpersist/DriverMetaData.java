package com.gitlab.mforoni.jpersist;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import com.google.common.annotations.Beta;

@Beta
public final class DriverMetaData {

  private final String name;
  private final String version;
  private final int major;
  private final int minor;

  public DriverMetaData(final DatabaseMetaData databaseMetaData) throws SQLException {
    this(databaseMetaData.getDriverName(), databaseMetaData.getDriverVersion(),
        databaseMetaData.getDriverMajorVersion(), databaseMetaData.getDriverMinorVersion());
  }

  public DriverMetaData(String name, String version, int major, int minor) {
    super();
    this.name = name;
    this.version = version;
    this.major = major;
    this.minor = minor;
  }

  public String getName() {
    return name;
  }

  public String getVersion() {
    return version;
  }

  public int getMajor() {
    return major;
  }

  public int getMinor() {
    return minor;
  }

  @Override
  public String toString() {
    return name;
  }
}
