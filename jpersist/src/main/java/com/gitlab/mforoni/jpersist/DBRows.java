package com.gitlab.mforoni.jpersist;

import java.sql.SQLException;
import java.util.List;
import javax.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gitlab.mforoni.jpersist.PKMetaData.PKColumn;
import com.gitlab.mforoni.jpersist.query.Query.Operator;
import com.gitlab.mforoni.jpersist.query.Where;
import com.google.common.annotations.Beta;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

public final class DBRows {
  private static final Logger LOGGER = LoggerFactory.getLogger(DBRows.class);

  private DBRows() {
    throw new AssertionError();
  }

  public static Where equalsPK(final DBRow dbRow) {
    final PrimaryKey originalPK = dbRow.getOriginalPK();
    if (originalPK.getSize() < 1) {
      throw new IllegalStateException();
    }
    final List<PKColumn> pkColumns = originalPK.getMetaData().getColumns();
    final Where.Builder wb = new Where.Builder(pkColumns.get(0).getName(), Operator.EQUAL,
        dbRow.getObject(pkColumns.get(0).getName()));
    for (int i = 1; i < pkColumns.size(); i++) {
      final PKColumn pkc = pkColumns.get(i);
      wb.and(pkc.getName(), Operator.EQUAL, dbRow.getObject(pkc.getName()));
    }
    return wb.toWhere();
  }

  /**
   *
   * @param dbRows the list of {@code DBRow} objects to be inserted or updated
   * @return
   * @throws SQLException
   * @throws UnsupportedOperationException
   * @throws IllegalArgumentException
   */
  public static int persist(@Nonnull final DBConnector dbConnector,
      @Nonnull final List<DBRow> dbRows) throws SQLException {
    int ins = 0, upd = 0;
    for (final DBRow dbRow : dbRows) {
      switch (dbRow.getState()) {
        case SPECULAR:
          // do nothing
          break;
        case NEW:
          ins += dbConnector.insert(dbRow);
          break;
        case UPDATED:
          upd += dbConnector.update(dbRow);
          break;
        default:
          throw new AssertionError();
      }
    }
    final String qualifiedName =
        dbRows.get(0).getDBTable().getTableDescription().getQualifiedName();
    LOGGER.debug("Table {}: {} rows inserted, {} rows updated", qualifiedName, ins, upd);
    return ins + upd;
  }

  /**
   * Create a new {@link DBRow} as copy of all non primary key values from the specified
   * {@code DBRow}.
   *
   * @param dbRow the {@code DBRow} from which copying the values
   * @see DBRow
   */
  @Beta
  public static DBRow clone(final DBRow dbRow) {
    final DBRow newDBRow = dbRow.getDBTable().newDBRow();
    final PKMetaData pkmd = dbRow.getDBTable().getPKMetaData();
    for (final ColumnMetaData columnMetaData : dbRow.getDBTable().getColumnMetaDatas()) {
      if (!pkmd.containsColumn(columnMetaData.getColumnName())) {
        newDBRow.setObject(columnMetaData.getColumnName(),
            dbRow.getObject(columnMetaData.getColumnName()));
      }
    }
    return newDBRow;
  }

  @Beta
  public static Table<PrimaryKey, String, Object> asTable(@Nonnull final List<DBRow> rows) {
    final Table<PrimaryKey, String, Object> records = HashBasedTable.create();
    for (final DBRow row : rows) {
      final PrimaryKey pk = PrimaryKey.valueOf(row);
      for (final ColumnMetaData cmd : row.getDBTable().getColumnMetaDatas()) {
        records.put(pk, cmd.getColumnName(), row.getObject(cmd.getColumnName()));
      }
    }
    return records;
  }
}
