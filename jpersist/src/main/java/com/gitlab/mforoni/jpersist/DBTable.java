package com.gitlab.mforoni.jpersist;

import static com.google.common.base.Preconditions.checkNotNull;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.mforoni.jbasic.util.JLogger;
import com.gitlab.mforoni.jpersist.PKMetaData.PKColumn;
import com.gitlab.mforoni.jpersist.query.Join;
import com.gitlab.mforoni.jpersist.query.OrderBy;
import com.gitlab.mforoni.jpersist.query.Query.JoinType;
import com.gitlab.mforoni.jpersist.query.Select;
import com.gitlab.mforoni.jpersist.query.Where;
import com.google.common.annotations.Beta;
import com.google.common.base.Stopwatch;

/**
 * Immutable class representing a database table. Contains the meta information of a database table
 * and a {@code DBConnector} for manipulating the database table content:
 * <ul>
 * <li>the {@link TableDesc} of this database table</li>
 * <li>the {@link PKMetaData} of this database table</li>
 * <li>all the {@link ColumnMetaData} of the columns defined in this database table, stored in a
 * {@code Map<String, ColumnMetaData>} to facilitate clients accessing these meta information (the
 * key is the column name).</li>
 * <li>a {@code DBConnector}
 * </ul>
 *
 * @author Marco Foroni
 * @see TableDesc
 * @see PKMetaData
 * @see ColumnMetaData
 * @see DBConnector
 */
@Immutable
public final class DBTable {
  private static final Logger LOGGER = LoggerFactory.getLogger(DBTable.class);
  @Nonnull
  private final DBConnector dbConnector;
  @Nonnull
  private final TableDesc tableDescription;
  @Nullable
  private final String alias;
  @Nonnull
  private final PKMetaData pkMetaData;
  /**
   * Mapping ColumnMetaDatas by column name<br>
   * Maybe is better mapping by column index but it doesn't make sense to have two columns with the
   * same name: see this <a href=
   * "http://stackoverflow.com/questions/8797593/is-there-any-use-to-duplicate-column-names-in-a-table">
   * link</a>
   */
  private final IgnoreCaseMap<ColumnMetaData> columnMetaDatas;

  /**
   * Instantiates a new database table.
   *
   * @param dbConnector the connector to the database
   * @param name the name of this table
   * @throws IllegalStateException if the metadata retrieval operations are not successful
   */
  public DBTable(@Nonnull final DBConnector dbConnector, @Nonnull final String name) {
    this(dbConnector, null, name, null);
  }

  /**
   * Instantiates a new database table.
   *
   * @param dbConnector the database connector
   * @param schema the schema of the table
   * @param name the name of the table
   * @throws IllegalStateException if the meta data retrieval operations are not successful
   */
  public DBTable(@Nonnull final DBConnector dbConnector, final String schema,
      @Nonnull final String name) {
    this(dbConnector, schema, name, null);
  }

  /**
   * Instantiates a new database table. For dealing with MS Access odbc jdbc driver unable to
   * retrieve primary key meta-data the client must provide a non null {@code PKMetaData}.
   *
   * @param dbConnector the database connector
   * @param schema the schema of the table
   * @param name the name of the table
   * @param pkMetaData the primary key meta information
   * @throws IllegalStateException if the meta data retrieval operations are not successful
   */
  public DBTable(@Nonnull final DBConnector dbConnector, @Nullable final String schema,
      @Nonnull final String name, @Nullable final String alias) {
    checkNotNull(dbConnector);
    checkNotNull(name);
    final Stopwatch stopwatch = Stopwatch.createStarted();
    this.dbConnector = dbConnector;
    this.alias = alias;
    final DBMetaData database = dbConnector.getDBMetaData();
    final List<ColumnMetaData> columnMetaDatas;
    try {
      this.tableDescription = database.getTable(null, schema, name);
      this.pkMetaData = database.getPKMetaData(schema, name);
      columnMetaDatas = database.getColumns(schema, name);
    } catch (final SQLException ex) {
      throw new IllegalStateException(String.format(
          "Unable to build DBTable for table %s: error while retrieving meta-data", name), ex);
    }
    if (columnMetaDatas == null || columnMetaDatas.size() == 0) {
      throw new IllegalStateException(String.format(
          "Unable to build DBTable for table %s: it seems to have no columns defined", name));
    }
    this.columnMetaDatas = new IgnoreCaseMap<>(new LinkedHashMap<String, ColumnMetaData>());
    for (final ColumnMetaData columnMetaData : columnMetaDatas) {
      this.columnMetaDatas.put(columnMetaData.getColumnName(), columnMetaData);
    }
    stopwatch.stop();
    LOGGER.debug("Created DBTable {}.{} alias {} in {}", schema, name, this.alias, stopwatch);
  }

  /**
   * Gets the DB connector.
   *
   * @return the DB connector
   */
  @Nonnull
  DBConnector getDBConnector() {
    return dbConnector;
  }

  @Nonnull
  public String getAlias() {
    return alias;
  }

  /**
   * Returns the {@code TableDescription} i.e. the meta information associated with this table.
   *
   * @return the {@code TableDescription} i.e. the meta information associated with this table.
   * @see TableDesc
   */
  @Nonnull
  public TableDesc getTableDescription() {
    return tableDescription;
  }

  /**
   * Returns the primary key of this database table.
   *
   * @return the primary key of this database table
   * @see PKMetaData
   */
  @Nonnull
  public PKMetaData getPKMetaData() {
    return pkMetaData;
  }

  /**
   * Returns the number of columns of this {@code DBTable}.
   *
   * @return the number of columns of this {@code DBTable}
   */
  public int getColumns() {
    return columnMetaDatas.size();
  }

  /**
   * Returns a {@code Set<String>} containing the names for all the columns defined in this database
   * table.
   *
   * @return a {@code Set<String>} containing the names for all the columns defined in this database
   *         table.
   * @see Set
   */
  @Nonnull
  public Set<String> getColumnNames() {
    return columnMetaDatas.keySet();
  }

  /**
   * Gets the column meta information for all the columns defined in this database table.
   *
   * @return the column meta information for all the columns defined in this database table
   * @see Collection
   */
  @Nonnull
  public Collection<ColumnMetaData> getColumnMetaDatas() {
    return columnMetaDatas.values();
  }

  /**
   * Returns the {@code ColumnMetaData} of the column with name {@code columnName}. If the table
   * does not contain such column returns <tt>null</tt>.
   * <p>
   * The time complexity is the same of {@code Map#get(Object)} operation.
   *
   * @param columnName the name of the column
   * @return the {@code ColumnMetaData} of the column with name {@code columnName}
   * @see ColumnMetaData
   */
  @Nonnull
  public ColumnMetaData getColumnMetaData(final String columnName) {
    final ColumnMetaData columnMetaData = columnMetaDatas.get(columnName);
    if (columnMetaData == null) {
      throw new IllegalArgumentException(String.format("Column name %s not defined in table %s",
          columnName, tableDescription.getName()));
    }
    return columnMetaData;
  }

  /**
   * Returns <tt>true</tt> if this {@code DBTable} contains the column having the specified name,
   * <tt>false</tt> otherwise .
   *
   * @param columnName the name of the column
   * @return <tt>true</tt> if this {@code DBTable} contains the column having the specified name,
   *         <tt>false</tt> otherwise
   */
  public boolean containsColumn(final String columnName) {
    final ColumnMetaData columnMetaData = columnMetaDatas.get(columnName);
    return (columnMetaData == null) ? false : true;
  }

  @Override
  public String toString() {
    return "DBTable " + tableDescription.getQualifiedName();
  }

  public String fullDescription() {
    return tableDescription.getQualifiedName().concat(" ").concat(dbConnector.toString());
  }

  /**
   * Returns a new empty {@code DBRow} with state {@link State#NEW}.
   *
   * @return a new empty {@code DBRow} with state {@code NEW}
   * @see DBRow
   * @see State
   */
  @Nonnull
  public DBRow newDBRow() {
    return new DBRowMap(this);
  }

  public Map<Integer, DBRow> toIntegerKeyMap() throws SQLException {
    final PKMetaData pkmd = getPKMetaData();
    if (pkmd.getSize() != 1) {
      throw new IllegalStateException();
    }
    return toIntegerKeyMap(pkmd.getColumns().get(0).getName());
  }

  public Map<Integer, DBRow> toIntegerKeyMap(final String columnName) throws SQLException {
    final Map<Integer, DBRow> map = new HashMap<>();
    final List<DBRow> rows = select();
    for (final DBRow row : rows) {
      map.put(row.getInt(columnName), row);
    }
    return map;
  }

  public Map<Long, DBRow> toLongKeyMap() throws SQLException {
    final PKMetaData pkmd = getPKMetaData();
    if (pkmd.getSize() != 1) {
      throw new IllegalStateException();
    }
    return toLongKeyMap(pkmd.getColumns().get(0).getName());
  }

  public Map<Long, DBRow> toLongKeyMap(final String columnName) throws SQLException {
    final Map<Long, DBRow> map = new HashMap<>();
    final List<DBRow> rows = select();
    for (final DBRow row : rows) {
      map.put(row.getLong(columnName), row);
    }
    return map;
  }

  @Beta
  @Deprecated
  @Nonnull
  Map<PrimaryKey, DBRow> toMap() throws SQLException {
    // TODO pericoloso perchè i valori memorizzati nella PrimaryKey hanno il tipo definito dal DB e
    // potrebbe non essere quello atteso dall'utilizzatore.
    // Vedi esempio MapEqualsDemo in progetto demo-java, atteso Integer ottenuto BigDecimal
    final Map<PrimaryKey, DBRow> map = new HashMap<>();
    final List<DBRow> rows = select();
    for (final DBRow row : rows) {
      final PrimaryKey primaryKey = PrimaryKey.valueOf(row);
      map.put(primaryKey, row);
    }
    return map;
  }

  /**
   * Select all the records of this {@code DBTable}. Returns a list of {@code DBRow} objects.
   *
   * @return all the records of this {@code DBTable} in a {@code List} of {@code DBRow}
   * @throws SQLException
   * @see DBRow
   */
  @Nonnull
  public List<DBRow> select() throws SQLException {
    return select(null, null, null);
  }

  /**
   * Select all the records of this {@code DBTable} ordered as specified. Returns a list of
   * {@code DBRow} objects.
   *
   * @param orderBy the order by condition
   * @return all the records of this {@code DBTable} in a {@code List} of {@code DBRow} ordered as
   *         specified
   * @throws SQLException
   * @see DBRow
   * @see OrderBy
   */
  @Nonnull
  public List<DBRow> select(final OrderBy orderBy) throws SQLException {
    return select(null, null, orderBy);
  }

  /**
   * Select the records of this {@code DBTable} that meet the specified condition. Returns a list of
   * {@code DBRow} objects.
   *
   * @param where the where condition
   * @return all the records of this {@code DBTable} that meet the specified condition in a
   *         {@code List} of {@code DBRow}
   * @throws SQLException
   * @see DBRow
   * @see Where
   */
  @Nonnull
  public List<DBRow> select(final Where where) throws SQLException {
    return select(null, where, null);
  }

  /**
   * Select the records of this {@code DBTable} that meet the specified condition ordered as
   * required. Returns a list of {@code DBRow} objects.
   *
   * @param where the where condition
   * @param orderBy the order by condition
   * @return all the records of this {@code DBTable} that meet the specified condition in a
   *         {@code List} of {@code DBRow} ordered as specified
   * @throws SQLException
   * @see DBRow
   * @see Where
   * @see OrderBy
   */
  @Nonnull
  public List<DBRow> select(final Where where, final OrderBy orderBy) throws SQLException {
    return select(null, where, orderBy);
  }

  @Nonnull
  public List<DBRow> select(final Join join) throws SQLException {
    return select(join, null, null);
  }

  @Nonnull
  public List<DBRow> select(final Join join, final Where where) throws SQLException {
    return select(join, where, null);
  }

  /**
   * Opens, uses and closes a {@code ResultSet} and a {@code PreparedStatement}.
   * 
   * @param join
   * @param where
   * @param orderBy
   * @return a list of {@code DBRow}
   * @throws SQLException
   */
  @Nonnull
  public List<DBRow> select(@Nullable final Join join, @Nullable final Where where,
      @Nullable final OrderBy orderBy) throws SQLException {
    return dbConnector.select(this, join, where, orderBy);
  }

  /**
   * Select the {@code DBRow} having the specified primary key value. Returns <tt>null</tt> if no
   * records is found having the specified primary key value.
   *
   * @param primaryKey the primary key value
   * @return the {@code DBRow} having the specified primary key value
   * @throws SQLException
   * @throws IllegalStateException if more than one record is found
   * @see PrimaryKey
   * @see DBRow
   */
  @Nullable
  public DBRow selectByPK(@Nonnull final PrimaryKey primaryKey) throws SQLException {
    checkNotNull(primaryKey);
    final Where where = primaryKey.getWhereBuilder().toWhere();
    final List<DBRow> list = select(where);
    if (list.size() > 1) {
      throw new IllegalStateException("Wrong primary key selection");
    } else if (list.size() == 0) {
      return null;
    }
    return list.get(0);
  }

  /**
   * Returns the record count. Executes a select count query.
   *
   * @return the number of records found in this {@code DBTable}
   * @throws SQLException
   */
  public int count() throws SQLException {
    /*
     * select count(*) from name
     */
    final Select query =
        new Select.Builder(null, getTableDescription().getQualifiedName(), Select.count("*"))
            .toSelect();
    return dbConnector.executeSelectNumber(query).intValue();
  }

  public Join buildJoin(final DBTable otherTable) {
    return buildJoin(JoinType.INNER_JOIN, otherTable);
  }

  public Join buildJoin(final JoinType joinType, final DBTable otherTable) {
    final List<String> commonColumns = getCommonColumns(getPKMetaData(), otherTable);
    if (commonColumns.size() == 0) {
      throw new IllegalStateException(
          "Unable to automatically generate join condition: no common pk attributes found");
    }
    final String otherAlias = otherTable.getAlias();
    final String condition = Join.buildCondition(otherAlias, this.alias, commonColumns);
    return new Join.Builder(otherAlias, otherTable.getTableDescription().getQualifiedName(),
        condition).toJoin();
  }

  /**
   * Returns the list of common column names between the specified primary key and the columns of
   * the specified {@code DBTable}.
   *
   * @param pk the primary key
   * @param dbTable the database table
   * @return the list of common column names between the specified primary key and the columns of
   *         the specified {@code DBTable}.
   */
  @Nonnull
  public static List<String> getCommonColumns(@Nonnull final PKMetaData pk,
      @Nonnull final DBTable dbTable) {
    final List<String> common = new ArrayList<>();
    for (final PKColumn pkColumn : pk.getColumns()) {
      if (dbTable.containsColumn(pkColumn.getName())) {
        common.add(pkColumn.getName());
      }
    }
    return common;
  }

  public void log(final Logger logger, final JLogger.Level level) {
    JLogger.log(level, logger, getTableDescription().toString());
    for (final ColumnMetaData columnMetaData : getColumnMetaDatas()) {
      JLogger.log(level, logger, columnMetaData.toString());
    }
    JLogger.log(level, logger, getPKMetaData().toString());
  }

  public void logVerbose(final Logger logger, final JLogger.Level level) {
    JLogger.log(level, logger, getTableDescription().toString());
    for (final ColumnMetaData columnMetaData : getColumnMetaDatas()) {
      JLogger.log(level, logger, columnMetaData.fullDescription());
    }
    JLogger.log(level, logger, getPKMetaData().toString());
  }

  /**
   * Companion builder class for {@link DBTable}.
   * 
   * @author Marco Foroni
   * @see DBTable
   */
  public static class Builder {
    @Nonnull
    private final DBConnector dbConnector;
    @Nullable
    private String schema = null;
    @Nonnull
    private final String name;
    @Nullable
    private String alias = null;

    public Builder(final DBConnector dbConnector, @Nonnull final String name) {
      this.dbConnector = dbConnector;
      this.name = name;
    }

    public Builder schema(final String schema) {
      this.schema = schema;
      return this;
    }

    public Builder alias(final String alias) {
      this.alias = alias;
      return this;
    }

    public DBTable build() {
      return new DBTable(dbConnector, schema, name, alias);
    }
  }
}
