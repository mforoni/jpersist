package com.gitlab.mforoni.jpersist;

import static com.gitlab.mforoni.jpersist.query.Query.Operator.EQUAL;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import com.gitlab.mforoni.jpersist.PKMetaData.PKColumn;
import com.gitlab.mforoni.jpersist.query.Where;
import com.google.common.collect.ImmutableSet;

/**
 * Immutable class representing the primary key value. It contains:
 * <ul>
 * <li>the primary key meta information, i.e. a {@code PKMetaData} object</li>
 * <li>a {@code Map<String, Object>} that maps the primary key values by their column name</li>
 * </ul>
 *
 * @author Foroni
 * @see PKMetaData
 */
@Immutable
public final class PrimaryKey {
  // mappa bidirezionale per recuperare nome attributo da valore è utile?
  // implementare comparable ha senso?
  private final PKMetaData metaData;
  /**
   * mappings primary key values by column name
   */
  private final Map<String, Object> values;

  /**
   * Instantiates a new {@code PrimaryKey}.
   *
   * @param builder the builder
   */
  private PrimaryKey(final Builder builder) {
    this.metaData = builder.pkmd;
    this.values = builder.values;
  }

  /**
   * Returns the values of this primary key in a {@code List} following the PK columns definition
   * order.
   * 
   * @return
   */
  public List<Object> values() {
    final List<Object> list = new ArrayList<>();
    for (short s = 0; s < metaData.getSize(); s++) {
      final PKColumn pkColumn = metaData.getPKColumn(s);
      list.add(values.get(pkColumn.getName()));
    }
    return list;
  }

  /**
   * Gets the metadata of this primary key.
   *
   * @return the {@code PKMetaData}
   * @see PKMetaData
   */
  public PKMetaData getMetaData() {
    return metaData;
  }

  /**
   * Gets the size, i.e. the number of columns that made up this primary key.
   *
   * @return the size, i.e. the number of columns of this primary key.
   * @see PKColumn
   */
  public int getSize() {
    return metaData.getSize();
  }

  /**
   * Checks if this {@code PrimaryKey} contains at least one <tt>null</tt> value.
   *
   * @return <tt>true</tt>, if the {@code PrimaryKey} contains <tt>null</tt> values, <tt>false</tt>
   *         otherwise.
   */
  public boolean containsNull() {
    return values.values().contains(null);
  }

  /**
   * Gets the value of the specified primary key column.
   *
   * @param pkColumnName the name of the primary key column to get the value
   * @return the value of the specified primary key column.
   * @throws IllegalArgumentException if the specified column name is not part of the primary key
   */
  @Nullable
  public Object getObject(final String pkColumnName) {
    if (!metaData.containsColumn(pkColumnName)) {
      throw new IllegalArgumentException(String
          .format("The specified column name %s is not part of the primary key", pkColumnName));
    }
    return values.get(pkColumnName);
  }

  public BigDecimal getBigDecimal(final String pkColumnName) {
    return (BigDecimal) getObject(pkColumnName);
  }

  public Number getNumber(final String pkColumnName) {
    return (Number) getObject(pkColumnName);
  }

  public Long getLong(final String pkColumnName) {
    return (Long) getObject(pkColumnName);
  }

  public Integer getInteger(final String pkColumnName) {
    return (Integer) getObject(pkColumnName);
  }

  public String getString(final String pkColumnName) {
    return String.class.cast(getObject(pkColumnName));
  }

  public Double getDouble(final String pkColumnName) {
    return (Double) getObject(pkColumnName);
  }

  /**
   * Returns all the entries <i>primary key column (name, value)</i> defined in this
   * {@code PrimaryKey}
   *
   * @return all the entries <i>primary key column (name, value)</i> defined in this
   *         {@code PrimaryKey}
   * @see Entry
   */
  public ImmutableSet<Entry<String, Object>> getEntries() {
    return ImmutableSet.copyOf(values.entrySet());
  }

  /**
   * Return a {@code WhereBuilder} from this {@code PrimaryKey}.
   *
   * @return
   * @throws IllegalStateException
   * @see Builder
   */
  Where.Builder getWhereBuilder() {
    final Iterator<Entry<String, Object>> iterator = values.entrySet().iterator();
    if (iterator.hasNext()) {
      Entry<String, Object> entry = iterator.next();
      final Where.Builder wb = new Where.Builder(entry.getKey(), EQUAL, entry.getValue());
      while (iterator.hasNext()) {
        entry = iterator.next();
        wb.and(entry.getKey(), EQUAL, entry.getValue());
      }
      return wb;
    } else {
      // should never happen
      throw new IllegalStateException("Cannot build WhereBuilder without mappings column-value");
    }
  }

  /**
   * Checks if this {@code PrimaryKey} contains the specified {@code value}.
   * <p>
   * Requires the same time of {@link Collection#contains(Object)} that should be
   * O({@link PrimaryKey#getSize()}) in the worst case.
   *
   * @return <tt>true</tt>, if this {@code PrimaryKey} contains the specified {@code value},
   *         <tt>false</tt> otherwise
   * @see Collection#contains(Object)
   */
  public boolean contains(@Nullable final Object value) {
    return values.values().contains(value);
  }

  /**
   * Returns <tt>true</tt> if this {@code PrimaryKey} and the specified object have:
   * <ul>
   * <li>the same {@code PrimaryKey} according to {@link PKMetaData#equals(Object)}
   * <li>the same entries in the {@code Map<String, Object>} members according to
   * {@link Map#equals(Object)}
   * </ul>
   */
  @Override
  public boolean equals(final Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof PrimaryKey)) {
      return false;
    }
    final PrimaryKey pk = (PrimaryKey) obj;
    if (pk.getSize() != this.getSize()) {
      return false;
    }
    if (!metaData.equals(pk.getMetaData())) {
      return false;
    }
    if (!values.equals(pk.values)) {
      return false;
    }
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + metaData.hashCode();
    result = 31 * result + values.hashCode();
    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return values.toString();
  }

  /**
   * Checks if the specified primary keys {@code pk1}, {@code pk2} have the same
   * {@code Map<String, Object>} members according to {@link Map#equals(Object)}.
   *
   * @param pk1 the first {@code PrimaryKey}
   * @param pk2 the second {@code PrimaryKey}
   * @return <tt>true</tt> if the specified primary keys {@code pk1}, {@code pk2} have the same
   *         {@code Map<String, Object>} members according to {@link Map#equals(Object)}, otherwise
   *         returns <tt>false</tt>
   */
  public static boolean equals(@Nonnull final PrimaryKey pk1, @Nonnull final PrimaryKey pk2) {
    return pk1.values.equals(pk2.values);
  }

  /**
   * Returns the {@code PrimaryKey} of the specified {@code DBRow}.
   * <p>
   * Requires O({@link PrimaryKey#getSize()}) in the worst case.
   *
   * @param dbRow the {@code DBRow}
   * @return the {@code PrimaryKey} of the specified {@code DBRow}
   * @see PrimaryKey
   * @see DBRow
   */
  @Nonnull
  static PrimaryKey valueOf(@Nonnull final DBRow dbRow) {
    // FIXME attenzione che carica la primary key con il tipo effettivo ricavato dal database e
    // questo potrebbe essere BigDecimal mentre tu ti aspetti altro
    final PKMetaData pkmd = dbRow.getDBTable().getPKMetaData();
    final PrimaryKey.Builder pkb = new Builder(pkmd);
    final List<PKColumn> pkColumns = pkmd.getColumns();
    for (final PKColumn pkColumn : pkColumns) {
      pkb.setValue(pkColumn.getName(), dbRow.getObject(pkColumn.getName()));
    }
    return pkb.build();
  }

  @Nonnull
  public static PrimaryKey valueOf(final DBTable dbTable, final Object value) {
    final PKMetaData pkmd = dbTable.getPKMetaData();
    if (pkmd.getSize() != 1) {
      throw new IllegalArgumentException("The primary key must have size 1");
    }
    final String pkColumnName = pkmd.getColumns().get(0).getName();
    return new Builder(pkmd).setValue(pkColumnName, value).build();
  }

  /**
   * The companion builder class of the {@link PrimaryKey}.
   *
   * @author Marco Foroni
   * @see PrimaryKey
   * @see PKMetaData
   */
  public static class Builder {
    private final PKMetaData pkmd;
    private final Map<String, Object> values;

    /**
     * Instantiates a new primary key value builder.
     *
     * @param pkmd the {@code PKMetaData}
     * @see PKMetaData
     */
    public Builder(@Nonnull final PKMetaData pkmd) {
      this.pkmd = pkmd;
      values = new HashMap<>(pkmd.getSize());
    }

    /**
     * Instantiates a new primary key value builder.
     *
     * @param dbTable the {@code DBTable}
     * @see DBTable
     */
    public Builder(@Nonnull final DBTable dbTable) {
      this(dbTable.getPKMetaData());
    }

    /**
     * Sets the primary key column having the specified name with the given value.
     *
     * @param pkColumnName the name of the primary key column value to set
     * @param value the value to set
     * @return this {@code Builder}
     * @throws IllegalArgumentException if the specified column is not defined in the
     *         {@code PrimaryKey}
     * @see PKMetaData
     */
    public Builder setValue(final String pkColumnName, final Object value) {
      if (!pkmd.containsColumn(pkColumnName)) {
        throw new IllegalArgumentException(
            String.format("The column %s is not part of the %s", pkColumnName, pkmd));
      }
      values.put(pkColumnName, value);
      return this;
    }

    /**
     * Sets the primary key column having the specified key sequence with the given value.
     *
     * @param keySequence the key sequence of the primary key column value to set
     * @param value the value to set
     * @return this {@code Builder}
     * @throws IllegalArgumentException if the specified key sequence is not defined in the
     *         {@code PrimaryKey}
     * @see PKMetaData
     */
    public Builder setValue(final short keySequence, final Object value) {
      final PKColumn pkc = pkmd.getPKColumn(keySequence);
      if (pkc == null) {
        throw new IllegalArgumentException("Wrong key sequence");
      }
      return setValue(pkc.getName(), value);
    }

    /**
     * Builds the {@code PrimaryKey}. Checks if all the primary key columns have been set with a
     * value.<br>
     * Requires O({@link PKMetaData#getSize()})
     *
     * @return the {@code PrimaryKey}
     * @throws IllegalStateException if not all primary key columns have been set with a value
     * @see PrimaryKey
     */
    public PrimaryKey build() {
      for (final PKColumn pkColumn : pkmd.getColumns()) {
        if (!values.containsKey(pkColumn.getName())) {
          throw new IllegalStateException(
              String.format("%s has not been set with a value", pkColumn));
        }
      }
      return new PrimaryKey(this);
    }
  }
}
