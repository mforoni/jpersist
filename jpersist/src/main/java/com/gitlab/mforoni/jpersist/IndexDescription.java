package com.gitlab.mforoni.jpersist;

/**
 * Immutable class representing the principal meta information of a database index. A database
 * index is a data structure that improves the speed of data retrieval operations on a database
 * table at the cost of additional writes and storage space to maintain the index data structure.
 * Indexes are used to quickly locate data without having to search every row in a database table
 * every time a database table is accessed. Indices can be created using one or more columns of a
 * database table, providing the basis for both rapid random lookups and efficient access of
 * ordered records.
 *
 * @author Marco Foroni
 */
public final class IndexDescription {
  private final String name;
  private final boolean nonUnique;
  private final String qualifier;
  private final short type;
  private final String columnName;

  /**
   * Instantiates a new index meta info.
   *
   * @param name the name
   * @param nonUnique the non unique
   * @param qualifier the qualifier
   * @param type the type
   * @param columnName the column name
   */
  IndexDescription(final String name, final boolean nonUnique, final String qualifier,
      final short type, final String columnName) {
    this.name = name;
    this.nonUnique = nonUnique;
    this.qualifier = qualifier;
    this.type = type;
    this.columnName = columnName;
  }

  /**
   * Gets the name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Checks if is non unique.
   *
   * @return true, if is non unique
   */
  public boolean isNonUnique() {
    return nonUnique;
  }

  /**
   * Gets the qualifier.
   *
   * @return the qualifier
   */
  public String getQualifier() {
    return qualifier;
  }

  /**
   * Gets the type.
   *
   * @return the type
   */
  public short getType() {
    return type;
  }

  /**
   * Gets the column name.
   *
   * @return the column name
   */
  public String getColumnName() {
    return columnName;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("IndexDescription: name=").append(name);
    if (nonUnique) {
      sb.append(", ").append("non-unique");
    }
    sb.append(", qualifier=").append(qualifier).append(", type=").append(type)
        .append(", column_name=").append(columnName);
    return sb.toString();
  }
}