package com.gitlab.mforoni.jpersist.query;

import static com.gitlab.mforoni.jpersist.query.Query.WHERE;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import com.github.mforoni.jbasic.JArrays;
import com.gitlab.mforoni.jpersist.query.Query.Function;
import com.gitlab.mforoni.jpersist.query.Query.Operator;
import com.google.common.collect.ImmutableList;

/**
 * Immutable class representing a <i>where condition</i> of a SQL statement.
 *
 * @author Foroni
 */
@Immutable
public final class Where {
  // TODO gestione parentesi in condizioni con and or
  private final String expression;
  private final ImmutableList<Object> parameters;
  private final ImmutableList<String> columnNames;

  public Where(final Function function, final String column, final Operator operator,
      final Object value) {
    this(new Builder(function, column, operator, value));
  }

  public Where(final String columnName, final Operator operator, final Object value) {
    this(new Builder(columnName, operator, value));
  }

  private Where(final Builder builder) {
    expression = builder.sb.toString();
    parameters = ImmutableList.copyOf(builder.parameters);
    columnNames = ImmutableList.copyOf(builder.columnNames);
  }

  public String getExpression() {
    return expression;
  }

  public ImmutableList<Object> getParameters() {
    return parameters;
  }

  /**
   * Returns parameters' column names
   *
   * @return
   */
  public ImmutableList<String> getColumnNames() {
    return columnNames;
  }

  @Override
  public String toString() {
    return expression + " " + parameters.toString();
  }

  /**
   * Companion builder class for the {@code Where} object.
   *
   * @author Foroni
   * @see Where
   */
  public static class Builder {
    private final StringBuilder sb;
    private final List<Object> parameters;
    private final List<String> columnNames;

    public Builder(final String alias, final String columnName, final Operator operator,
        final Object value) {
      this(null, alias, columnName, operator, value);
    }

    public Builder(final String columnName, final Operator operator, final Object value) {
      this(null, null, columnName, operator, value);
    }

    public Builder(final Function function, final String columnName, final Operator operator,
        final Object value) {
      this(function, null, columnName, operator, value);
    }

    public Builder(final Function function, final String alias, final String columnName,
        final Operator operator, final Object value) {
      parameters = new ArrayList<>();
      columnNames = new ArrayList<>();
      sb = new StringBuilder(WHERE).append(' ');
      save(function, alias, columnName, operator, value);
    }

    private void save(@Nullable final Function function, @Nullable final String alias,
        final String columnName, final Operator operator, final Object value) {
      final String qualifiedName =
          (function != null) ? Select.function(function, Select.qualify(alias, columnName))
              : Select.qualify(alias, columnName);
      sb.append(qualifiedName).append(buildCondition(columnName, operator, value));
      if (operator != Operator.IN && operator != Operator.NOT_IN) {
        parameters.add(value);
        columnNames.add(columnName);
      }
    }

    private String buildCondition(final String column, final Operator operator,
        final Object value) {
      switch (operator) {
        case EQUAL:
          return "=?";
        case MINOR:
          return "<?";
        case MAJOR:
          return ">?";
        case NOT_EQUAL:
          return "<>?";
        case LIKE:
          return " like ?";
        case NOT_LIKE:
          return " not like ?";
        case IN:
          return buildMultiparamsCondition("in", column, value);
        case NOT_IN:
          return buildMultiparamsCondition("not in", column, value);
        default:
          // should never happen
          throw new IllegalStateException("Operator " + operator + " not handled");
      }
    }

    private String buildMultiparamsCondition(final String operator, final String columnName,
        final Object value) {
      if (JArrays.isArray(value)) {
        final Class<?> type = value.getClass().getComponentType();
        final List<?> values;
        if (type.isPrimitive()) {
          values = JArrays.primitiveArrayToList(value);
        } else {
          values = Arrays.asList((Object[]) value);
        }
        if (values.size() == 0) {
          throw new IllegalArgumentException(
              "Operators IN/NOT IN require a non-empty array as parameter");
        }
        for (final Object val : values) {
          parameters.add(val);
          columnNames.add(columnName);
        }
        final StringBuilder sb = new StringBuilder(" ").append(operator).append(" (?");
        for (int i = 1; i < values.size(); i++) {
          sb.append(", ?");
        }
        return sb.append(")").toString();
      } else {
        throw new IllegalArgumentException("Operators IN/NOT IN require an array as parameter");
      }
    }

    public Builder and(final String columnName, final Operator operator, final Object value) {
      return and(null, null, columnName, operator, value);
    }

    public Builder and(final String alias, final String columnName, final Operator operator,
        final Object value) {
      return and(null, alias, columnName, operator, value);
    }

    public Builder and(final Function function, final String columnName, final Operator operator,
        final Object value) {
      return and(function, null, columnName, operator, value);
    }

    public Builder and(final Function function, final String alias, final String columnName,
        final Operator operator, final Object value) {
      sb.append(" and ");
      save(function, alias, columnName, operator, value);
      return this;
    }

    public Where toWhere() {
      return new Where(this);
    }
  }
}
