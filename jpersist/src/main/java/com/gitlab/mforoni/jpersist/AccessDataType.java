package com.gitlab.mforoni.jpersist;

import com.google.common.annotations.Beta;

/**
 * See this <a href="http://www.w3schools.com/sql/sql_datatypes.asp">link</a>.
 * 
 * @author Foroni
 */
@Beta
enum AccessDataType {
  TEXT, //
  MEMO, // TODO to test
  BYTE, // TODO to test
  INTEGER, //
  LONG, //
  SINGLE, // TODO to test
  DOUBLE, // TODO to test
  DATETIME,
}
