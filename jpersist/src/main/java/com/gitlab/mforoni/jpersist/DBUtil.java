package com.gitlab.mforoni.jpersist;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Optional;

/**
 * Utility class for managing databases
 * 
 * @author Marco Foroni
 */
public final class DBUtil { // rename in Databases
  private static final Logger LOGGER = LoggerFactory.getLogger(DBUtil.class);

  // Suppresses default constructor, ensuring non-instantiability.
  private DBUtil() {
    throw new AssertionError();
  }

  public static boolean existTable(@Nonnull final DBConnector dbConnector,
      @Nonnull final String tableName) throws SQLException {
    return existTable(dbConnector, Optional.<String>absent(), tableName);
  }

  public static boolean existTable(@Nonnull final DBConnector dbConnector,
      final Optional<String> schema, @Nonnull final String tableName) throws SQLException {
    final List<TableDesc> list =
        schema.isPresent() ? dbConnector.getDBMetaData().getTables(schema.get(), tableName)
            : dbConnector.getDBMetaData().getTables(tableName);
    return (list.size() > 0) ? true : false;
  }

  /**
   * FIXME se MS ACCESS non crea la primary key (with jdbc-odbc driver)
   * 
   * @param dbConnector
   * @param tableDefinition
   * @throws SQLException
   */
  public static void createTable(final DBConnector dbConnector, final TableDef tableDefinition)
      throws SQLException {
    dbConnector.executeUpdate(tableDefinition.toSQL());
    LOGGER.debug("Created table {} ", tableDefinition.getTableName());
  }

  /**
   * Executes a truncate SQL statement on table {@code tableName}.
   * <p>
   * <b>Note</b>: not supported by MS ACCESS DBMS via jdbc-odbc driver.
   * 
   * @param dbConnector the database connector
   * @param tableName the name of the table
   * @throws SQLException
   */
  public static void truncateTable(@Nonnull final DBConnector dbConnector,
      @Nonnull final String tableName) throws SQLException {
    truncateTable(dbConnector, null, tableName);
  }

  /**
   * Executes a truncate SQL statement on table {@code schema.tableName}.
   * <p>
   * <b>Note</b>: not supported by MS ACCESS DBMS via ODBC driver.
   * 
   * @param dbConnector the database connector
   * @param schema the schema of the table
   * @param tableName the name of the table
   * @throws SQLException
   */
  public static void truncateTable(@Nonnull final DBConnector dbConnector,
      @Nullable final String schema, @Nonnull final String tableName) throws SQLException {
    final String query = SQLStatements.truncateTable(schema, tableName);
    dbConnector.executeUpdate(query);
    LOGGER.debug("Truncated table {}", tableName);
  }

  /**
   * Executes a delete SQL statement on table {@code tableName}.
   * 
   * @param dbConnector the database connector
   * @param tableName the name of the table
   * @throws SQLException
   */
  public static void deleteTable(@Nonnull final DBConnector dbConnector,
      @Nonnull final String tableName) throws SQLException {
    deleteTable(dbConnector, null, tableName);
  }

  /**
   * Executes a delete SQL statement on table {@code schema.tableName}.
   * 
   * @param dbConnector the database connector
   * @param schema the schema of the table
   * @param tableName the name of the table
   * @throws SQLException
   */
  public static void deleteTable(@Nonnull final DBConnector dbConnector,
      @Nullable final String schema, @Nonnull final String tableName) throws SQLException {
    final String query = SQLStatements.deleteTable(schema, tableName);
    dbConnector.executeUpdate(query);
    LOGGER.debug("Deleted table {}", tableName);
  }

  /**
   * Executes a drop SQL statement on table {@code tableName}.
   * 
   * @param dbConnector the database connector
   * @param tableName the name of the table
   * @throws SQLException
   */
  public static void dropTable(@Nonnull final DBConnector dbConnector,
      @Nonnull final String tableName) throws SQLException {
    dropTable(dbConnector, null, tableName);
  }

  /**
   * Executes a drop SQL statement on table {@code schema.tableName}.
   * 
   * @param dbConnector the database connector
   * @param schema the schema of the table
   * @param tableName the name of the table
   * @throws SQLException
   */
  public static void dropTable(@Nonnull final DBConnector dbConnector,
      @Nullable final String schema, @Nonnull final String tableName) throws SQLException {
    final String query = SQLStatements.dropTable(schema, tableName);
    dbConnector.executeUpdate(query);
    LOGGER.debug("Dropped table {}", tableName);
  }

  public static void setPrimaryKey(final DBConnector dbConnector, final String tableName,
      final String constraintName, final List<String> pkColumnNames) throws SQLException {
    final String query =
        SQLStatements.buildAlterTablePrimaryKey(tableName, constraintName, pkColumnNames);
    dbConnector.executeUpdate(query);
    LOGGER.debug("Primary key {} set on table {}, columns: ", constraintName, tableName,
        pkColumnNames);
  }

  public static void setPrimaryKey(final DBConnector dbConnector, final String constraintName,
      final TableDef tableDefinition) throws SQLException {
    final List<String> pkColumns = new ArrayList<>();
    for (final ColumnDef columnDefinition : tableDefinition.getColumnDefinitions()) {
      if (columnDefinition.isPK()) {
        pkColumns.add(columnDefinition.getName());
      }
    }
    setPrimaryKey(dbConnector, tableDefinition.getTableName(), constraintName, pkColumns);
  }
}
