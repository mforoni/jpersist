package com.gitlab.mforoni.jpersist;

import java.math.BigDecimal;
import java.sql.SQLException;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public abstract class DBRow { // losing interface to have state setter protected
  /**
   * The enumeration State representing the state of this {@code DBRow}. The possible values are:
   * <ul>
   * <li>{@code SPECULAR} if this {@code DBRow} has the same values of the row in the database table
   * <li>{@code NEW} if this {@code DBRow} represents a new row that is not in the database table
   * <li>{@code UPDATED} if this {@code DBRow} has at least one value different from the row in the
   * database table
   * <ul>
   */
  public enum State {
    /**
     * Indicates that the {@code DBRow} has the same values of the row in the database table
     */
    SPECULAR,
    /**
     * Indicates that the {@code DBRow} represents a new row that is not in the database table
     */
    NEW,
    /**
     * Indicates that the {@code DBRow} has at least one value different from the row in the
     * database table
     */
    UPDATED,
    /**
     * 
     */
    DELETED
  }

  /**
   * Gets the database table of this {@code DBRow}.
   *
   * @return the database table
   * @see DBTable
   */
  @Nonnull
  public abstract DBTable getDBTable();

  /**
   * Gets the state of this {@code DBRow}.
   *
   * @return the state of this {@code DBRow}
   * @see State
   */
  @Nonnull
  public abstract State getState();

  protected abstract void setState(State state);

  /**
   * Checks if the value of the column having the specified index is modified.
   *
   * @param columnIndex the index of the column
   * @return <tt>true</tt>, if the value of the column having the specified index is modified,
   *         <tt>false</tt> otherwise
   */
  // boolean isModified(int columnIndex);
  /**
   * Checks if the value of the column having the specified name is modified.
   *
   * @param columnName the column name to check if the value is modified
   * @return <tt>true</tt>, if the value of the column having the specified name is modified,
   *         <tt>false</tt> otherwise.
   */
  public abstract boolean isModified(String columnName);

  /**
   * Gets the size of this {@code DBRow} i.e. the number of columns.
   *
   * @return the size of this {@code DBRow} i.e. the number of columns.
   */
  public abstract int getSize();

  /**
   * Gets the original {@code PrimaryKey}.
   *
   * @return the original {@code PrimaryKey}
   * @see PrimaryKey
   */
  public abstract PrimaryKey getOriginalPK();

  public abstract PrimaryKey getPrimaryKey();

  /**
   * Gets the value of the column having the specified name.
   *
   * @param columnName the column name to retrieve the value
   * @return the value of the column having the specified name
   * @throws IllegalArgumentException if the specified column name is not defined in the column meta
   *         data of the {@code DBTable} associated with this {@code DBRow}.
   */
  @Nullable
  public abstract Object getObject(String columnName) throws IllegalArgumentException;

  @Nullable
  public abstract Long getLong(String columnName) throws IllegalArgumentException;

  @Nullable
  public abstract Integer getInteger(String columnName) throws IllegalArgumentException;

  public abstract int getInt(String columnName) throws IllegalArgumentException;

  @Nullable
  public abstract String getString(String columnName) throws IllegalArgumentException;

  @Nullable
  public abstract BigDecimal getBigDecimal(String columnName) throws IllegalArgumentException;

  public abstract void setObject(String columnName, Object value);

  public abstract int persist(DBConnector dbConnector) throws SQLException;

  public abstract String getDescription();
}
