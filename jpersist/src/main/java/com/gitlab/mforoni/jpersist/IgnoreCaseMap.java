package com.gitlab.mforoni.jpersist;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import com.google.common.annotations.Beta;

@Beta
final class IgnoreCaseMap<V> implements Map<String, V> {
  private final Map<String, V> map;

  public IgnoreCaseMap() {
    this(new HashMap<String, V>());
  }

  public IgnoreCaseMap(final Map<String, V> map) {
    if (map.size() > 0) {
      throw new IllegalStateException();
    }
    this.map = map;
  }

  @Override
  public int size() {
    return map.size();
  }

  @Override
  public boolean isEmpty() {
    return map.isEmpty();
  }

  @Nullable
  private static String getUpperCaseKey(final Object key) {
    return key != null ? key.toString().toUpperCase() : null;
  }

  @Override
  public boolean containsKey(final Object key) {
    return map.containsKey(getUpperCaseKey(key));
  }

  @Override
  public boolean containsValue(final Object value) {
    return map.containsKey(value);
  }

  @Override
  public V get(final Object key) {
    return map.get(getUpperCaseKey(key));
  }

  @Override
  public V put(final String key, final V value) {
    return map.put(getUpperCaseKey(key), value);
  }

  @Override
  public V remove(final Object key) {
    return map.remove(getUpperCaseKey(key));
  }

  @Override
  public void putAll(final Map<? extends String, ? extends V> m) {
    // FIXME missing controls to ensure map size - see HashMap.putAll()
    for (final Entry<? extends String, ? extends V> entry : m.entrySet()) {
      put(entry.getKey(), entry.getValue());
    }
  }

  @Override
  public void clear() {
    map.clear();
  }

  @Override
  public Set<String> keySet() {
    return map.keySet();
  }

  @Override
  public Collection<V> values() {
    return map.values();
  }

  @Override
  public Set<Entry<String, V>> entrySet() {
    return map.entrySet();
  }
}
