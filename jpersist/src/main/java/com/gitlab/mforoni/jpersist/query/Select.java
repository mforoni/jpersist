package com.gitlab.mforoni.jpersist.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import com.github.mforoni.jbasic.JStrings;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/**
 * Immutable class representing an <i>SQL select statement</i>.
 * 
 * @author Foroni
 */
@Immutable
public final class Select extends AbstractQuery {
  private Select(final String statement, final ImmutableList<Object> parameters) {
    super(statement, parameters);
  }

  public static class Builder {
    final StringBuilder statement;
    // optional
    final List<Object> parameters; // may be empty
    /**
     * mapping aliases by table name
     */
    private final Map<String, String> aliases;

    /**
     * Constructs a builder object for a select query containing the statement <i>select columns
     * from table alias</i> if {@code columns} is not null, otherwise constructs <i>select alias.*
     * from table alias</i>. If {@code alias} is null the first character of {@code table} is used.
     *
     * @param alias
     * @param table
     * @param columns
     */
    public Builder(@Nullable String alias, @Nonnull final String table,
        @Nullable final String columns) {
      Preconditions.checkNotNull(table);
      if (alias == null) {
        alias = table.substring(0, 1);
      }
      statement = new StringBuilder(SELECT).append(' ');
      if (columns == null) {
        statement.append(alias).append(".*");
      } else {
        statement.append(columns);
      }
      statement.append(' ').append(FROM).append(' ').append(table).append(' ').append(alias);
      aliases = new HashMap<>();
      parameters = new ArrayList<>();
      aliases.put(table, alias);
    }

    public Builder join(final Join join) {
      statement.append(' ').append(join.getStatement());
      return this;
    }

    public Builder innerJoin(final String table, final String alias, final String table2,
        final List<String> columns) {
      final String alias2 = aliases.get(table2);
      if (alias2 == null) {
        throw new IllegalArgumentException(
            "Unable to build join with not previously defined table " + table2);
      }
      return innerJoin(alias, table, Join.buildCondition(alias, alias2, columns));
    }

    private Builder join(final JoinType type, final String alias, final String table,
        final String condition) {
      statement.append(' ').append(Join.buildExpression(type, alias, table, condition));
      return this;
    }

    public Builder innerJoin(final String alias, final String table, final String condition) {
      return join(JoinType.INNER_JOIN, alias, table, condition);
    }

    public Builder leftJoin(final String alias, final String table, final String condition) {
      return join(JoinType.LEFT_JOIN, alias, table, condition);
    }

    public Builder rightJoin(final String alias, final String table, final String condition) {
      return join(JoinType.RIGHT_JOIN, alias, table, condition);
    }

    public Builder where(final Where where) {
      parameters.addAll(where.getParameters());
      statement.append(' ').append(where.getExpression());
      return this;
    }

    public Builder orderBy(final OrderBy orderBy) {
      statement.append(' ').append(orderBy.getExpression());
      return this;
    }

    public Builder addParameters(final Object... values) {
      if (values != null) {
        for (final Object value : values) {
          parameters.add(value);
        }
      }
      return this;
    }

    public Select toSelect() {
      final String sql = statement.toString();
      final int countQuestionMarks = JStrings.occurrences(sql, '?');
      final int countParams = countParameters(parameters);
      if (countQuestionMarks != countParams) {
        throw new IllegalStateException(
            String.format("Number of question marks %d must be the same of parameters %d",
                countQuestionMarks, countParams));
      }
      return new Select(statement.toString(), ImmutableList.copyOf(parameters));
    }
  }

  public static int countParameters(final List<Object> parameters) {
    int size = 0;
    for (final Object param : parameters) {
      if (param instanceof Object[]) {
        size += ((Object[]) param).length;
      } else if (param instanceof List<?>) {
        size += ((List<?>) param).size();
      } else {
        size++;
      }
    }
    return size;
  }

  public static String columns(final String... columns) {
    return Joiner.on(", ").join(Arrays.asList(columns));
  }

  /**
   * Returns the string <i>alias.column</i> if {@code alias} is not null, otherwise returns
   * <i>column</i>.
   *
   * @param alias
   * @param column
   * @return the string <i>alias.column</i> if {@code alias} is not null, otherwise returns
   *         <i>column</i>
   */
  public static String qualify(@Nullable final String alias, final String column) {
    return (alias != null) ? alias.concat(DOT).concat(column) : column;
  }

  public static String max(final String alias, final String column) {
    return function(Function.MAX, qualify(alias, column));
  }

  public static String max(final String column) {
    return function(Function.MAX, column);
  }

  public static String min(final String alias, final String column) {
    return function(Function.MIN, qualify(alias, column));
  }

  public static String min(final String column) {
    return function(Function.MIN, column);
  }

  public static String count(final String alias, final String column) {
    return function(Function.COUNT, qualify(alias, column));
  }

  public static String count(final String column) {
    return function(Function.COUNT, column);
  }

  public static String coalesce(final String expr1, final String expr2) {
    return function(Function.COALESCE, expr1.concat(", ").concat(expr2));
  }

  public static String function(@Nonnull final Function function, final String expr) {
    return function.name().toLowerCase().concat("(").concat(expr).concat(")");
  }
}
