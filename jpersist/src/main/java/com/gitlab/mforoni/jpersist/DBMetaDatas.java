package com.gitlab.mforoni.jpersist;

import static com.google.common.base.Preconditions.checkNotNull;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Stopwatch;

public final class DBMetaDatas {
  private static final Logger LOGGER = LoggerFactory.getLogger(DBMetaDatas.class);

  private DBMetaDatas() {
    throw new AssertionError();
  }

  @Nonnull
  public static List<String> getCatalogs(@Nonnull final DatabaseMetaData databaseMetaData)
      throws SQLException {
    final Stopwatch stopwatch = Stopwatch.createStarted();
    final List<String> catalogNames = new ArrayList<>();
    try (final ResultSet rs = databaseMetaData.getCatalogs()) {
      while (rs.next()) {
        catalogNames.add(rs.getString(1));
      }
    }
    stopwatch.stop();
    LOGGER.debug("Retrieved catalog names in {}", stopwatch);
    return catalogNames;
  }

  @Nonnull
  public static List<Schema> getSchemas(@Nonnull final DatabaseMetaData databaseMetaData)
      throws SQLException {
    final Stopwatch stopwatch = Stopwatch.createStarted();
    final List<Schema> schemas = new ArrayList<>();
    try (final ResultSet rs = databaseMetaData.getSchemas()) {
      while (rs.next()) {
        final String schema = rs.getString(1);
        final String catalog = rs.getString(2);
        schemas.add(new Schema(schema, catalog));
      }
    }
    stopwatch.stop();
    LOGGER.debug("Retrieved schemas in {}", stopwatch);
    return schemas;
  }

  @Nonnull
  public static List<TableDesc> getTables(@Nonnull final DatabaseMetaData databaseMetaData,
      @Nullable final String catalog, @Nullable final String schemaPattern,
      @Nullable final String tableNamePattern, final TableType... tableTypes) throws SQLException {
    final Stopwatch stopwatch = Stopwatch.createStarted();
    final List<TableDesc> tables = new ArrayList<>();
    final String[] types = new String[tableTypes != null ? tableTypes.length : 0];
    for (int i = 0; tableTypes != null && i < tableTypes.length; i++) {
      types[i] = tableTypes[i].getName();
    }
    try (final ResultSet rs = databaseMetaData.getTables(catalog, schemaPattern, tableNamePattern,
        types.length > 0 ? types : null)) {
      while (rs.next()) {
        final String realCatalog = rs.getString(1);
        final String realSchema = rs.getString(2);
        final String tableName = rs.getString(3);
        final String tableType = rs.getString(4);
        // Check type
        final TableDesc td = new TableDesc(realCatalog, realSchema, tableName, tableType);
        tables.add(td);
      }
      stopwatch.stop();
      LOGGER.debug(
          "Retrieved tables descriptions for catalog={}, schema_pattern={} and table_name_pattern={} in {}",
          catalog, schemaPattern, tableNamePattern, stopwatch);
      return tables;
    }
  }

  @Nonnull
  public static List<ColumnMetaData> getColumns(@Nonnull final DatabaseMetaData databaseMetaData,
      @Nullable final String catalog, final @Nonnull String schema, @Nonnull final String tableName,
      @Nullable final String columNamePattern) throws SQLException {
    checkNotNull(tableName);
    final Stopwatch stopwatch = Stopwatch.createStarted();
    final List<ColumnMetaData> list = new ArrayList<>();
    try (final ResultSet rs =
        databaseMetaData.getColumns(catalog, schema, tableName, columNamePattern)) {
      while (rs.next()) {
        list.add(buildColumnMetaData(rs, catalog, schema, tableName));
      }
    }
    LOGGER.debug(
        "Retrieved metadata for all the columns of table={}, schema={}, catalog={} and column_name_pattern={} in {}",
        tableName, schema, catalog, columNamePattern, stopwatch);
    return list;
  }

  private static ColumnMetaData buildColumnMetaData(@Nonnull final ResultSet rs,
      @Nullable final String catalog, @Nullable final String schema,
      @Nonnull final String tableName) throws SQLException {
    final String retrievedCatalog = rs.getString(1); // table catalog (may be null)
    checkCatalog(catalog, retrievedCatalog);
    final String retrievedSchema = rs.getString(2); // table schema (may be null)
    checkSchema(schema, retrievedSchema);
    final String retrievedTableName = rs.getString(3); // TABLE_NAME String => table name
    checkTableName(tableName, retrievedTableName);
    final String columnName = rs.getString(4); // column name
    final int dataType = rs.getInt(5); // SQL type from java.sql.Types
    final String typeName = rs.getString(6); // Data source dependent type name, for a UDT the type
                                             // name is fully qualified
    final int columnSize = rs.getInt(7); // column size.
    // BUFFER_LENGTH is not used.
    final int decimalDigits = rs.getInt(9); // the number of fractional digits. Null is returned for
                                            // data types where DECIMAL_DIGITS is not applicable.
    // NUM_PREC_RADIX int => Radix (typically either 10 or 2)
    final int nullable = rs.getInt(11); // is NULL allowed.
    // - columnNoNulls - might final not allow NULL final values
    // - columnNullable - final definitely allows NULL final values
    // - columnNullableUnknown - final nullability unknown
    // REMARKS String => comment describing column (may be null)
    // COLUMN_DEF String => default value for the column, which should final be interpreted as final
    // a string when final the value is final enclosed in single quotes (may be null)
    // SQL_DATA_TYPE int => unused
    // SQL_DATETIME_SUB int => unused
    // CHAR_OCTET_LENGTH int => for char types the maximum number of bytes in the column
    final int ordinalPosition = rs.getInt(17); // index of column in table (starting at 1)
    // IS_NULLABLE String => ISO rules are used to determine the nullability for a column.
    // - YES --- if the column can include NULLs
    // - NO --- if the column cannot include NULLs
    // - empty string --- if the nullability for the column is unknown
    // SCOPE_CATALOG String => catalog of table that is the scope of a reference attribute (null if
    // DATA_TYPE isn't REF)
    // SCOPE_SCHEMA String => schema of table that is the scope of a reference attribute (null if
    // the DATA_TYPE isn't REF)
    // SCOPE_TABLE String => table name that this the scope of a reference attribute (null if the
    // DATA_TYPE isn't REF)
    // SOURCE_DATA_TYPE short => source type of a distinct type or user-generated final Ref type,
    // SQL type
    // final from java.sql.Types (null if DATA_TYPE isn't DISTINCT or user-generated REF)
    // final String isAutoincrem = rs.getString(23);// Indicates whether this column is auto
    // incremented
    // - YES --- if the column is auto incremented
    // - NO --- if the column is not auto incremented
    // - empty string --- if it cannot be determined whether the column is auto incremented
    // IS_GENERATEDCOLUMN String => Indicates whether this is a generated column
    // - YES --- if this a generated column
    // - NO --- if this not a generated column
    // - empty string --- if it cannot be determined whether this is a generated column
    return new ColumnMetaData(ordinalPosition, columnName, dataType, typeName, columnSize,
        decimalDigits, nullable, tableName);
  }

  /**
   * Retrieves a description of the given table's primary key columns. The resulting primary key
   * columns are stored in the {@code PKMetaData} ordered by column name. A {@code PKMetaData}
   * contains some primary key meta information as the columns that make up the primary key, their
   * order and the name of the constraint.<br>
   * Opens, uses and closes a {@code ResultSet}.
   * <p>
   * <b>Note:</b> not supported by odbc jdbc driver with MS ACCESS: using this DBMS this method will
   * throw the following {@code java.sql.SQLException}: [Microsoft][Driver Manager ODBC] Il driver
   * non supporta questa funzione.
   *
   * @param catalog the catalog (may be <tt>null</tt>)
   * @param schema the schema (may be <tt>null</tt>)
   * @param tableName the table name (cannot be <tt>null</tt>)
   * @return the {@code PKMetaData}
   * @throws SQLException
   * @see DatabaseMetaData#getPrimaryKeys(String, String, String)
   * @see PKMetaData
   * @see ResultSet
   */
  @Nonnull
  public static PKMetaData getPKMetaData(@Nonnull final DatabaseMetaData databaseMetaData,
      @Nullable final String catalog, @Nullable final String schema,
      @Nonnull final String tableName) throws SQLException {
    checkNotNull(tableName);
    final Stopwatch stopwatch = Stopwatch.createStarted();
    final PKMetaData.Builder pkmdb = new PKMetaData.Builder(tableName);
    try (ResultSet rs = databaseMetaData.getPrimaryKeys(catalog, schema, tableName)) {
      while (rs.next()) {
        final String retrievedCatalog = rs.getString(1); // table catalog (may be null)
        checkCatalog(catalog, retrievedCatalog);
        final String retrievedSchema = rs.getString(2); // table schema (may be null)
        checkSchema(schema, retrievedSchema);
        final String retrievedTableName = rs.getString(3); // table name
        checkTableName(tableName, retrievedTableName);
        final String columnName = rs.getString(4); // column name
        final short keySeq = rs.getShort(5);
        // sequence number within primary key (a value of 1 represents the first column of the
        // primary key, a value of 2 would represent the second column within the primary key).
        pkmdb.addColumn(columnName, keySeq);
        final String pkName = rs.getString(6); // primary key name (may be null)
        pkmdb.constraintName(pkName);
      }
    }
    stopwatch.stop();
    LOGGER.debug("Retrieved PK for table={}, schema={}, catalog={} in {}", tableName, schema,
        catalog, stopwatch);
    return pkmdb.build();
  }

  private static void checkCatalog(@Nullable final String catalog,
      @Nullable final String retrievedCatalog) {
    if (!Objects.equals(catalog, retrievedCatalog)) {
      LOGGER.warn("Catalog inconsistent: provided={} retrieved={}", catalog, retrievedCatalog);
    }
  }

  private static void checkSchema(@Nullable final String schema,
      @Nullable final String retrievedSchema) {
    if (!Objects.equals(schema, retrievedSchema)) {
      LOGGER.warn("Schema inconsistent: provided={} retrieved={}", schema, retrievedSchema);
    }
  }

  private static void checkTableName(@Nonnull final String tableName,
      @Nonnull final String retrievedTableName) {
    if (!Objects.equals(tableName, retrievedTableName)) {
      LOGGER.warn("Table name inconsistent: provided={} retrieved={}", tableName,
          retrievedTableName);
    }
  }

  /**
   * Retrieves a description of the given table's indices and statistics. The returned list of
   * {@code IndexDescription} are ordered by non unique, type, index name, and ordinal position. All
   * the parameters except the table name are optional, if not specified the comparison is omitted.
   * An {@code IndexDescription} contains some index meta information like the column on which the
   * index is defined, the type and the name.
   * <p>
   * Opens, uses and closes a {@code ResultSet}.
   *
   * @param catalog the catalog (may be <tt>null</tt>)
   * @param schema the schema (may be <tt>null</tt>)
   * @param tableName the table name (cannot be <tt>null</tt>)
   * @return the {@code IndexDescription}
   * @throws SQLException
   * @see DatabaseMetaData#getIndexInfo(String, String, String, boolean, boolean)
   * @see IndexDescription
   */
  @Nonnull
  public static List<IndexDescription> getIndexes(@Nonnull final DatabaseMetaData databaseMetaData,
      @Nullable final String catalog, @Nullable final String schema,
      @Nonnull final String tableName) throws SQLException {
    checkNotNull(tableName);
    final Stopwatch stopwatch = Stopwatch.createStarted();
    final List<IndexDescription> indexes = new ArrayList<>();
    try (final ResultSet rs =
        databaseMetaData.getIndexInfo(catalog, schema, tableName, true, true)) {
      while (rs.next()) {
        final IndexDescription index = buildIndexDescription(rs, catalog, schema, tableName);
        if (index != null) {
          indexes.add(index);
        }
      }
    }
    stopwatch.stop();
    LOGGER.debug("Retrieved indexes for table={}, schema={} and catalog={} in {}", tableName,
        schema, catalog, stopwatch);
    return indexes;
  }

  @Nullable
  private static IndexDescription buildIndexDescription(@Nonnull final ResultSet rs,
      @Nullable final String catalog, @Nullable final String schema,
      @Nonnull final String tableName) throws SQLException {
    final String retrievedCatalog = rs.getString(1);
    checkCatalog(catalog, retrievedCatalog);
    final String retrievedSchema = rs.getString(2);
    checkSchema(schema, retrievedSchema);
    final String retrievedTableName = rs.getString(3);
    checkTableName(tableName, retrievedTableName);
    final boolean nonUnique = rs.getBoolean(4); // Can index values be non-unique. false when TYPE
                                                // is tableIndexStatistic
    final String indexQualifier = rs.getString(5); // index catalog (may be null); null when TYPE is
                                                   // tableIndexStatistic
    final String indexName = rs.getString(6); // index name; null when TYPE is tableIndexStatistic
    final short indexType = rs.getShort(7); // index type:
    // - tableIndexStatistic - this identifies table statistics that are returned in conjuction with
    // a table's index descriptions
    // - tableIndexClustered - this is a clustered index
    // - tableIndexHashed - this is a hashed index
    // - tableIndexOther - this is some other style of index
    // ORDINAL_POSITION short => column sequence number within index; zero when TYPE is
    // tableIndexStatistic
    final String columnName = rs.getString(9); // column name; null when TYPE is tableIndexStatistic
    return indexName == null ? null
        : new IndexDescription(indexName, nonUnique, indexQualifier, indexType, columnName);
  }
}
