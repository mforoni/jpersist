package com.gitlab.mforoni.jpersist;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import com.google.common.annotations.Beta;

/**
 * Immutable class representing a database schema. A database schema of a database system is its
 * structure described in a formal language supported by the database management system (DBMS). The
 * term "schema" refers to the organization of data as a blueprint of how the database is
 * constructed (divided into database tables in the case of relational databases).
 * <p>
 * The formal definition of a database schema is a set of formulas (sentences) called integrity
 * constraints imposed on a database. These integrity constraints ensure compatibility between parts
 * of the schema. All constraints are expressible in the same language. A database can be considered
 * a structure in realization of the database language. The states of a created conceptual schema
 * are transformed into an explicit mapping, the database schema. This describes how real-world
 * entities are modeled in the database. <br>
 * A database schema specifies, based on the database administrator's knowledge of possible
 * applications, the facts that can enter the database, or those of interest to the possible
 * end-users. The notion of a database schema plays the same role as the notion of theory in
 * predicate calculus. A model of this "theory" closely corresponds to a database, which can be seen
 * at any instant of time as a mathematical object. Thus a schema can contain formulas representing
 * integrity constraints specifically for an application and the constraints specifically for a type
 * of database, all expressed in the same database language. <br>
 * In a relational database, the schema defines the tables, fields, relationships, views, indexes,
 * packages, procedures, functions, queues, triggers, types, sequences, materialized views,
 * synonyms, database links, directories, XML schemas, and other elements.
 * <p>
 * A database generally stores its schema in a data dictionary. Although a schema is defined in text
 * database language, the term is often used to refer to a graphical depiction of the database
 * structure. In other words, schema is the structure of the database that defines the objects in
 * the database. In an Oracle Database system, the term "schema" has a slightly different
 * connotation.
 * <p>
 * A {@code Schema} contains:
 * <ul>
 * <li>the name of the schema (cannot be <tt>null</tt>) and</li>
 * <li>the name of the catalog (may be <tt>null</tt>)</li>
 * </ul>
 *
 * @author Foroni Marco
 */
@Beta
@Immutable
public final class Schema {
  private final String name;
  private final String catalog;

  /**
   * Instantiates a new {@code Schema}.
   *
   * @param name the name of the schema (cannot be <tt>null</tt>)
   * @param catalog the catalog (may be <tt>null</tt>)
   */
  public Schema(@Nonnull final String name, @Nullable final String catalog) {
    checkNotNull(name);
    this.name = name;
    this.catalog = catalog;
  }

  /**
   * Gets the name of the schema.
   *
   * @return the schema's name
   */
  @Nonnull
  public String getName() {
    return name;
  }

  /**
   * Gets the catalog.
   *
   * @return the catalog
   */
  @Nullable
  public String getCatalog() {
    return catalog;
  }

  @Override
  public String toString() {
    return String.format("Schema %s of catalog %s", name, catalog);
  }
}
