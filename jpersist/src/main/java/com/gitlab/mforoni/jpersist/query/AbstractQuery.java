package com.gitlab.mforoni.jpersist.query;

import com.google.common.collect.ImmutableList;

abstract class AbstractQuery implements Query {
  private final String statement;
  // optional
  private final ImmutableList<Object> parameters; // may be empty

  protected AbstractQuery(final String statement, final ImmutableList<Object> parameters) {
    this.statement = statement;
    this.parameters = parameters;
  }

  @Override
  public String getStatement() {
    return statement;
  }

  @Override
  public ImmutableList<Object> getParameters() {
    return parameters;
  }

  @Override
  public String toString() {
    return statement + " " + parameters.toString();
  }
}
