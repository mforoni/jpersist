package com.gitlab.mforoni.jpersist;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.annotation.Nonnull;

/**
 * Represents a database column definition.
 */
public final class ColumnDef {
  private final String name;
  private final SQLType sqlType;
  private final boolean nullable;
  private final boolean isPK;
  private final int precision;
  private final int scale;

  private ColumnDef(final ColumnDef.Builder builder) {
    this.name = builder.name;
    this.sqlType = builder.sqlType;
    this.nullable = builder.nullable;
    this.isPK = builder.pk;
    this.precision = builder.precision;
    this.scale = builder.scale;
  }

  /**
   * Gets the name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Gets the sql type.
   *
   * @return the sql type
   */
  public SQLType getSQLType() {
    return sqlType;
  }

  /**
   * Checks if is nullable.
   *
   * @return true, if is nullable
   */
  public boolean isNullable() {
    return nullable;
  }

  /**
   * Checks if is primary key.
   *
   * @return true, if is primary key
   */
  public boolean isPK() {
    return isPK;
  }

  public int getPrecision() {
    return precision;
  }

  public int getScale() {
    return scale;
  }

  @Override
  public String toString() {
    return "ColumnDef [name=" + name + ", sqlType=" + sqlType + "]";
  }

  public String toSQL(final boolean excludePK) {
    final int size = name.length() + 1 + sqlType.toString().length() + 1
        + (isNullable() ? TableDef.NOT_NULL.length() + 1 : 0)
        + (isPK() ? TableDef.PRIMARY_KEY.length() + 1 : 0) + (precision != -1 ? 4 : 0);
    final StringBuilder sb =
        new StringBuilder(size).append(name).append(' ').append(sqlType.toString());
    if (precision != -1) {
      sb.append('(').append(precision);
      if (scale != -1) {
        sb.append(',').append(scale);
      }
      sb.append(')');
    }
    if (!nullable) {
      sb.append(' ').append(TableDef.NOT_NULL);
    }
    if (!excludePK && isPK) {
      sb.append(' ').append(TableDef.PRIMARY_KEY);
    }
    return sb.toString();
  }

  /**
   * The companion builder class.
   */
  public static class Builder {
    private final String name;
    private final SQLType sqlType;
    private boolean nullable;
    private boolean pk;
    private int precision;
    private int scale;

    /**
     * Instantiates a new column definition builder.
     *
     * @param name the name
     * @param sqlType the SQL type
     */
    public Builder(@Nonnull final String name, @Nonnull final SQLType sqlType) {
      checkNotNull(name);
      checkNotNull(sqlType);
      this.name = name;
      this.sqlType = sqlType;
      nullable = true;
      pk = false;
      precision = -1;
      scale = -1;
    }

    /**
     * Indicates that this column is part of the primary key and is not nullable.
     *
     * @return this {@code Builder}
     */
    public ColumnDef.Builder pk() {
      pk = true;
      nullable = false;
      return this;
    }

    /**
     * Not nullable.
     *
     * @return the column definition builder
     */
    public ColumnDef.Builder notNullable() {
      nullable = false;
      return this;
    }

    public ColumnDef.Builder precision(final int precision) {
      this.precision = precision;
      return this;
    }

    public ColumnDef.Builder scale(final int scale) {
      this.scale = scale;
      return this;
    }

    /**
     * Builds the {@code ColumnDef}.
     *
     * @return the column definition
     */
    public ColumnDef build() {
      return new ColumnDef(this);
    }
  }
}
