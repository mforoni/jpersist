package com.gitlab.mforoni.jpersist;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nullable;
import org.joda.time.LocalDate;

/**
 * @see java.sql.Types
 */
public enum SQLType {
  BIT(java.sql.Types.BIT, true, Boolean.class, boolean.class), //
  TINYINT(java.sql.Types.TINYINT, true, Integer.class, byte.class), //
  SMALLINT(java.sql.Types.SMALLINT, true, Integer.class, short.class), //
  INTEGER(java.sql.Types.INTEGER, true, Integer.class, int.class), //
  BIGINT(java.sql.Types.BIGINT, true, Long.class, long.class), //
  FLOAT(java.sql.Types.FLOAT, true, Double.class, double.class), //
  REAL(java.sql.Types.REAL, true, Float.class, float.class), //
  DOUBLE(java.sql.Types.DOUBLE, true, Double.class, double.class), //
  NUMERIC(java.sql.Types.NUMERIC, true, BigDecimal.class), //
  DECIMAL(java.sql.Types.DECIMAL, true, BigDecimal.class), //
  CHAR(java.sql.Types.CHAR, false, String.class), //
  VARCHAR(java.sql.Types.VARCHAR, false, String.class), //
  LONGVARCHAR(java.sql.Types.LONGVARCHAR, false, String.class), //
  DATE(java.sql.Types.DATE, false, java.sql.Date.class), //
  TIME(java.sql.Types.TIME, false, java.sql.Time.class), //
  TIMESTAMP(java.sql.Types.TIMESTAMP, false, java.sql.Timestamp.class), //
  BINARY(java.sql.Types.BINARY, false), //
  VARBINARY(java.sql.Types.VARBINARY, false), //
  LONGVARBINARY(java.sql.Types.LONGVARBINARY, false), //
  NULL(java.sql.Types.NULL, false), //
  OTHER(java.sql.Types.OTHER, false), //
  JAVA_OBJECT(java.sql.Types.JAVA_OBJECT, false), //
  DISTINCT(java.sql.Types.DISTINCT, false), //
  STRUCT(java.sql.Types.STRUCT, false), //
  ARRAY(java.sql.Types.ARRAY, false), //
  BLOB(java.sql.Types.BLOB, false), //
  CLOB(java.sql.Types.CLOB, false), //
  REF(java.sql.Types.REF, false), //
  DATALINK(java.sql.Types.DATALINK, false), //
  BOOLEAN(java.sql.Types.BOOLEAN, false, Boolean.class), //
  ROWID(java.sql.Types.ROWID, false), //
  NCHAR(java.sql.Types.NCHAR, false, String.class), //
  NVARCHAR(java.sql.Types.NVARCHAR, false, String.class), //
  LONGNVARCHAR(java.sql.Types.LONGNVARCHAR, false, String.class), //
  NCLOB(java.sql.Types.NCLOB, false), //
  SQLXML(java.sql.Types.SQLXML, false);
  //
  private static final Map<Integer, SQLType> MAP = new HashMap<>();
  static {
    for (final SQLType sqlType : SQLType.values()) {
      MAP.put(sqlType.getCode(), sqlType);
    }
  }
  private final int code;
  private final boolean numeric;
  private final Class<?> javaType;
  private final Class<?> primitiveType;

  private SQLType(final int code, final boolean numeric) {
    this(code, numeric, null);
  }

  private SQLType(final int code, final boolean numeric, final Class<?> javaType) {
    this(code, numeric, javaType, null);
  }

  private SQLType(final int code, final boolean numeric, final Class<?> javaType,
      final Class<?> primitiveType) {
    this.code = code;
    this.numeric = numeric;
    this.javaType = javaType;
    this.primitiveType = primitiveType;
  }

  public int getCode() {
    return code;
  }

  public boolean isNumeric() {
    return numeric;
  }

  public Class<?> getJavaType() {
    return javaType;
  }

  public Class<?> getPrimitiveType() {
    return primitiveType;
  }

  public boolean acceptType(final Class<?> type) {
    if (type.equals(LocalDate.class) && javaType.equals(java.sql.Date.class)) {
      return true;
    } else {
      return type.equals(javaType) || type.equals(primitiveType);
    }
  }

  @Nullable
  public static SQLType parse(final int code) {
    return MAP.get(code);
  }
}
