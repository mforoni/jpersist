package com.gitlab.mforoni.jpersist;

import static com.google.common.base.Preconditions.checkNotNull;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import com.github.mforoni.jbasic.JStrings;
import com.gitlab.mforoni.jpersist.DBRow.State;
import com.gitlab.mforoni.jpersist.PKMetaData.PKColumn;
import com.google.common.annotations.Beta;

/**
 * This class consists of {@code static} utility methods for building SQL statements (
 * <a href="https://en.wikipedia.org/wiki/SQL">SQL@wikipedia</a>).
 * <ul>
 * <li>{@link #insertInto(DBTable)}</li>
 * <li>{@link #update(DBRow)}</li>
 * <li>{@link #deleteTable(String, String)}</li> FIXME completare
 * </ul>
 * 
 * @author Marco Foroni
 */
@Beta
final class SQLStatements {
  public static final int DEFAULT_ALTER_TABLE_QUERY_LENGTH = 50;

  // Suppresses default constructor, ensuring non-instantiability.
  private SQLStatements() {
    throw new AssertionError();
  }

  @Nonnull
  public static String select(@Nonnull final Class<?> type) {
    return String.format("select * from %s", Annotations.getTableName(type));
  }

  @Nonnull
  public static String select(@Nonnull final Class<?> type,
      @Nonnull final Collection<Field> fields) {
    final StringBuilder sb =
        new StringBuilder("select * from ").append(Annotations.getTableName(type));
    final Iterator<Field> iterator = fields.iterator();
    if (iterator.hasNext()) {
      sb.append(" where ").append(Annotations.getColumnName(iterator.next())).append("=?");
      while (iterator.hasNext()) {
        sb.append(" and ").append(Annotations.getColumnName(iterator.next())).append("=?");
      }
    }
    return sb.toString();
  }

  public static String insertInto(@Nonnull final Class<?> type,
      @Nonnull final Collection<Field> columns) {
    final StringBuilder sb =
        new StringBuilder("INSERT INTO ").append(Annotations.getTableName(type)).append(" (");
    int counter = 0;
    boolean first = true;
    for (final Field f : columns) {
      if (f.getAnnotation(GeneratedValue.class) == null) {
        if (first) {
          sb.append(Annotations.getColumnName(f));
          first = false;
        } else {
          sb.append(", ").append(Annotations.getColumnName(f));
        }
        counter++;
      }
    }
    sb.append(") VALUES (").append(JStrings.concat("?", counter, ", ")).append(")");
    return sb.toString();
  }

  public static String update(@Nonnull final Class<?> type,
      @Nonnull final Collection<Field> columns, @Nonnull final Collection<Field> idFields) {
    final StringBuilder sb =
        new StringBuilder("UPDATE ").append(Annotations.getTableName(type)).append(" SET ");
    boolean first = true;
    for (final Field f : columns) {
      if (f.getAnnotation(Id.class) == null) {
        if (first) {
          sb.append(Annotations.getColumnName(f));
          first = false;
        } else {
          sb.append(", ").append(Annotations.getColumnName(f));
        }
      }
    }
    sb.append(" WHERE ");
    final Iterator<Field> pkIterator = idFields.iterator();
    if (pkIterator.hasNext()) {
      sb.append(Annotations.getColumnName(pkIterator.next())).append("=?");
      while (pkIterator.hasNext()) {
        sb.append(", ").append(Annotations.getColumnName(pkIterator.next())).append("=?");
      }
    }
    return sb.toString();
  }

  public static String delete(@Nonnull final Class<?> type,
      @Nonnull final Collection<Field> idFields) {
    final StringBuilder sb =
        new StringBuilder("DELETE FROM ").append(Annotations.getTableName(type)).append(" WHERE ");
    final Iterator<Field> pkIterator = idFields.iterator();
    if (pkIterator.hasNext()) {
      sb.append(Annotations.getColumnName(pkIterator.next())).append("=?");
      while (pkIterator.hasNext()) {
        sb.append(", ").append(Annotations.getColumnName(pkIterator.next())).append("=?");
      }
    }
    return sb.toString();
  }

  @Nonnull
  public static String selectOneRow(final String tableName) {
    return "SELECT TOP 1 * FROM " + tableName;
  }

  /**
   * Builds a string representing an update SQL statement for all the column values modified in the
   * specified {@code DBRow}.
   * 
   * @param dbRow a not null {@code DBRow}
   * @return an update SQL statement as string
   * @throws IllegalArgumentException if the state of {@code DBRow} is not {@link State#UPDATED}
   * @see DBRow
   * @see DBTable
   */
  public static String updateModified(@Nonnull final DBRow dbRow) {
    checkNotNull(dbRow);
    if (dbRow.getState() != State.UPDATED) {
      throw new IllegalArgumentException("DBRow state is not correct: must be UPDATED");
    }
    final DBTable dbTable = dbRow.getDBTable();
    final String tableName = dbTable.getTableDescription().getQualifiedName();
    final StringBuilder query = new StringBuilder(11 + tableName.length() + dbRow.getSize() * 20);
    query.append("UPDATE ").append(tableName).append(" SET ");
    boolean first = true;
    for (final ColumnMetaData columnMetaData : dbTable.getColumnMetaDatas()) {
      if (dbRow.isModified(columnMetaData.getColumnName())) {
        if (first) {
          query.append(columnMetaData.getColumnName()).append("=?");
          first = false;
        } else {
          query.append(", ").append(columnMetaData.getColumnName()).append("=?");
        }
      }
    }
    final List<PKColumn> pkColumns = dbTable.getPKMetaData().getColumns();
    query.append(" WHERE ").append(pkColumns.get(0).getName()).append("=?");
    for (int i = 1; i < pkColumns.size(); i++) {
      query.append(" AND ").append(pkColumns.get(i).getName()).append("=?");
    }
    return query.toString();
  }

  public static String update(@Nonnull final DBRow dbRow) {
    checkNotNull(dbRow);
    if (dbRow.getState() != State.UPDATED) {
      throw new IllegalArgumentException("DBRow state is not correct: must be UPDATED");
    }
    final DBTable dbTable = dbRow.getDBTable();
    final String tableName = dbTable.getTableDescription().getQualifiedName();
    final StringBuilder query = new StringBuilder(11 + tableName.length() + dbRow.getSize() * 20);
    query.append("UPDATE ").append(tableName).append(" SET ");
    final Iterator<ColumnMetaData> iterator = dbTable.getColumnMetaDatas().iterator();
    if (iterator.hasNext()) {
      ColumnMetaData columnMetaData = iterator.next();
      query.append(columnMetaData.getColumnName()).append("=?");
      while (iterator.hasNext()) {
        columnMetaData = iterator.next();
        query.append(", ").append(columnMetaData.getColumnName()).append("=?");
      }
    }
    final List<PKColumn> pkColumns = dbTable.getPKMetaData().getColumns();
    query.append(" WHERE ").append(pkColumns.get(0).getName()).append("=?");
    for (int i = 1; i < pkColumns.size(); i++) {
      query.append(" AND ").append(pkColumns.get(i).getName()).append("=?");
    }
    return query.toString();
  }

  /**
   * Returns an <i>insert into</i> SQL statement for all the columns of the specified database
   * table. The parameters are set as question marks so it can be used in a
   * {@code PreparedStatement}.
   * 
   * @param dbTable a {@code DBTable}
   * @return an <i>insert into</i> SQL statement for all the columns of the specified database
   *         table.
   * @see DBTable
   */
  public static String insertInto(@Nonnull final DBTable dbTable) {
    final String tableName = dbTable.getTableDescription().getQualifiedName();
    final StringBuilder query = new StringBuilder(
        12 + tableName.length() + dbTable.getColumns() * 15 + 10 + dbTable.getColumns() * 3 + 1);
    query.append("INSERT INTO ");
    query.append(tableName);
    final Iterator<ColumnMetaData> iterator = dbTable.getColumnMetaDatas().iterator();
    if (iterator.hasNext()) {
      ColumnMetaData columnMetaData = iterator.next();
      query.append(" (" + columnMetaData.getColumnName());
      while (iterator.hasNext()) {
        columnMetaData = iterator.next();
        query.append(", ");
        query.append(columnMetaData.getColumnName());
      }
    }
    query.append(") VALUES (?");
    for (int col = 1; col < dbTable.getColumns(); col++) {
      query.append(", ?");
    }
    query.append(")");
    return query.toString();
  }

  @Nonnull
  public static String dropTable(@Nonnull final DBTable dbTable) {
    return truncateTable(dbTable.getTableDescription().getSchema(),
        dbTable.getTableDescription().getName());
  }

  @Nonnull
  public static String dropTable(@Nullable final String schema, @Nonnull final String tableName) {
    String sql = "DROP TABLE ";
    if (schema != null) {
      sql = sql.concat(schema).concat(".");
    }
    sql = sql.concat(tableName);
    return sql;
  }

  @Nonnull
  public static String truncateTable(@Nonnull final DBTable dbTable) {
    return truncateTable(dbTable.getTableDescription().getSchema(),
        dbTable.getTableDescription().getName());
  }

  @Nonnull
  public static String truncateTable(@Nullable final String schema,
      @Nonnull final String tableName) {
    String sql = "TRUNCATE TABLE ";
    if (schema != null) {
      sql = sql.concat(schema).concat(".");
    }
    sql = sql.concat(tableName);
    return sql;
  }

  @Nonnull
  public static String deleteTable(@Nonnull final DBTable dbTable) {
    return deleteTable(dbTable.getTableDescription().getSchema(),
        dbTable.getTableDescription().getName());
  }

  @Nonnull
  public static String deleteTable(@Nullable final String schema, @Nonnull final String tableName) {
    String sql = "DELETE * FROM ";
    if (schema != null) {
      sql = sql.concat(schema).concat(".");
    }
    sql = sql.concat(tableName);
    return sql;
  }

  /**
   * Builds an <i>alter table</i> sql statement for setting the primary key of table
   * {@code tableName}.
   *
   * @param tableName the table name
   * @param constraintName the constraint name
   * @param pkColumnNames the primary key column names
   * @return the string
   */
  @Beta
  public static String buildAlterTablePrimaryKey(final String tableName,
      final String constraintName, final List<String> pkColumnNames) {
    if (pkColumnNames == null || pkColumnNames.size() == 0) {
      throw new IllegalArgumentException(
          "The list of primary key column names cannot be empty or null");
    }
    final StringBuilder query = new StringBuilder(DEFAULT_ALTER_TABLE_QUERY_LENGTH);
    query.append("ALTER TABLE ").append(tableName);
    query.append(" ADD CONSTRAINT ").append(constraintName);
    query.append(" PRIMARY KEY(").append(pkColumnNames.get(0));
    for (int i = 1; i < pkColumnNames.size(); i++) {
      query.append(", ").append(pkColumnNames.get(i));
    }
    query.append(");");
    return query.toString();
  }
}
