package com.gitlab.mforoni.jpersist.query;

import static com.gitlab.mforoni.jpersist.query.Query.DESC;
import static com.gitlab.mforoni.jpersist.query.Query.DOT;
import static com.gitlab.mforoni.jpersist.query.Query.ORDER_BY;
import javax.annotation.concurrent.Immutable;

/**
 * Immutable class representing a <i>order by clause</i> of a SQL statement.
 * 
 * @author Foroni
 */
@Immutable
public final class OrderBy {
  private final String expression;

  public enum Order {
    DESCENDING, ASCENDING
  }

  public OrderBy(final String columnName) {
    this(new OrderBy.Builder(columnName));
  }

  public OrderBy(final String alias, final String columnName) {
    this(new OrderBy.Builder(alias, columnName));
  }

  public OrderBy(final String columnName, final Order order) {
    this(new OrderBy.Builder(null, columnName, order));
  }

  private OrderBy(final Builder builder) {
    this.expression = builder.sb.toString();
  }

  public String getExpression() {
    return expression;
  }

  @Override
  public String toString() {
    return expression;
  }

  public static class Builder {
    private final StringBuilder sb;

    public Builder(final String columnName) {
      this(null, columnName, Order.ASCENDING);
    }

    public Builder(final String alias, final String columnName) {
      this(alias, columnName, Order.ASCENDING);
    }

    public Builder(final String columnName, final Order order) {
      this(null, columnName, order);
    }

    public Builder(final String alias, final String columnName, final Order order) {
      sb = new StringBuilder(ORDER_BY).append(' ');
      sb.append(build(alias, columnName, order));
    }

    private String build(final String alias, final String columnName, final Order order) {
      final StringBuilder sb = new StringBuilder();
      if (alias != null) {
        sb.append(alias).append(DOT);
      }
      sb.append(columnName);
      if (order == Order.DESCENDING) {
        sb.append(' ').append(DESC);
      }
      return sb.toString();
    }

    public Builder comma(final String columnName) {
      sb.append(", ").append(build(null, columnName, Order.ASCENDING));
      return this;
    }

    public Builder comma(final String columnName, final Order order) {
      sb.append(", ").append(build(null, columnName, order));
      return this;
    }

    public Builder comma(final String alias, final String columnName) {
      sb.append(", ").append(build(alias, columnName, Order.ASCENDING));
      return this;
    }

    public Builder comma(final String alias, final String columnName, final Order order) {
      sb.append(", ").append(build(alias, columnName, order));
      return this;
    }

    public OrderBy toOrderBy() {
      return new OrderBy(this);
    }
  }
}
