package com.gitlab.mforoni.jpersist.query;

import java.util.Arrays;
import java.util.List;
import javax.annotation.concurrent.Immutable;
import com.gitlab.mforoni.jpersist.query.Query.JoinType;

/**
 * Immutable class representing one or more join conditions part of an <i>SQL statement</i>.
 * 
 * @author Foroni
 * @see Select
 */
@Immutable
public final class Join {
  private final String statement;

  private Join(final String statement) {
    this.statement = statement;
  }

  public String getStatement() {
    return statement;
  }

  public static class Builder {
    private final StringBuilder statement;

    public Builder(final String alias, final String table, final String condition) {
      this(JoinType.INNER_JOIN, alias, table, condition);
    }

    public Builder(final JoinType type, final String alias, final String table,
        final String condition) {
      statement = new StringBuilder(buildExpression(type, alias, table, condition));
    }

    public Builder join(final String alias, final String table, final String condition) {
      return join(JoinType.INNER_JOIN, alias, table, condition);
    }

    public Builder join(final JoinType type, final String alias, final String table,
        final String condition) {
      statement.append(' ').append(buildExpression(type, alias, table, condition));
      return this;
    }

    public Join toJoin() {
      return new Join(statement.toString());
    }
  }

  public static String buildExpression(final JoinType joinType, final String alias,
      final String table, final String condition) {
    return new StringBuilder(joinType.sql).append(' ').append(table).append(' ').append(alias)
        .append(' ').append(Query.ON).append(' ').append(condition).toString();
  }

  public static String buildCondition(final String alias1, final String alias2,
      final String column) {
    return buildCondition(alias1, alias2, Arrays.asList(column));
  }

  public static String buildCondition(final String alias1, final String alias2,
      final List<String> columns) {
    if (columns == null || columns.size() == 0) {
      throw new IllegalStateException();
    }
    final StringBuilder sb = new StringBuilder(Select.qualify(alias1, columns.get(0)));
    sb.append('=').append(Select.qualify(alias2, columns.get(0)));
    for (int i = 1; i < columns.size(); i++) {
      sb.append(' ').append(Query.AND).append(' ');
      sb.append(Select.qualify(alias1, columns.get(i)));
      sb.append('=').append(Select.qualify(alias2, columns.get(i)));
    }
    return sb.toString();
  }
}
