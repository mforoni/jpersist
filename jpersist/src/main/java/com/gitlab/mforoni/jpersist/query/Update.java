package com.gitlab.mforoni.jpersist.query;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import com.google.common.collect.ImmutableList;

/**
 * Immutable class representing an <i>SQL update statement</i>.
 * 
 * @author Foroni
 */
@Immutable
public final class Update extends AbstractQuery {
  private Update(final String statement, final ImmutableList<Object> parameters) {
    super(statement, parameters);
  }

  public static class Builder {
    private Where where;
    private final StringBuilder statement;
    private final List<Object> parameters;

    public Builder(@Nonnull final String tableName, @Nonnull final String columnName,
        @Nonnull final Object value) {
      statement = new StringBuilder(UPDATE).append(' ').append(tableName).append(" SET ")
          .append(columnName).append("=?");
      parameters = new ArrayList<>();
      parameters.add(value);
      where = null;
    }

    public Builder set(final String columnName, @Nonnull final Object value) {
      statement.append(", ").append(columnName).append("=?");
      parameters.add(value);
      return this;
    }

    public Builder where(final Where where) {
      this.where = where;
      return this;
    }

    public Update toUpdate() {
      if (where != null) {
        statement.append(' ').append(where.getExpression());
        parameters.addAll(where.getParameters());
      }
      return new Update(statement.toString(), ImmutableList.copyOf(parameters));
    }
  }
}
