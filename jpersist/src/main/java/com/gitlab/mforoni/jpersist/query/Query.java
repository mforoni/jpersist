package com.gitlab.mforoni.jpersist.query;

import java.util.List;
import javax.annotation.Nonnull;

/**
 * @author Foroni
 */
public interface Query {
  /*
   * Come gestire i metadati dei parametri? Se hai a disposizione i column names potresti ricavare i
   * metadati dal DBTable. Problema se query in join servono più dbtable e vi potrebbero essere
   * collisioni. Nota: I metadati dei campi selezionati in query di tipo select non vengono
   * memorizzati, il DBConnector usa i metadati della DBTable corrispondente.
   */
  public static final String SELECT = "select";
  public static final String MAX = "Max";
  public static final String MIN = "Min";
  public static final String COUNT = "Count";
  public static final String FROM = "from";
  public static final String INNER_JOIN = "inner join";
  public static final String ON = "on";
  public static final String WHERE = "where";
  public static final String AND = "and";
  public static final String DOT = ".";
  public static final String ORDER_BY = "order by";
  public static final String DESC = "desc";
  public static final String ROWNUM = "ROWNUM";
  public static final String UPDATE = "update";
  public static final String DELETE_FROM = "delete from";
  public static final String SET = "set";

  public enum Operator {
    EQUAL, NOT_EQUAL, MINOR, MAJOR, LIKE, NOT_LIKE, IN, NOT_IN // isNull, notNull
  };
  public enum Function {
    SUM, AVG, COUNT, MIN, MAX, UPPER, COALESCE
  }
  public enum JoinType {
    INNER_JOIN("inner join"), LEFT_JOIN("left join"), RIGHT_JOIN("right join");
    final String sql;

    private JoinType(final String sql) {
      this.sql = sql;
    }
  }

  @Nonnull
  public String getStatement();

  @Nonnull
  public List<Object> getParameters();
}
