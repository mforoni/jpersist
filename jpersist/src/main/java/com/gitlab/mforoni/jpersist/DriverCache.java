package com.gitlab.mforoni.jpersist;

import java.util.HashSet;
import java.util.Set;
import javax.annotation.Nonnull;
import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

enum DriverCache {
  INSTANCE;

  private static final Logger LOGGER = LoggerFactory.getLogger(DriverCache.class);
  /**
   * Saving driver analyzed and therefore available
   */
  private final Set<Driver> drivers = new HashSet<>();

  /**
   * The need for registering <i>JDBC driver</i> depends on the driver itself and if you are using a
   * JDK older than JDK 6:
   * <ul>
   * <li>any JDBC 4.0 (Java 1.6) drivers that are found in your class path are automatically loaded.
   * However, even if you are using a 1.6 JDK you must manually load any drivers prior to JDBC 4.0
   * with the method <i>Class.forName</i></li>
   * <li>in previous versions of JDBC, to obtain a connection, you first had to initialize your JDBC
   * driver by calling the method <i>Class.forName</i>. This methods required an object of type
   * java.sql.Driver. Each JDBC driver contains one or more classes that implements the interface
   * java.sql.Driver. E.g. ojdbc14 is a JDBC 3.0 (Java 1.5) driver so ojdbc14.jar needs to be
   * manually loaded even on Java 1.6.</li>
   * </ul>
   * Therefore from JDK6 onward each JDBC 4.0 drivers automatically get loaded if JVM found any
   * Driver class in classpath. More info about JDBC driver can be found at
   * <a href="https://en.wikipedia.org/wiki/JDBC_driver">wikipedia</a>.
   * <p>
   * <b>Note that JDBC-ODBC Bridge was removed in Java8</b>.
   * <p>
   * More info can be found at:
   * <ul>
   * <li><a href="http://www.onjava.com/2006/08/02/jjdbc-4-enhancements-in-java-se-6.html">link
   * 1</a></li>
   * <li></li>
   * </ul>
   * http://stackoverflow.com/questions/13959202/loading-jdbc-driver-without-class-forname
   * http://stackoverflow.com/questions/23255766/no-suitable-driver-found-when-making-sql-connection-to-ms-access-database-in-jav
   * http://stackoverflow.com/questions/5992126/loading-jdbc-driver
   *
   * @param driver
   * @return
   * @throws IllegalStateException
   * @throws IllegalArgumentException
   */
  void initializeJdbcDriver(@Nonnull final Driver driver) throws IllegalStateException {
    if (drivers.contains(driver)) {
      LOGGER.debug("Driver {} already analyzed", driver);
      return;
    } else {
      load(driver);
      drivers.add(driver);
    }
  }

  private static void load(@Nonnull final Driver driver) throws IllegalStateException {
    if (driver.getType() < 4 || !SystemUtils.isJavaVersionAtLeast(JavaVersion.JAVA_1_6)) {
      // if (!SystemUtils.isJavaVersionAtLeast(160)) {
      LOGGER.debug("Driver {} need to be manually loaded", driver);
      switch (driver) {
        case ORACLE_14:
          loadOracleJdbcDriver();
          break;
        case ORACLE_6:
          throw new IllegalStateException("Type 4 JDBC driver ojdbc6 requires JDK 6");
        case ODBC_JDBC:
          loadOdbcJdbcBridge();
          break;
        case UCANACCESS:
          throw new IllegalStateException("Type 4 JDBC driver UCanAccess requires JDK 6");
        case HSQLDB:
          throw new IllegalStateException("Type 4 JDBC driver HSQLDB requires JDK 6");
      }
    } else {
      LOGGER.debug("Driver {} does not need to be manually loaded", driver);
    }
  }

  private static void loadDriver(final String className) throws IllegalStateException {
    try {
      Class.forName(className);
    } catch (ClassNotFoundException e) {
      throw new IllegalStateException("Missing driver " + className, e);
    }
  }

  private static void loadOracleJdbcDriver() throws IllegalStateException {
    loadDriver("oracle.jdbc.OracleDriver");
    LOGGER.debug("Oracle JDBC Driver Registered!");
  }

  /**
   * Problem: using a 64 bit program as a 64 bit JDK to connect to a 32 bit access database is not
   * possible, see this link:
   * http://stackoverflow.com/questions/10289655/how-to-connect-to-a-32-bit-access-database-from-64-bit-jvm
   * <p>
   * Il problema è non posso usare una JDK con architettura (32 o 64) diversa da quella dell'
   * installazione di MS Access (Office)
   *
   * @throws ClassNotFoundException
   */
  private static void loadOdbcJdbcBridge() throws IllegalStateException {
    loadDriver("sun.jdbc.odbc.JdbcOdbcDriver");
    LOGGER.debug("SUN JDBC ODBC Driver Registered!");
    // TODO andrebbe verificato che la JDK e la versione di MS Access siano entrambe a 32-bit o
    // entrambe
    // a 64 bit.
    // JVM architecture (32bit or 64bit):
    final String jvmArch = System.getProperty("sun.arch.data.model");
    LOGGER.debug("JVM architecture (32-bit or 64-bit): {}", jvmArch);
    if (Integer.parseInt(jvmArch) != 32) {
      LOGGER.warn(
          "Only 32 bit JVM can connect to a MS Access database created with 32 bit Office suite");
      throw new IllegalStateException(
          "Only 32 bit JVM can connect to a MS Access database created with 32 bit Office suite using ODBC bridge");
    }
  }
}
