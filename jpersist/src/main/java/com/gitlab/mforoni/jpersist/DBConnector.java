package com.gitlab.mforoni.jpersist;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import com.gitlab.mforoni.jpersist.DBRow.State;
import com.gitlab.mforoni.jpersist.query.Join;
import com.gitlab.mforoni.jpersist.query.OrderBy;
import com.gitlab.mforoni.jpersist.query.Query;
import com.gitlab.mforoni.jpersist.query.Select;
import com.gitlab.mforoni.jpersist.query.Where;
import com.google.common.annotations.Beta;

public interface DBConnector extends AutoCloseable {
  boolean isReadOnly();

  /**
   * Returns the {@link DBMetaData} instance associated with this {@code DBConnector}.
   *
   * @return the {@code Database}
   */
  @Nonnull
  DBMetaData getDBMetaData();

  DriverMetaData getDriverMetaData() throws SQLException;

  boolean getAutoCommit() throws SQLException;

  /**
   * Verifies jdbc connection status, check if the physical connection is closed.
   *
   * @return <tt>true</tt> if connection is closed, <tt>false</tt> otherwise
   * @see Connection#isClosed()
   */
  boolean isConnectionClosed() throws SQLException;

  /**
   * Verifies jdbc connection status, check if the physical connection is valid.
   *
   * @param timeout the time in seconds to wait for the database operation used to validate the
   *        connection to complete. If the timeout period expires before the operation completes,
   *        this method returns false. A value of 0 indicates a timeout is not applied to the
   *        database operation.
   * @return <tt>true</tt> if connection is valid, </tt>false</tt> otherwise
   * @throws SQLException
   * @see Connection#isValid(int)
   */
  boolean isConnectionValid(int timeout) throws SQLException;

  /**
   * Verifies jdbc connection status with a timeout of 30 seconds.
   *
   * @return <tt>true</tt> if connection is valid, </tt>false</tt> otherwise
   * @throws SQLException
   * @see Connection#isValid(int)
   */
  boolean isConnectionValid() throws SQLException;

  @Nullable
  Number executeSelectNumber(@Nonnull Select query) throws SQLException;

  @Nullable
  Number executeSelectNumber(@Nonnull String statement, Object... parameters) throws SQLException;

  /**
   * Executes the specified <i>SQL SELECT statement</i>. Returns a list of arrays retrieved from the
   * database.
   * <p>
   * Opens, uses and close a {@code ResultSet}. Opens, uses and, if no caching mechanism is enabled
   * in {@code PreparedStatementProvider}, closes one {@code PreparedStatement}.
   *
   * @param query
   * @return
   * @throws SQLException
   * @see ResultSet
   * @see PreparedStatement
   * @see PreparedStatementProvider
   * @see PreparedStatementProvider#release(PreparedStatement)
   */
  @Nonnull
  List<Object[]> executeSelect(@Nonnull Select query) throws SQLException;

  /**
   * Executes the specified <i>SQL SELECT statement</i>. Returns a list of arrays retrieved from the
   * database.
   * <p>
   * Opens, uses and closes a {@link PreparedStatement} and a {@link ResultSet}.
   *
   * @param statement
   * @param parameters
   * @return
   * @throws SQLException
   * @see ResultSet
   * @see PreparedStatement
   */
  @Nonnull
  List<Object[]> executeSelect(@Nonnull String statement, Object... parameters) throws SQLException;

  /**
   * Executes the SQL statement specified in the String {@code statement}, which must be an SQL Data
   * Manipulation Language (DML) statement, such as INSERT, UPDATE or DELETE; or an SQL statement
   * that returns nothing, such as a DDL statement.
   * <p>
   * All the parameters are set with {@link PreparedStatement#setObject(int, Object)} method.
   *
   * @param statement the SQL statement (cannot be <tt>null</tt>)
   * @param parameters a list of parameters
   * @return either (1) the row count for SQL Data Manipulation Language (DML) statements or (2) 0
   *         for SQL statements that return nothing
   * @throws SQLException
   * @see PreparedStatement#executeUpdate()
   */
  int executeUpdate(@Nonnull String statement, Object... parameters) throws SQLException;

  /**
   * Executes the SQL statement specified in the {@link Query} object, which must be an SQL Data
   * Manipulation Language (DML) statement, such as INSERT, UPDATE or DELETE; or an SQL statement
   * that returns nothing, such as a DDL statement.
   *
   * @param query the update {@code Query}
   * @return either (1) the row count for SQL Data Manipulation Language (DML) statements or (2) 0
   *         for SQL statements that return nothing
   * @throws SQLException
   * @see Query
   * @see PreparedStatement#executeUpdate()
   */
  int executeUpdate(@Nonnull Query query) throws SQLException;

  /**
   * Note: {@link Statement#SUCCESS_NO_INFO}
   * 
   * @param query
   * @param parameters
   * @return
   * @throws SQLException
   */
  @Beta
  int[] executeBatch(@Nonnull String sql, List<Object>[] parameters) throws SQLException;

  boolean supportBatchUpdates() throws SQLException;

  List<DBRow> select(@Nonnull final DBTable dbTable, @Nullable final Join join,
      @Nullable final Where where, @Nullable final OrderBy orderBy) throws SQLException;

  /**
   * Inserts one {@link DBRow} in the database. Executes one SQL insert statement.
   * <p>
   * Opens, uses and closes one {@link PreparedStatement}.
   *
   * @param dbRow
   * @return <tt>1</tt> if the row has been inserted, <tt>0</tt> otherwise.
   * @throws SQLException
   * @throws UnsupportedOperationException if the connector is created in read-only mode
   */
  int insert(@Nonnull DBRow dbRow) throws SQLException;

  /**
   * Inserts a list of {@link DBRow} belonging to the same {@link DBTable} in the database. Executes
   * as many SQL insert statements as the rows size. If the {@code dbRows} belongs to different
   * table an {@code IllegalArgumentException} is thrown.<br>
   * Helpful when the list of record to insert has a size in the hundreds.
   * <p>
   * Opens, uses and, if no caching mechanism is enabled in {@link PreparedStatementProvider},
   * closes one {@link PreparedStatement}.
   *
   * @param dbRows
   * @return <tt>N</tt> if <tt>N</tt> rows have been inserted, <tt>0</tt> otherwise.
   * @throws SQLException
   * @throws UnsupportedOperationException if the connector is created in read-only mode
   * @throws IllegalArgumentException if db
   */
  int insert(@Nonnull List<DBRow> dbRows) throws SQLException;

  /**
   * Executes one SQL update statements. The {@code dbRow} must have state equals to
   * {@link State#UPDATED}. If so, all the values in {@code dbRow} are updated in the database.
   *
   * @param dbRow
   * @return <tt>1</tt> if the row has been updated, <tt>0</tt> otherwise.
   * @throws SQLException
   */
  int update(@Nonnull DBRow dbRow) throws SQLException;

  /**
   * Update all the {@link DBRow}s specified as parameter. Executes as many SQL update statements as
   * the {@code dbRows} list size. All the {@code dbRows} must have state equals to
   * {@link State#UPDATED} and must refer to the same {@link DBTable}. If the fields modified are
   * always the same for all the {@code dbRows} it uses only one {@code PreparedStatement},
   * otherwise uses all the necessary.
   * <p>
   * Opens, uses and, closes one {@link PreparedStatement}.
   *
   * @param dbRows
   * @return <tt>N</tt> if <tt>N</tt> rows have been updated, <tt>0</tt> otherwise.
   * @throws SQLException
   * @throws IllegalArgumentException if the list of {@code DBRow} is <tt>null</tt> or empty
   * @see PreparedStatement#executeUpdate()
   * @see PreparedStatementProvider
   */
  int update(@Nonnull List<DBRow> dbRows) throws SQLException;

  int delete(@Nonnull DBRow dbRow) throws SQLException;

  int delete(@Nonnull List<DBRow> dbRows) throws SQLException;

  <T> List<T> select(@Nonnull final Class<T> type) throws SQLException;

  <T> T selectByPK(@Nonnull final Class<T> type, @Nonnull final Object value,
      final Object... values) throws SQLException;

  <T> T selectByUniqueConstraint(@Nonnull final Class<T> type,
      @Nonnull final Collection<Field> fields, @Nonnull final Object value, final Object... values)
      throws SQLException;

  <T> void insert(@Nonnull final T instance, @Nonnull final Class<T> type) throws SQLException;

  <T> void update(@Nonnull final T instance, @Nonnull final Class<T> type) throws SQLException;

  <T> void delete(@Nonnull final T instance, @Nonnull final Class<T> type) throws SQLException;
}
