package com.gitlab.mforoni.jpersist;

import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import com.github.mforoni.jbasic.JEnums;
import com.google.common.annotations.Beta;
import com.google.common.base.Preconditions;

/**
 * Immutable class representing a description of a database table. This class contains:
 * <ul>
 * <li>the catalog name (may be <tt>null</tt>)</li>
 * <li>the schema name (may be <tt>null</tt>)</li>
 * <li>the table name (cannot be <tt>null</tt>)</li>
 * <li>the table type: typical types are "TABLE", "VIEW", "SYSTEM TABLE", "GLOBAL TEMPORARY", "LOCAL
 * TEMPORARY", "ALIAS", "SYNONYM"</li>
 * </ul>
 * 
 * @author Marco Foroni
 */
@Immutable
public final class TableDesc {
  private final String catalog;
  private final String schema;
  private final String name;
  private final String type;

  /**
   * Instantiates a new {@code TableDescription}
   *
   * @param catalog the catalog (may be <tt>null</tt>)
   * @param schema the schema (may be <tt>null</tt>)
   * @param name the table name (cannot be <tt>null</tt>)
   * @param type the table type (cannot be <tt>null</tt>)
   */
  public TableDesc(@Nullable final String catalog, @Nullable final String schema,
      @Nonnull final String name, @Nonnull final String type) {
    Preconditions.checkNotNull(name);
    Preconditions.checkNotNull(type);
    this.catalog = catalog;
    this.schema = schema;
    this.name = name;
    this.type = type;
  }

  /**
   * Gets the catalog.
   *
   * @return the catalog
   */
  @Nullable
  public String getCatalog() {
    return catalog;
  }

  /**
   * Gets the schema.
   *
   * @return the schema
   */
  @Nullable
  public String getSchema() {
    return schema;
  }

  /**
   * Gets the table name.
   *
   * @return the table name
   */
  @Nonnull
  public String getName() {
    return name;
  }

  /**
   * Gets the table type.
   *
   * @return the table type
   */
  @Nonnull
  public String getType() {
    return type;
  }

  @Nonnull
  @Beta
  public TableType getTableType() {
    return JEnums.conventionalValueOf(TableType.class, type);
  }

  /**
   * Returns {@code schema.name} if {@code schema != null}, otherwise returns {@code name}
   *
   * @return {@code schema.name} if {@code schema != null}, otherwise returns {@code name}
   */
  @Nonnull
  public String getQualifiedName() {
    return (schema != null) ? schema.concat(".").concat(name) : name;
  }

  /**
   * Returns the fully qualified table name
   * http://stackoverflow.com/questions/17904642/does-using-fully-qualified-names-affect-performace
   *
   * @return
   */
  @Nonnull
  public String getFullyQualifiedName() {
    String fqn = catalog != null ? catalog.concat(".") : "";
    if (schema != null) {
      fqn = fqn.concat(schema).concat(".");
    }
    fqn = fqn.concat(name);
    return fqn;
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + ((catalog == null) ? 0 : catalog.hashCode());
    result = 31 * result + ((schema == null) ? 0 : schema.hashCode());
    result = 31 * result + name.hashCode();
    result = 31 * result + ((type == null) ? 0 : type.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final TableDesc other = (TableDesc) obj;
    if (!Objects.equals(catalog, other.catalog)) {
      return false;
    }
    if (!Objects.equals(schema, other.schema)) {
      return false;
    }
    if (!name.equals(other.name)) {
      return false;
    }
    if (!Objects.equals(type, other.type)) {
      return false;
    }
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Table ");
    sb.append(getFullyQualifiedName()).append(" (").append(type).append(')');
    return sb.toString();
  }
}
