package com.gitlab.mforoni.jpersist;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import com.github.mforoni.jbasic.JStrings;
import com.google.common.collect.ImmutableList;

/**
 * Immutable class useful for representing a database table definition DDL.
 *
 * @author Marco Foroni
 */
@Immutable
public final class TableDef {

  private static final String CREATE_TABLE = "CREATE TABLE";
  static final String NOT_NULL = "NOT NULL";
  static final String PRIMARY_KEY = "PRIMARY KEY";
  private static final String CONSTRAINT = "CONSTRAINT";
  private final String tableName;
  private final ImmutableList<ColumnDef> columnDefinitions;
  private final ImmutableList<String> pkNames;
  private final String constraintName;

  /**
   * Instantiates a new table definition.
   *
   * @param builder the builder
   */
  private TableDef(final Builder builder) {
    this.tableName = builder.tableName;
    this.columnDefinitions = ImmutableList.copyOf(builder.columnDefinitions);
    this.pkNames = ImmutableList.copyOf(builder.pkNames);
    this.constraintName = builder.constraintName;
  }

  /**
   * Gets the table name.
   *
   * @return the table name
   */
  public String getTableName() {
    return tableName;
  }

  public ImmutableList<ColumnDef> getColumnDefinitions() {
    return columnDefinitions;
  }

  public int getPkSize() {
    return pkNames.size();
  }

  @Override
  public String toString() {
    return "TableDef [tableName=" + tableName + "]";
  }

  public String toSQL() {
    return toSQL(JStrings.NEWLINE);
  }

  public String toSQL(@Nullable final String separator) {
    final StringBuilder sb = new StringBuilder(columnDefinitions.size() * 30);
    sb.append(CREATE_TABLE).append(' ').append(tableName).append(" (");
    if (separator != null) {
      sb.append(separator);
    }
    final boolean excludePK = (getPkSize() > 1) ? true : false;
    for (int i = 0; i < columnDefinitions.size(); i++) {
      final ColumnDef columnDefinition = columnDefinitions.get(i);
      sb.append("\t").append(columnDefinition.toSQL(excludePK));
      if (i != columnDefinitions.size() - 1) {
        sb.append(',');
        if (separator != null) {
          sb.append(separator);
        }
      }
    }
    if (excludePK) {
      sb.append(',');
      if (separator != null) {
        sb.append(separator);
      }
      sb.append(CONSTRAINT).append(' ').append(constraintName).append(' ').append(PRIMARY_KEY)
          .append(" (").append(pkNames.get(0));
      for (int i = 1; i < pkNames.size(); i++) {
        sb.append(',').append(pkNames.get(i));
      }
      sb.append(')');
      if (separator != null) {
        sb.append(separator);
      }
    }
    if (separator != null) {
      sb.append(separator);
    }
    sb.append(");");
    return sb.toString();
  }

  /**
   * The {@code Builder} companion class for {@link TableDef} class.
   * 
   * @see TableDef
   */
  public static class Builder {

    private final String tableName;
    private final List<ColumnDef> columnDefinitions;
    private final List<String> pkNames;
    private String constraintName;

    /**
     * Instantiates a new builder.
     *
     * @param dbmsName the dbms name
     * @param tableName the table name
     */
    public Builder(@Nonnull final String tableName) {
      checkNotNull(tableName);
      this.tableName = tableName;
      columnDefinitions = new ArrayList<>();
      pkNames = new ArrayList<>();
      constraintName = null;
    }

    /**
     * Adds a column.
     *
     * @param columnDefinition the column definition
     * @return this {@code TableDefinitionBuilder}
     */
    public Builder addColumn(@Nonnull final ColumnDef columnDefinition) {
      if (columnDefinition.isPK()) {
        pkNames.add(columnDefinition.getName());
      }
      columnDefinitions.add(columnDefinition);
      return this;
    }

    public Builder pkConstraint(@Nonnull final String name) {
      constraintName = name;
      return this;
    }

    /**
     * Builds the {@code TableDefinition}.
     *
     * @return the table definition
     * @see TableDef
     */
    public TableDef build() {
      if (columnDefinitions.size() == 0) {
        throw new IllegalStateException(
            "Cannot build a proper TableDefinition without having defined any columns");
      }
      if (pkNames.size() == 0 || (pkNames.size() > 1 && constraintName == null)) {
        throw new IllegalStateException();
      }
      return new TableDef(this);
    }
  }
}
