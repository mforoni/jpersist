package com.gitlab.mforoni.jpersist;

public enum TableType {
  TABLE("TABLE"), //
  VIEW("VIEW"), //
  SYSTEM_TABLE("SYSTEM TABLE"), //
  GLOBAL_TEMPORARY("GLOBAL TEMPORARY"), //
  LOCAL_TEMPORARY("LOCAL TEMPORARY"), //
  ALIAS("ALIAS"), //
  SYNONYM("SYNONYM");
  private final String name;

  private TableType(final String name) {
    this.name = name;
  }

  String getName() {
    return name;
  }
}