package com.gitlab.mforoni.jpersist.query;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/**
 * Immutable class representing an <i>SQL delete statement</i>.
 * 
 * @author Foroni
 */
@Immutable
public final class Delete extends AbstractQuery {
  private Delete(final String statement, final ImmutableList<Object> parameters) {
    super(statement, parameters);
  }

  public static class Builder {
    private final StringBuilder statement;
    private Where where;
    private final List<Object> parameters;

    public Builder(@Nonnull final String tableName) {
      Preconditions.checkNotNull(tableName);
      statement = new StringBuilder(DELETE_FROM).append(' ').append(tableName);
      where = null;
      parameters = new ArrayList<>();
    }

    public Builder where(final Where where) {
      this.where = where;
      return this;
    }

    public Delete toDelete() {
      if (where != null) {
        statement.append(' ').append(where.getExpression());
        parameters.addAll(where.getParameters());
      }
      return new Delete(statement.toString(), ImmutableList.copyOf(parameters));
    }
  }
}
