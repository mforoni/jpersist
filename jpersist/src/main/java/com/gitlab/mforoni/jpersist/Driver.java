package com.gitlab.mforoni.jpersist;

import javax.annotation.Nonnull;
import com.github.mforoni.jbasic.JExceptions;
import com.google.common.annotations.Beta;

@Beta
enum Driver {
  ORACLE_14(3), ORACLE_6(4), ODBC_JDBC(1), UCANACCESS(4), HSQLDB(4);
  private final int type;

  private Driver(final int type) {
    this.type = type;
  }

  public int getType() {
    return type;
  }

  /**
   * Retrieves the driver used in the connection URL.
   *
   * @param url the connection URL
   * @return the driver
   */
  @Nonnull
  public static Driver parseUrl(@Nonnull final String url) {
    if (url.startsWith("jdbc:oracle:thin")) {
      // return Driver.ORACLE_14; // se usi ojdbc14.jar
      return Driver.ORACLE_6; // se usi ojdbc6.jar
    } else if (url.startsWith("jdbc:odbc:Driver={Microsoft Access Driver")) {
      return Driver.ODBC_JDBC;
    } else if (url.startsWith("jdbc:ucanaccess")) {
      return Driver.UCANACCESS;
    } else if (url.startsWith("jdbc:hsqldb")) {
      return Driver.HSQLDB;
    } else {
      throw JExceptions.newIllegalArgument("Cannot detect driver from url %s", url);
    }
  }
}
