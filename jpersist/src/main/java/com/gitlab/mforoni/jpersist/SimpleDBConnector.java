package com.gitlab.mforoni.jpersist;

import static com.gitlab.mforoni.jpersist.query.Query.SELECT;
import static com.google.common.base.Preconditions.checkNotNull;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.mforoni.jbasic.JExceptions;
import com.github.mforoni.jbasic.reflect.JFields;
import com.github.mforoni.jbasic.reflect.JMethods;
import com.github.mforoni.jbasic.reflect.JTypes;
import com.gitlab.mforoni.jpersist.DBRow.State;
import com.gitlab.mforoni.jpersist.query.Delete;
import com.gitlab.mforoni.jpersist.query.Join;
import com.gitlab.mforoni.jpersist.query.OrderBy;
import com.gitlab.mforoni.jpersist.query.Query;
import com.gitlab.mforoni.jpersist.query.Select;
import com.gitlab.mforoni.jpersist.query.Where;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;

/**
 * 
 * @author Marco Foroni
 * @see DBConnector
 * @see Connection
 */
final class SimpleDBConnector implements DBConnector {
  private static final Logger LOGGER = LoggerFactory.getLogger(SimpleDBConnector.class);
  @Nonnull
  private final Connection connection;
  @Nonnull
  private final DBMetaData dbMetaData;
  private final boolean readOnly;

  SimpleDBConnector(@Nonnull final Connection connection) throws SQLException {
    this(connection, false);
  }

  /**
   * Construct a DBConnector instance. If {@code readOnly} flag is <tt>true</tt> DML operations are
   * not allowed.
   *
   * @param connection the JDBC connection
   * @param readOnly a boolean flag indicating whether the database is in read-only mode
   * @throws SQLException
   */
  SimpleDBConnector(@Nonnull final Connection connection, final boolean readOnly)
      throws SQLException {
    Preconditions.checkNotNull(connection);
    this.connection = connection;
    this.dbMetaData = new DBMetaData(connection);
    this.readOnly = readOnly;
  }

  @Override
  public String toString() {
    return Joiner.on(", ").join(connection.toString(), dbMetaData.toString());
  }

  /**
   * Closes jdbc connection.
   *
   * @see Connection#close()
   */
  @Override
  public void close() throws SQLException {
    connection.close();
  }

  Connection getJdbcConnection() {
    return connection;
  }

  @Override
  public boolean isReadOnly() {
    return readOnly;
  }

  @Override
  public DBMetaData getDBMetaData() {
    return dbMetaData;
  }

  @Override
  public DriverMetaData getDriverMetaData() throws SQLException {
    return new DriverMetaData(dbMetaData.getDatabaseMetaData());
  }

  @Override
  public boolean getAutoCommit() throws SQLException {
    return connection.getAutoCommit();
  }

  @Override
  public boolean isConnectionClosed() throws SQLException {
    return connection.isClosed();
  }

  @Override
  public boolean isConnectionValid(final int timeout) throws SQLException {
    return connection.isValid(timeout);
  }

  @Override
  public boolean isConnectionValid() throws SQLException {
    return connection.isValid(30);
  }

  @Nullable
  @Override
  public Number executeSelectNumber(@Nonnull final Select query) throws SQLException {
    return executeSelectNumber(query.getStatement(), query.getParameters());
  }

  @Nullable
  @Override
  public Number executeSelectNumber(@Nonnull final String statement, final Object... parameters)
      throws SQLException {
    checkNotNull(statement);
    final Stopwatch sw = Stopwatch.createStarted();
    try (final PreparedStatement ps = connection.prepareStatement(statement)) {
      if (parameters != null && parameters.length > 0) {
        PreparedStatements.setParameters(ps, parameters);
      }
      Number n = null;
      try (final ResultSet rs = ps.executeQuery()) {
        if (rs.next()) {
          final Object o = rs.getObject(1);
          if (o instanceof Number) {
            n = (Number) o;
          } else {
            sw.stop();
            throw new IllegalStateException();
          }
        }
      }
      LOGGER.debug("Execute sql: {} - {} in {}", statement, Arrays.asList(parameters), sw.stop());
      return n;
    }
  }

  @Nonnull
  @Override
  public List<Object[]> executeSelect(@Nonnull final Select query) throws SQLException {
    final List<Object> parameters = query.getParameters();
    return executeSelect(query.getStatement(), parameters.toArray(new Object[parameters.size()]));
  }

  @Nonnull
  @Override
  public List<Object[]> executeSelect(@Nonnull final String statement, final Object... parameters)
      throws SQLException {
    checkNotNull(statement);
    final Stopwatch sw = Stopwatch.createStarted();
    final List<Object[]> rows = new ArrayList<>();
    try (final PreparedStatement ps = connection.prepareStatement(statement)) {
      if (parameters != null && parameters.length > 0) {
        PreparedStatements.setParameters(ps, parameters);
      }
      try (final ResultSet rs = ps.executeQuery()) {
        final ResultSetMetaData rsmd = rs.getMetaData();
        final int columns = rsmd.getColumnCount();
        while (rs.next()) {
          final Object[] row = new Object[columns];
          for (int i = 0; i < columns; i++) {
            row[i] = rs.getObject(i + 1);
          }
          rows.add(row);
        }
      }
      LOGGER.debug("Execute sql: {} - {} in {}", statement, Arrays.asList(parameters), sw.stop());
      return rows;
    }
  }

  @Override
  public int executeUpdate(@Nonnull final String statement, final Object... parameters)
      throws SQLException {
    if (readOnly) {
      throw new UnsupportedOperationException(
          "Operation not permitted: the connector was created in read-only mode");
    }
    checkNotNull(statement);
    if (statement.trim().startsWith(SELECT)) {
      throw new UnsupportedOperationException(
          "Cannot call execution of update with select statement");
    }
    final Stopwatch sw = Stopwatch.createStarted();
    try (final PreparedStatement ps = connection.prepareStatement(statement)) {
      // setting query parameters:
      if (parameters != null && parameters.length > 0) {
        PreparedStatements.setParameters(ps, parameters);
      }
      final int result = ps.executeUpdate();
      LOGGER.debug("Execute sql: {} - {} in {}", statement, Arrays.asList(parameters), sw.stop());
      return result;
    }
  }

  @Override
  public int executeUpdate(final Query query) throws SQLException {
    return executeUpdate(query.getStatement(), query.getParameters());
  }

  @Override
  public int[] executeBatch(final String sql, final List<Object>[] parameters) throws SQLException {
    // XXX add log
    connection.setAutoCommit(false);
    try (final PreparedStatement ps = connection.prepareStatement(sql)) {
      for (final List<Object> params : parameters) {
        int parameterIndex = 1;
        for (final Object param : params) {
          ps.setObject(parameterIndex++, param);
        }
        ps.addBatch();
      }
      final int[] result = ps.executeBatch();
      connection.commit();
      return result;
    }
  }

  @Override
  public boolean supportBatchUpdates() throws SQLException {
    return getDBMetaData().getDatabaseMetaData().supportsBatchUpdates();
  }

  @Override
  public List<DBRow> select(@Nonnull final DBTable dbTable, @Nullable final Join join,
      @Nullable final Where where, @Nullable final OrderBy orderBy) throws SQLException {
    final Stopwatch sw = Stopwatch.createStarted();
    final Select.Builder sb = new Select.Builder(dbTable.getAlias(),
        dbTable.getTableDescription().getQualifiedName(), null);
    if (join != null) {
      sb.join(join);
    }
    if (where != null) {
      sb.where(where);
    }
    if (orderBy != null) {
      sb.orderBy(orderBy);
    }
    final Select select = sb.toSelect();
    try (final PreparedStatement ps = connection.prepareStatement(select.getStatement())) {
      if (select.getParameters().size() > 0) {
        PreparedStatements.setParameters(ps, select.getParameters());
      }
      try (final ResultSet rs = ps.executeQuery()) {
        final List<DBRow> dbRows = new ArrayList<>();
        while (rs.next()) {
          dbRows.add(new DBRowMap(dbTable, rs, getDBMetaData()));
        }
        LOGGER.debug("Execute sql: {} - {} in {}", select.getStatement(), select.getParameters(),
            sw.stop());
        return dbRows;
      }
    }
  }

  @Override
  public int insert(final DBRow dbRow) throws SQLException {
    return insert(ImmutableList.of(dbRow));
  }

  @Override
  public int insert(final List<DBRow> dbRows) throws SQLException {
    final Stopwatch stopwatch = Stopwatch.createStarted();
    if (isReadOnly()) {
      throw new UnsupportedOperationException(
          "Operation not permitted: the connector was created in read-only mode");
    }
    Preconditions.checkNotNull(dbRows);
    if (dbRows.size() == 0) {
      throw new IllegalArgumentException("dbRows parameter cannot be empty");
    }
    checkPreconditions(dbRows, State.NEW);
    final DBTable dbTable = dbRows.get(0).getDBTable();
    final String sql = SQLStatements.insertInto(dbTable);
    try (final PreparedStatement ps = connection.prepareStatement(sql)) {
      int result = 0;
      for (final DBRow dbRow : dbRows) {
        // adding parameters:
        PreparedStatements.setInsertStatementParameters(ps, dbRow);
        // executes insert SQL stetement keeping count of already inserted:
        result += ps.executeUpdate();
        // after the insertion the state of DBRow is SPECULAR
        dbRow.setState(State.SPECULAR);
      }
      LOGGER.debug("{} row(s) have been inserted in table {} taking {}", result,
          dbTable.getTableDescription().getQualifiedName(), stopwatch.stop());
      return result;
    }
  }

  /**
   * Check if all the specified {@code dbRows} have:
   * <ul>
   * <li>the member {@code State} equal to the specified one and</li>
   * <li>the same {@code DBTable} member according to {@link DBTable#equals(Object)}
   * </ul>
   * If one of these conditions is not fulfilled an {@link IllegalArgumentException} is thrown.
   *
   * @param dbRows the {@code List} of {@code DBRow} elements
   * @param state the {@code State} to check
   * @throws IllegalArgumentException
   * @see DBRow
   * @see State
   */
  private static void checkPreconditions(@Nonnull final List<DBRow> dbRows,
      @Nonnull final State state) {
    // checking preconditions:
    if (dbRows.size() == 0) {
      throw new IllegalArgumentException("Nothing to check");
    }
    final DBTable dbTable = dbRows.get(0).getDBTable();
    for (final DBRow dbRow : dbRows) {
      if (dbRow.getState() != state) {
        throw new IllegalArgumentException(String.format("State not %s for all the dbRows", state));
      }
      if (!dbTable.equals(dbRow.getDBTable())) {
        throw new IllegalArgumentException("DBTable not the same for all the dbRows");
      }
    }
  }

  @Override
  public int update(final DBRow dbRow) throws SQLException {
    return update(ImmutableList.of(dbRow));
  }

  @Override
  public int update(final List<DBRow> dbRows) throws SQLException {
    if (isReadOnly()) {
      throw new UnsupportedOperationException(
          "Operation not permitted: the connector was created in read-only mode");
    }
    if (dbRows == null || dbRows.size() == 0) {
      throw new IllegalArgumentException("dbRows parameter cannot be null or empty");
    }
    final Stopwatch stopwatch = Stopwatch.createStarted();
    checkPreconditions(dbRows, State.UPDATED);
    final String statement = SQLStatements.update(dbRows.get(0));
    try (final PreparedStatement ps = connection.prepareStatement(statement)) {
      int result = 0;
      for (final DBRow dbRow : dbRows) {
        PreparedStatements.setUpdateStatementParameters(ps, dbRow);
        result += ps.executeUpdate();
        dbRow.setState(State.SPECULAR);
      }
      LOGGER.debug("{} row(s) of table {} have been updated in {}", result,
          dbRows.get(0).getDBTable().getTableDescription().getFullyQualifiedName(),
          stopwatch.stop());
      return result;
    }
  }

  @Override
  public int delete(final DBRow dbRow) throws SQLException {
    return delete(ImmutableList.of(dbRow));
  }

  @Override
  public int delete(final List<DBRow> dbRows) throws SQLException {
    if (isReadOnly()) {
      throw new UnsupportedOperationException(
          "Operation not permitted: the connector was created in read-only mode");
    }
    if (dbRows == null || dbRows.size() == 0) {
      throw new IllegalArgumentException("dbRows parameter cannot be null or empty");
    }
    final DBTable dbTable = dbRows.get(0).getDBTable();
    for (final DBRow dbRow : dbRows) {
      if (dbRow.getState() == State.NEW) {
        throw JExceptions.newIllegalState("Cannot delete never persisted object %s", dbRow);
      }
      if (!dbTable.equals(dbRow.getDBTable())) {
        throw new IllegalStateException();
      }
    }
    final Stopwatch stopwatch = Stopwatch.createStarted();
    final String tableName = dbTable.getTableDescription().getQualifiedName();
    final Delete deleteQuery =
        new Delete.Builder(tableName).where(DBRows.equalsPK(dbRows.get(0))).toDelete();
    try (final PreparedStatement ps = connection.prepareStatement(deleteQuery.getStatement())) {
      int result = 0;
      for (final DBRow dbRow : dbRows) {
        PreparedStatements.setParameters(ps, dbRow.getOriginalPK().values());
        result += ps.executeUpdate();
        dbRow.setState(State.DELETED);
      }
      LOGGER.debug("{} row(s) of table {} have been deleted in {}", result,
          dbRows.get(0).getDBTable().getTableDescription().getFullyQualifiedName(),
          stopwatch.stop());
      return result;
    }
  }

  @Nonnull
  @Override
  public <T> List<T> select(final Class<T> type) throws SQLException {
    checkTableAnnotation(type);
    final Collection<Field> columns = checkAndGetColumns(type);
    final Stopwatch sw = Stopwatch.createStarted();
    final List<T> list = new ArrayList<>();
    final String sql = SQLStatements.select(type);
    try (final PreparedStatement ps = connection.prepareStatement(sql);
        final ResultSet rs = ps.executeQuery()) {
      while (rs.next()) {
        final T instance = JTypes.newInstance(type);
        fill(type, instance, rs, columns);
        list.add(instance);
      }
    }
    LOGGER.debug("Execute sql: {} in {}", sql, sw.stop());
    return list;
  }

  private static void checkTableAnnotation(final Class<?> type) {
    Preconditions.checkArgument(type.getAnnotation(Table.class) != null,
        "The specified type has no Table annotation");
  }

  private static Collection<Field> checkAndGetIdFields(final Class<?> type) {
    final Collection<Field> pk =
        Collections2.filter(JFields.fromType(type), hasAnnotation(Id.class));
    Preconditions.checkArgument(pk.size() > 0, "The specified type has no field annotated with Id");
    return pk;
  }

  private static Collection<Field> checkAndGetColumns(final Class<?> type) {
    final Collection<Field> columns = getColumnFields(type);
    Preconditions.checkArgument(columns.size() > 0,
        "The specified type has no field annotated with Column");
    return columns;
  }

  private static Collection<Field> getColumnFields(final Class<?> type) {
    return Collections2.filter(JFields.fromType(type), hasAnnotation(Column.class));
  }

  public static <A extends Annotation> Predicate<Field> hasAnnotation(final Class<A> annotation) {
    return new Predicate<Field>() {
      @Override
      public boolean apply(@Nonnull final Field f) {
        return f.getAnnotation(annotation) != null;
      }
    };
  }

  private static <T> void fill(final Class<T> type, final T instance, final ResultSet rs,
      final Collection<Field> fields) throws SQLException {
    for (final Field field : fields) {
      final Object value = rs.getObject(Annotations.getColumnName(field));
      final Optional<Method> setter = JMethods.optionalSetter(type, field.getName());
      if (setter.isPresent()) {
        JMethods.invokeSetter(setter.get(), instance, value);
      } else {
        throw new IllegalStateException();
      }
    }
  }

  @Nullable
  @Override
  public <T> T selectByPK(@Nonnull final Class<T> type, @Nonnull final Object value,
      final Object... values) throws SQLException {
    final Collection<Field> pk =
        Collections2.filter(JFields.fromType(type), hasAnnotation(Id.class));
    return selectByUniqueConstraint(type, pk, value, values);
  }

  @Nullable
  @Override
  public <T> T selectByUniqueConstraint(@Nonnull final Class<T> type,
      @Nonnull final Collection<Field> fields, @Nonnull final Object value, final Object... values)
      throws SQLException {
    Preconditions.checkArgument(fields.size() == 1 + (values != null ? values.length : 0));
    checkTableAnnotation(type);
    final Collection<Field> columns = checkAndGetColumns(type);
    final Stopwatch sw = Stopwatch.createStarted();
    final String sql = SQLStatements.select(type, fields);
    T instance = null;
    try (final PreparedStatement ps = connection.prepareStatement(sql)) {
      ps.setObject(1, value);
      PreparedStatements.setParameters(ps, 2, values);
      try (final ResultSet rs = ps.executeQuery()) {
        if (rs.next()) {
          instance = JTypes.newInstance(type);
          fill(type, instance, rs, columns);
        }
        if (rs.next()) {
          throw new IllegalStateException();
        }
      }
    }
    LOGGER.debug("Excute sql: {} in {}", sql, sw.stop());
    return instance;
  }

  @Override
  public <T> void insert(@Nonnull final T instance, @Nonnull final Class<T> type)
      throws SQLException {
    checkTableAnnotation(type);
    final Collection<Field> columns = checkAndGetColumns(type);
    final Collection<Field> idFields = checkAndGetIdFields(type);
    final Stopwatch sw = Stopwatch.createStarted();
    final String insert = SQLStatements.insertInto(type, columns);
    final int result;
    try (final PreparedStatement ps =
        connection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS)) {
      int index = 1;
      for (final Field f : columns) {
        if (!hasAnnotation(GeneratedValue.class).apply(f)) {
          final Method getter = JMethods.optionalGetter(type, f.getName()).get();
          ps.setObject(index++, JMethods.invokeGetter(getter, instance));
        }
      }
      result = ps.executeUpdate();
      assert result == 1;
      try (final ResultSet rs = ps.getGeneratedKeys()) {
        final int pkColumns = rs.getMetaData().getColumnCount();
        assert pkColumns == idFields.size();
        final Object[] genKeys = new Object[pkColumns];
        final List<Field> pkFields = new ArrayList<>(idFields);
        if (rs.next()) {
          for (int i = 0; i < pkColumns; i++) {
            final Field field = pkFields.get(i);
            final String idName = Annotations.getColumnName(field);
            genKeys[i] = rs.getObject(idName);
            JMethods.invokeSetter(type, field.getName(), instance, genKeys[i]);
          }
        }
      }
    }
    LOGGER.debug("Excute sql: {} in {}", insert, sw.stop());
  }

  @Override
  public <T> void update(@Nonnull final T instance, @Nonnull final Class<T> type)
      throws SQLException {
    checkTableAnnotation(type);
    final Collection<Field> columns = checkAndGetColumns(type);
    final Collection<Field> idFields = checkAndGetIdFields(type);
    final Stopwatch sw = Stopwatch.createStarted();
    final int result;
    final String sql = SQLStatements.update(type, columns, idFields);
    try (final PreparedStatement ps = connection.prepareStatement(sql)) {
      int index = 1;
      for (final Field f : columns) {
        if (f.getAnnotation(Id.class) == null) {
          final Method getter = JMethods.optionalGetter(type, f.getName()).get();
          ps.setObject(index++, JMethods.invokeGetter(getter, instance));
        }
      }
      for (final Field f : idFields) {
        final Method getter = JMethods.optionalGetter(type, f.getName()).get();
        ps.setObject(index++, JMethods.invokeGetter(getter, instance));
      }
      result = ps.executeUpdate();
      assert (result == 1);
    }
    LOGGER.debug("Excute sql: {} in {}", sql, sw.stop());
  }

  @Override
  public <T> void delete(@Nonnull final T instance, @Nonnull final Class<T> type)
      throws SQLException {
    checkTableAnnotation(type);
    checkAndGetColumns(type);
    final Collection<Field> idFields = checkAndGetIdFields(type);
    final Stopwatch sw = Stopwatch.createStarted();
    final int result;
    final String sql = SQLStatements.delete(type, idFields);
    try (final PreparedStatement ps = connection.prepareStatement(sql)) {
      int index = 1;
      for (final Field f : idFields) {
        final Method getter = JMethods.optionalGetter(type, f.getName()).get();
        ps.setObject(index++, JMethods.invokeGetter(getter, instance));
      }
      result = ps.executeUpdate();
      assert (result == 1);
    }
    LOGGER.debug("Excute sql: {} in {}", sql, sw.stop());
  }
}
