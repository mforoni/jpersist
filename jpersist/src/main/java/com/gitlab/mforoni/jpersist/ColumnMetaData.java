package com.gitlab.mforoni.jpersist;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.sql.DatabaseMetaData.columnNoNulls;
import static java.sql.DatabaseMetaData.columnNullable;
import static java.sql.DatabaseMetaData.columnNullableUnknown;
import java.sql.DatabaseMetaData;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

/**
 * Immutable class representing a database field meta-data. In the context of a relational database,
 * a row—also called a record or tuple—represents a single, implicitly structured data item in a
 * table. In simple terms, a database table can be thought of as consisting of rows and columns or
 * fields. Each row in a table represents a set of related data, and every row in the table has the
 * same structure.
 * <p>
 * This {@code ColumnMetaData} contains:
 * <ul>
 * <li>the column index</li>
 * <li>the column name</li>
 * <li>the sql type</li>
 * <li>the column type name</li>
 * <li>the precision of the column</li>
 * <li>the scale of the column</li>
 * <li>the nullability of the column</li>
 * <li>the table name</li>
 * </ul>
 * 
 * @author Marco Foroni
 */
@Immutable
public final class ColumnMetaData {
  private final int columnIndex;
  private final String columnName; // caption
  private final int sqlTypeCode;
  private final String columnTypeName;
  private final int precision;
  private final int scale; // number of digits after the point
  private final int nullable;
  private final String tableName;

  /**
   * Instantiates a new {@code ColumnMetaData}.
   *
   * @param columnIndex the column index
   * @param columnName the column name
   * @param sqlType the sql type
   * @param columnTypeName the column type name
   * @param precision the precision of the column
   * @param scale the scale of the column
   * @param nullable the nullability of the column
   * @param tableName the table name
   * @see java.sql.Types
   */
  ColumnMetaData(final int columnIndex, @Nonnull final String columnName, final int sqlType,
      @Nonnull final String columnTypeName, final int precision, final int scale,
      final int nullable, @Nonnull final String tableName) {
    checkNotNull(columnName);
    checkNotNull(columnTypeName);
    checkNotNull(tableName);
    checkArgument(nullable == columnNoNulls || nullable == columnNullable
        || nullable == columnNullableUnknown);
    this.columnIndex = columnIndex;
    this.columnName = columnName;
    this.sqlTypeCode = sqlType;
    this.columnTypeName = columnTypeName;
    this.precision = precision;
    this.scale = scale;
    this.nullable = nullable;
    this.tableName = tableName;
  }

  /**
   * Gets the column index.
   *
   * @return the column index
   */
  public int getColumnIndex() {
    return columnIndex;
  }

  /**
   * Gets the column name.
   *
   * @return the column name
   */
  public String getColumnName() {
    return columnName;
  }

  /**
   * Gets the SQL type code.
   *
   * @return the SQL type
   * @see java.sql.Types
   */
  public int getSQLTypeCode() {
    return sqlTypeCode;
  }

  public SQLType getSQLType() {
    return SQLType.parse(sqlTypeCode);
  }

  /**
   * Gets the column type name.
   *
   * @return the column type name
   */
  public String getColumnTypeName() {
    return columnTypeName;
  }

  /**
   * Gets the precision.
   *
   * @return the precision
   */
  public int getPrecision() {
    return precision;
  }

  /**
   * Gets the scale.
   *
   * @return the scale
   */
  public int getScale() {
    return scale;
  }

  /**
   * Gets the nullability. All possible values are:
   * <ul>
   * <li>{@link DatabaseMetaData#columnNoNulls}</li>
   * <li>{@link DatabaseMetaData#columnNullable}</li>
   * <li>{@link DatabaseMetaData#columnNullableUnknown}</li>
   * </ul>
   * 
   * @return the nullability
   */
  public int getNullable() {
    return nullable;
  }

  /**
   * Returns <tt>true</tt> only if {@code nullable} contains
   * {@link DatabaseMetaData#columnNullable}.
   *
   * @return <tt>true</tt> only if {@code nullable} contains
   *         {@link DatabaseMetaData#columnNullable}.
   */
  public boolean isNullable() {
    return nullable == DatabaseMetaData.columnNullable;
  }

  /**
   * Gets the table name.
   *
   * @return the table name
   */
  public String getTableName() {
    return tableName;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof ColumnMetaData)) {
      return false;
    }
    final ColumnMetaData columnMetaData = (ColumnMetaData) obj;
    if (columnIndex != columnMetaData.columnIndex) {
      return false;
    }
    if (!columnName.equals(columnMetaData.columnName)) {
      return false;
    }
    if (sqlTypeCode != columnMetaData.sqlTypeCode) {
      return false;
    }
    if (!columnTypeName.equals(columnMetaData.columnTypeName)) {
      return false;
    }
    if (precision != columnMetaData.precision) {
      return false;
    }
    if (scale != columnMetaData.scale) {
      return false;
    }
    if (nullable != columnMetaData.nullable) {
      return false;
    }
    if (!tableName.equals(columnMetaData.tableName)) {
      return false;
    }
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + columnIndex;
    result = 31 * result + columnName.hashCode();
    result = 31 * result + sqlTypeCode;
    result = 31 * result + columnTypeName.hashCode();
    result = 31 * result + precision;
    result = 31 * result + scale;
    result = 31 * result + nullable;
    result = 31 * result + tableName.hashCode();
    return result;
  }

  /**
   * Returns a brief description of this {@code ColumnMetaData} in the form of
   * {@code tableName.columnName}.
   * 
   * @return a brief description of this {@code ColumnMetaData}
   */
  @Override
  public String toString() {
    return tableName + "." + columnName;
  }

  public String fullDescription() {
    return new StringBuilder("ColumnMetaData: table=").append(tableName).append(", index=")
        .append(columnIndex).append(", column name=").append(columnName).append(", SQL type=")
        .append(sqlTypeCode).append(", type name=").append(columnTypeName).append(", precision=")
        .append(precision).append(", scale=").append(scale).append(", nullable=").append(nullable)
        .toString();
  }
}
