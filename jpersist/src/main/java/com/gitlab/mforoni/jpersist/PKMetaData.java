package com.gitlab.mforoni.jpersist;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;

/**
 * The immutable class representing a database primary key meta information. It contains:
 * <ul>
 * <li>the catalog (may be <tt>null</tt>)</li>
 * <li>the schema (may be <tt>null</tt>)</li>
 * <li>the table name (cannot be <tt>null</tt>)</li>
 * <li>the list of {@code PKColumn} i.e. the columns that make up this primary key (cannot be
 * <tt>null</tt>)</li>
 * <li>the name of this constraint (may be <tt>null</tt>)</li>
 * </ul>
 *
 * @author Marco Foroni
 * @see PKColumn
 */
@Immutable
public final class PKMetaData {
  private static final Logger LOGGER = LoggerFactory.getLogger(PKMetaData.class);
  @Nullable
  private final String catalog;
  @Nullable
  private final String schema;
  @Nonnull
  private final String tableName;
  /**
   * The columns that make up this Primary Key.
   */
  private final ImmutableList<PKColumn> columns;
  /**
   * Name of primary key (constraint name), may be null.
   */
  @Nullable
  private final String name;

  /**
   * Instantiates a new {@code PKMetaData}.
   *
   * @param builder the {@code Builder}
   * @see Builder
   */
  private PKMetaData(@Nonnull final Builder builder) {
    this.catalog = builder.catalog;
    this.schema = builder.schema;
    this.tableName = builder.tableName;
    this.columns = ImmutableList.copyOf(builder.columns);
    this.name = builder.name;
  }

  /**
   * Gets the catalog.
   *
   * @return the catalog
   */
  @Nullable
  public String getCatalog() {
    return catalog;
  }

  /**
   * Gets the schema.
   *
   * @return the schema
   */
  @Nullable
  public String getSchema() {
    return schema;
  }

  /**
   * Gets the table name.
   *
   * @return the table name
   */
  @Nonnull
  public String getTableName() {
    return tableName;
  }

  /**
   * Gets the primary key name (constraint name).
   *
   * @return the primary key name
   */
  @Nullable
  public String getName() {
    return name;
  }

  /**
   * Gets the size, i.e. the number of columns that made up this primary key.
   *
   * @return the size, i.e. the number of columns of this primary key.
   * @see PKColumn
   */
  public int getSize() {
    return columns.size();
  }

  /**
   * Gets the primary key columns.
   *
   * @return the primary key columns
   */
  @Nonnull
  public ImmutableList<PKColumn> getColumns() {
    return columns;
  }

  /**
   * Returns the {@code PKColumn} having the specified key sequence. Returns <tt>null</tt> if no
   * column with the specified key sequence is found.
   * <p>
   * Requires O({@link PKMetaData#getSize()}) in the worst case.
   * 
   * @param keySequence the key sequence of the primary key column to retrieve
   * @return the {@code PKColumn} if exists a column with the specified key sequence, otherwise
   *         returns <tt>null</tt>.
   * @see PKColumn
   */
  @Nullable
  PKColumn getPKColumn(final short keySequence) {
    for (final PKColumn pkColumn : columns) {
      if (pkColumn.getKeySequence() == keySequence) {
        return pkColumn;
      }
    }
    return null;
  }

  /**
   * Returns the {@code PKColumn} having the specified name. Returns <tt>null</tt> if no column with
   * the specified name is found.
   * <p>
   * Requires O({@link PKMetaData#getSize()}) in the worst case.
   * 
   * @param name the name of the primary key column to retrieve
   * @return the {@code PKColumn} if exists a column with the specified name, otherwise returns
   *         <tt>null</tt>.
   * @see PKColumn
   */
  @Nullable
  PKColumn getPKColumn(final String name) {
    for (final PKColumn pkColumn : columns) {
      if (pkColumn.getName().equals(name)) {
        return pkColumn;
      }
    }
    return null;
  }

  /**
   * Checks if the specified column name is part of this primary key.
   * <p>
   * Requires O({@link PKMetaData#getSize()}) in the worst case.
   *
   * @param columnName the column name to check
   * @return <tt>true</tt> if the column name is part of this primary key, <tt>false</tt> otherwise
   */
  public boolean containsColumn(@Nonnull final String columnName) {
    return getKeySequence(columnName) != (short) -1 ? true : false;
  }

  public short getKeySequence(@Nonnull final String columnName) {
    for (final PKColumn pkColumn : columns) {
      if (pkColumn.getName().equals(columnName)) {
        return pkColumn.getKeySequence();
      }
    }
    return (short) -1;
  }

  /**
   * Checks if the specified column name is part of this primary key without considering the case.
   * <p>
   * Requires O({@link PKMetaData#getSize()}) in the worst case.
   * 
   * @param columnName the column name to check
   * @return <tt>true</tt> if the column name is part of this primary key, <tt>false</tt> otherwise
   */
  public boolean containsColumnIgnoreCase(@Nonnull final String columnName) {
    return getKeySequenceIgnoreCase(columnName) != (short) -1 ? true : false;
  }

  public short getKeySequenceIgnoreCase(@Nonnull final String columnName) {
    for (final PKColumn pkColumn : columns) {
      if (pkColumn.getName().equalsIgnoreCase(columnName)) {
        return pkColumn.getKeySequence();
      }
    }
    return (short) -1;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof PKMetaData)) {
      return false;
    }
    final PKMetaData pkmd = (PKMetaData) obj;
    if (!Objects.equals(catalog, pkmd.catalog)) {
      return false;
    }
    if (!Objects.equals(schema, pkmd.schema)) {
      return false;
    }
    if (!tableName.equals(pkmd.tableName)) {
      return false;
    }
    if (!columns.equals(pkmd.columns)) {
      return false;
    }
    if (!Objects.equals(name, pkmd.name)) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (catalog == null ? 0 : catalog.hashCode());
    result = 31 * result + (schema == null ? 0 : schema.hashCode());
    result = 31 * result + tableName.hashCode();
    result = 31 * result + columns.hashCode();
    result = 31 * result + (name == null ? 0 : name.hashCode());
    return result;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("PKMetaData: table=").append(tableName)
        .append(", columns=").append(columns);
    if (name != null) {
      sb.append(", constraint_name=").append(name);
    }
    return sb.toString();
  }

  /**
   * The companion builder class of the {@code PKMetaData}.
   *
   * @author Foroni
   * @see PKMetaData
   */
  public static class Builder {
    private final String catalog; // // may be null
    private final String schema; // may be null
    private final String tableName;
    private final List<PKColumn> columns;
    private String name; // may be null
    private short counter;

    /**
     * Instantiates a new {@code Builder} for the class {@code PKMetaData}.
     * 
     * @param catalog the catalog
     * @param schema the schema
     * @param tableName the name of the database table
     * @see PKMetaData
     * @see Builder
     */
    public Builder(@Nullable final String catalog, @Nullable final String schema,
        @Nonnull final String tableName) {
      checkNotNull(tableName);
      this.catalog = catalog;
      this.schema = schema;
      this.tableName = tableName;
      columns = new ArrayList<>();
      counter = 1;
      name = null;
    }

    /**
     * Instantiates a new primary key builder.
     *
     * @param schema the schema
     * @param tableName the name of the database table
     */
    public Builder(@Nullable final String schema, @Nonnull final String tableName) {
      this(null, schema, tableName);
    }

    /**
     * Instantiates a new primary key builder.
     *
     * @param tableName the name of the database tables
     */
    public Builder(@Nonnull final String tableName) {
      this(null, null, tableName);
    }

    /**
     * Adds the column having name {@code columnName} to this primary key.
     *
     * @param columnName name of primary key column
     * @return this {@code Builder}
     */
    public Builder addColumn(@Nonnull final String columnName) {
      return addColumn(columnName, counter++);
    }

    /**
     * Adds the column having name {@code columnName} to this primary key.
     *
     * @param columnName name of primary key column
     * @param keySequence the key sequence index
     * @return this {@code Builder}
     */
    public Builder addColumn(@Nonnull final String columnName, final short keySequence) {
      columns.add(new PKColumn(columnName, keySequence));
      return this;
    }

    /**
     * Sets the primary key constraint name.
     *
     * @param constraintName name of the primary key
     * @return this {@code Builder}
     */
    public Builder constraintName(final String constraintName) {
      if (this.name == null) {
        this.name = constraintName;
      } else if (!name.equals(constraintName)) {
        LOGGER.warn("Inconsistent constraint name: stored={}, retrieved={}", this.name,
            constraintName);
      }
      return this;
    }

    /**
     * Builds the {@link PKMetaData}.
     *
     * @return a {@code PKMetaData}
     */
    public PKMetaData build() {
      return new PKMetaData(this);
    }
  }
  /**
   * The Class {@code PKColumn}.
   */
  public static class PKColumn {
    private final String name;
    private final short keySequence;

    /**
     * Instantiates a new primary key column.
     *
     * @param name the name of the column
     * @param keySequence the key sequence
     */
    private PKColumn(@Nonnull final String name, final short keySequence) {
      this.name = name;
      this.keySequence = keySequence;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    @Nonnull
    public String getName() {
      return name;
    }

    /**
     * Gets the key sequence.
     *
     * @return the key sequence
     */
    public short getKeySequence() {
      return keySequence;
    }

    @Override
    public int hashCode() {
      int result = 17;
      result = 31 * result + keySequence;
      result = 31 * result + (name == null ? 0 : name.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final PKColumn other = (PKColumn) obj;
      if (keySequence != other.keySequence) {
        return false;
      }
      if (!Objects.equals(name, other.name)) {
        return false;
      }
      return true;
    }

    @Override
    public String toString() {
      return new StringBuilder(name).append(" [").append(keySequence).append("]").toString();
    }
  }
}
