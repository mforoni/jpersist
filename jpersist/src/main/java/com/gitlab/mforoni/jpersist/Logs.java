package com.gitlab.mforoni.jpersist;

import java.sql.SQLException;
import java.util.List;
import javax.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.mforoni.jbasic.util.JLogger;
import com.github.mforoni.jbasic.util.JLogger.Level;

public final class Logs {
  private static final Logger LOGGER = LoggerFactory.getLogger(Logs.class);

  private Logs() {
    throw new AssertionError();
  }

  public static void log(@Nonnull final DBMetaData dbMetaData) throws SQLException {
    log(dbMetaData, null);
  }

  /**
   * Logs at <i>info</i> level all metadata (catalogs, schema, tables) for this database on the
   * provided {@code Logger}.
   *
   * @param logger
   * @param tableNamePattern
   * @throws SQLException
   * @see org.slf4j.Logger
   * @see Schema
   * @see TableDesc
   */
  public static void log(@Nonnull final DBMetaData dbMetaData, final String tableNamePattern)
      throws SQLException {
    LOGGER.info("Logging DB MetaData for database {}", dbMetaData);
    final List<String> catalogs = dbMetaData.getCatalogs();
    if (catalogs.size() == 0) {
      LOGGER.info("No catalogs found in this database");
    }
    for (final String catalog : catalogs) {
      LOGGER.info("Catalog name = {}", catalog);
    }
    final List<Schema> schemas = dbMetaData.getSchemas();
    if (schemas.size() == 0) {
      LOGGER.info("No schemas found in this database");
    }
    for (final Schema schema : schemas) {
      LOGGER.info("{}", schema);
    }
    final List<TableDesc> tables = dbMetaData.getTables(tableNamePattern);
    if (tables.size() == 0) {
      LOGGER.info("No tables found in this database having name matching the pattern {}",
          tableNamePattern);
    }
    for (final TableDesc tableDesc : tables) {
      LOGGER.info("{}", tableDesc);
      if (tableDesc.getTableType() == TableType.TABLE) {
        final List<ColumnMetaData> columnMetaDatas = dbMetaData.getColumns(tableDesc);
        LOGGER.info("{}", columnMetaDatas);
      }
    }
  }

  public static void verbose(final List<DBRow> dbRows) {
    verbose(dbRows, Level.INFO);
  }

  private static void verbose(final List<DBRow> dbRows, final JLogger.Level level) {
    for (final DBRow dbRow : dbRows) {
      JLogger.log(level, LOGGER, dbRow.getDescription());
    }
  }
}
