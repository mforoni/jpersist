#!/bin/sh
# check if first argument is an empty string
if [ -z "$1" ] 
then 
	echo "No argument supplied: please provide path of ojdbc6.jar"
else
	echo "Installing ojdbc6.jar on local maven repository"
	mvn install:install-file -Dfile="$1" -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.3 -Dpackaging=jar
fi

