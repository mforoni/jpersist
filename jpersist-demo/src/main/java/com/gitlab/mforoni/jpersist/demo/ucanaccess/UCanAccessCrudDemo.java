package com.gitlab.mforoni.jpersist.demo.ucanaccess;

import com.github.mforoni.jbasic.io.JFiles;
import com.gitlab.mforoni.jpersist.DBConnector;
import com.gitlab.mforoni.jpersist.DBConnectors;
import com.gitlab.mforoni.jpersist.demo.CrudDemo;

/**
 * Tests JPersist API for CRUD operations with UCanAccess driver. The test consist of : create,
 * insert, update and delete the four basic functions of persistent storage.
 * 
 * @author Marco Foroni
 */
final class UCanAccessCrudDemo {
  private UCanAccessCrudDemo() {} // Preventing instantiation

  public static void main(final String[] args) {
    try (final DBConnector dbConnector = DBConnectors.newDBConnector(JFiles.fromResource(""))) {
      CrudDemo.execute(dbConnector);
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }
}
