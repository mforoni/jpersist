package com.gitlab.mforoni.jpersist.demo.hsql;

import static com.gitlab.mforoni.jpersist.demo.hsql.HsqlTableDefDemo.HSQLDB_JPERSIST_DEMODB_PROPERTIES;
import com.github.mforoni.jbasic.io.JFiles;
import com.gitlab.mforoni.jpersist.DBConnector;
import com.gitlab.mforoni.jpersist.DBConnectors;
import com.gitlab.mforoni.jpersist.demo.CrudDemo;

final class HsqlCrudDemo {
  private HsqlCrudDemo() {}

  public static void main(final String[] args) {
    try (final DBConnector dbConnector =
        DBConnectors.newDBConnector(JFiles.fromResource(HSQLDB_JPERSIST_DEMODB_PROPERTIES))) {
      CrudDemo.execute(dbConnector);
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }
}
