package com.gitlab.mforoni.jpersist.demo.oracle;

import com.github.mforoni.jbasic.io.JFiles;
import com.gitlab.mforoni.jpersist.DBConnector;
import com.gitlab.mforoni.jpersist.DBConnectors;
import com.gitlab.mforoni.jpersist.Logs;

/**
 * Tests JPersist API for retrieving database meta-data on Oracle DBMS.
 * <p>
 *
 * @author Marco Foroni
 */
final class OracleDBMetaDataDemo {
  private OracleDBMetaDataDemo() {} // Preventing instantiation

  public static void main(final String[] args) throws Exception {
    try (final DBConnector dbConnector = DBConnectors.newDBConnector(JFiles.fromResource(""))) {
      Logs.log(dbConnector.getDBMetaData());
    }
  }
}
