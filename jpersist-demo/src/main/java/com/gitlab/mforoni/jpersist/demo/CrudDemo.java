package com.gitlab.mforoni.jpersist.demo;

import java.sql.SQLException;
import java.util.List;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gitlab.mforoni.jpersist.DBConnector;
import com.gitlab.mforoni.jpersist.DBRow;
import com.gitlab.mforoni.jpersist.DBRow.State;
import com.gitlab.mforoni.jpersist.DBTable;
import com.gitlab.mforoni.jpersist.Logs;

public final class CrudDemo {
  private static final Logger LOGGER = LoggerFactory.getLogger(CrudDemo.class);

  CrudDemo() {}

  private static void createTable(final DBConnector dbConnector) throws SQLException {
    final DBPersonaDef dbPersonaDef = new DBPersonaDef();
    dbPersonaDef.execute(dbConnector);
  }

  public static void execute(final DBConnector dbConnector) throws SQLException {
    createTable(dbConnector);
    final DBTable dbtPerson = new DBTable(dbConnector, Person.PERSON);
    LOGGER.info("{}", dbtPerson);
    // insert
    LOGGER.info("Testing INSERT..");
    final DBRow dbrRossi = dbtPerson.newDBRow();
    dbrRossi.setObject(Person.ID, 1);
    dbrRossi.setObject(Person.FIRST_NAME, "Mario");
    dbrRossi.setObject(Person.LAST_NAME, "Rossi");
    dbrRossi.setObject(Person.BIRTH_DATE, new LocalDate(1980, 2, 11));
    dbrRossi.setObject(Person.EMAIL, "mario.rossi@email.it");
    LOGGER.info("{}", dbrRossi);
    dbConnector.insert(dbrRossi);
    LOGGER.info("{} has been inserted in {}", dbrRossi, dbtPerson);
    final DBRow dbrNeri = dbtPerson.newDBRow();
    dbrNeri.setObject(Person.ID, 2);
    dbrNeri.setObject(Person.FIRST_NAME, "Francesco");
    dbrNeri.setObject(Person.LAST_NAME, "Neri");
    dbrNeri.setObject(Person.BIRTH_DATE, new LocalDate(1975, 12, 31));
    dbrNeri.setObject(Person.EMAIL, "francesco.neri@email.it");
    LOGGER.info("{}", dbrNeri);
    dbConnector.insert(dbrNeri);
    LOGGER.info("{} has been inserted in {}", dbrNeri, dbtPerson);
    List<DBRow> persons = dbtPerson.select();
    LOGGER.info("Select all from persons: {}", persons);
    Logs.verbose(persons);
    assert (dbrNeri.getState() == State.SPECULAR);
    // update
    LOGGER.info("Testing UPDATE...");
    dbrNeri.setObject(Person.EMAIL, null);
    dbrNeri.setObject(Person.BIRTH_DATE, new LocalDate(1996, 7, 21));
    LOGGER.info("{}", dbrNeri.getDescription());
    dbConnector.update(dbrNeri);
    LOGGER.info("{} has been updated", dbrNeri);
    persons = dbtPerson.select();
    LOGGER.info("Select all from persone: {}", persons);
    Logs.verbose(persons);
    // delete
    LOGGER.info("Testing DELETE...");
    dbConnector.delete(dbrRossi);
    LOGGER.info("{} has been deleted", dbrRossi);
    persons = dbtPerson.select();
    LOGGER.info("Select all from persone: {}", persons);
    Logs.verbose(persons);
  }
}
