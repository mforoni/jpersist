package com.gitlab.mforoni.jpersist.demo.ucanaccess;

import com.github.mforoni.jbasic.io.JFiles;
import com.gitlab.mforoni.jpersist.DBConnector;
import com.gitlab.mforoni.jpersist.DBConnectors;
import com.gitlab.mforoni.jpersist.Logs;

/**
 * Tests simple meta-data information retrieving operations with UCanAccess driver.
 *
 * @author Marco Foroni
 */
final class UCanAccessDBMetaDataDemo {
  private UCanAccessDBMetaDataDemo() {}

  public static void main(final String[] args) {
    try (final DBConnector dbConnector = DBConnectors.newDBConnector(JFiles.fromResource(""))) {
      Logs.log(dbConnector.getDBMetaData());
    } catch (final Throwable t) {
      t.printStackTrace();
    }
  }
}
