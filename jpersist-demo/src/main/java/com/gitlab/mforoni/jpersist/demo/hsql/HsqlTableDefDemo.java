package com.gitlab.mforoni.jpersist.demo.hsql;

import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.mforoni.jbasic.io.JFiles;
import com.gitlab.mforoni.jpersist.DBConnector;
import com.gitlab.mforoni.jpersist.DBConnectors;
import com.gitlab.mforoni.jpersist.demo.DBPersonaDef;

final class HsqlTableDefDemo {
  private static final Logger LOGGER = LoggerFactory.getLogger(HsqlTableDefDemo.class);
  static final String HSQLDB_JPERSIST_DEMODB_PROPERTIES = "hsqldb-jpersist-demodb.properties";

  private static void start(final DBConnector dbConnector) throws SQLException {
    final DBPersonaDef dbPersonaDef = new DBPersonaDef();
    dbPersonaDef.print();
    dbPersonaDef.execute(dbConnector);
  }

  public static void main(final String[] args) {
    try (final DBConnector dbConnector =
        DBConnectors.newDBConnector(JFiles.fromResource(HSQLDB_JPERSIST_DEMODB_PROPERTIES))) {
      start(dbConnector);
    } catch (final Exception e) {
      LOGGER.error("Error: ", e);
    }
  }
}
