package com.gitlab.mforoni.jpersist.demo;

final class Person {

  private Person() {

  }

  public static final String PERSON = "PERSON";

  public static final String ID = "id";
  public static final String FIRST_NAME = "first_name";
  public static final String LAST_NAME = "last_name";
  public static final String BIRTH_DATE = "birth_date";
  public static final String EMAIL = "email";
}
