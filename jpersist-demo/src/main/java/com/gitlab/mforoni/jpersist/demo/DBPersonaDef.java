package com.gitlab.mforoni.jpersist.demo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.mforoni.jbasic.JStrings;
import com.gitlab.mforoni.jpersist.ColumnDef;
import com.gitlab.mforoni.jpersist.DBConnector;
import com.gitlab.mforoni.jpersist.DBUtil;
import com.gitlab.mforoni.jpersist.SQLType;
import com.gitlab.mforoni.jpersist.TableDef;

/**
 * Useful for testing table creation.
 * 
 * @author Foroni Marco
 * @see TableDef
 * @see ColumnDef
 */
public final class DBPersonaDef {
  private static final Logger LOGGER = LoggerFactory.getLogger(DBPersonaDef.class);
  private final List<TableDef> tableDefs;

  public DBPersonaDef() {
    tableDefs = getDefinitions();
  }

  private static final List<TableDef> getDefinitions() {
    final List<TableDef> definitions = new ArrayList<>();
    final TableDef teamsTableDef = new TableDef.Builder(Person.PERSON)
        .addColumn(new ColumnDef.Builder(Person.ID, SQLType.INTEGER).pk().build())
        .addColumn(new ColumnDef.Builder(Person.FIRST_NAME, SQLType.VARCHAR).precision(255)
            .notNullable().build())
        .addColumn(new ColumnDef.Builder(Person.LAST_NAME, SQLType.VARCHAR).precision(255)
            .notNullable().build())
        .addColumn(new ColumnDef.Builder(Person.BIRTH_DATE, SQLType.DATE).build())
        .addColumn(new ColumnDef.Builder(Person.EMAIL, SQLType.VARCHAR).precision(255).build())
        .build();
    definitions.add(teamsTableDef);
    return definitions;
  }

  public void print() {
    for (final TableDef tableDef : tableDefs) {
      LOGGER.info("{}:{}{}", tableDef, JStrings.NEWLINE, tableDef.toSQL());
    }
  }

  public void execute(final DBConnector dbConnector) throws SQLException {
    LOGGER.info("Driver: {}", dbConnector.getDriverMetaData());
    for (final TableDef tableDef : tableDefs) {
      final String tableName = tableDef.getTableName();
      if (DBUtil.existTable(dbConnector, tableName)) {
        DBUtil.dropTable(dbConnector, tableName);
      }
      DBUtil.createTable(dbConnector, tableDef);
      LOGGER.info("Table {} successfully created.", tableName);
    }
  }
}
