package com.gitlab.mforoni.jpersist.demo.hsql;

import static com.gitlab.mforoni.jpersist.demo.hsql.HsqlTableDefDemo.HSQLDB_JPERSIST_DEMODB_PROPERTIES;
import com.github.mforoni.jbasic.io.JFiles;
import com.gitlab.mforoni.jpersist.DBConnector;
import com.gitlab.mforoni.jpersist.DBConnectors;
import com.gitlab.mforoni.jpersist.Logs;

public class HsqlDBMetaDataDemo {
  private HsqlDBMetaDataDemo() {}

  public static void main(final String[] args) {
    try (final DBConnector dbConnector =
        DBConnectors.newDBConnector(JFiles.fromResource(HSQLDB_JPERSIST_DEMODB_PROPERTIES))) {
      Logs.log(dbConnector.getDBMetaData());
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }
}
