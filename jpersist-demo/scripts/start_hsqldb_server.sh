#!/bin/bash

echo whoami
echo $(whoami)

function os_name()
{
	if [[ "$OSTYPE" == "linux-gnu" ]]; then
			# ...
			echo "linux"
	elif [[ "$OSTYPE" == "darwin"* ]]; then
			# Mac OSX
			echo "osx"
	elif [[ "$OSTYPE" == "cygwin" ]]; then
			# POSIX compatibility layer and Linux environment emulation for Windows
			echo "windows"
	elif [[ "$OSTYPE" == "msys" ]]; then
			# Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
			echo "windows"
	elif [[ "$OSTYPE" == "win32" ]]; then
			# I'm not sure this can happen.
			echo "windows"
	elif [[ "$OSTYPE" == "freebsd"* ]]; then
			# ...
			echo "linux"
	else
			# Unknown.
			echo "unknown"
	fi
}

os=$(os_name)
echo Detected ${os} OS...
user=$(whoami)
echo Detected user ${user}

if [[ ${os} == "windows" ]]; then
	path_db=C:/Users/"$user"/Databases/hsqldb/jpersist/demodb
	path_hsqldb=C:/Users/"$user"/.m2/repository/org/hsqldb/hsqldb/2.4.1/
elif [[ ${os} == "linux" ]]; then
	path_db=~/Databases/hsqldb/jpersist/demodb
	path_hsqldb=~/.m2/repository/org/hsqldb/hsqldb/2.4.1/
else

	echo "os type "${os}" not handled"
	return 1
fi

echo "...handling case..."
echo "creation database at "${path_db}
mkdir -p "$path_db"
echo "moving to hsqldb in mvn default repository"
cd "$path_hsqldb"
echo "launching hsqldb server..."
java -cp hsqldb-2.4.1.jar org.hsqldb.server.Server --database.0 file:"$path_db" --dbname.0 demodb
