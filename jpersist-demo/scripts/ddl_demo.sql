-- NB: Le seguenti query di creazione tabelle per poter essere eseguite con successo necessitano delle seguenti azioni:
-- 1) si crea un nuovo database access con nome demo.accdb
-- 2) si apre il file con MS Access e si avvia il comando Tasto Office > Gestisci > Compatta e ripristina database

-- test table creation on demo.accdb
CREATE TABLE example (
	pk_integer_field INTEGER PRIMARY KEY, 
	long_field LONG NOT NULL, 
	text_255_field TEXT(255) NOT NULL,
	datetime_field DATETIME, 
	text_field TEXT
)

CREATE TABLE example_2 (
	field1 INTEGER PRIMARY KEY, 
	field2 LONG NOT NULL, 
	field3 TEXT(255) NOT NULL,
	field4 DATETIME, 
	field5 TEXT DEFAULT 'testo'
)

CREATE TABLE persona (
	idpersona LONG PRIMARY KEY,
	nome TEXT NOT NULL,
	cognome TEXT NOT NULL
)

-- test insert
insert into example values (1, 3000, 'ciao', #2009-04-21 14:25:53#, 'rossi')

SELECT * FROM example